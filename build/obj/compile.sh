objdir=`pwd`
mbdyndir=~/Build/mbdyn-1.5.5
cd ..
make
cd obj
rm *.o
find ../src/ -name "*.o" -exec cp {} $objdir/ \;
ld -r $objdir/*.o -o cudem.o
/usr/local/cuda/bin/nvcc -m64 -arch=sm_35 -o libcudem.so --shared -Xcompiler cudem.o
