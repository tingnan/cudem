/* 
 * File:   MD.h
 * Author: tingnan
 *
 * Created on February 8, 2011, 8:39 PM
 */

#ifndef _MD_H_
#define	_MD_H_

#ifndef __ENABLE_CUDA__
#define __ENABLE_CUDA__
#endif

#define USE_SINGLE 1
#define USE_DOUBLE 0

#if USE_SINGLE
typedef float FloatType;
#endif

#if USE_DOUBLE
typedef double FloatType;
#endif

typedef FloatType RealVec[3];
typedef FloatType mat3[3][3];

#endif
