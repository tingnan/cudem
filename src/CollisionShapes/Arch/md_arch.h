/* 
 * File:   MDShape.h
 * Author: tingnan
 *
 * Created on February 8, 2011, 10:12 PM
 * 
 * The MDShape class is defined here. basically a surface is composed of
 * 
 */

#ifndef _MDARCH_H_
#define	_MDARCH_H_
#include "md_shape.h"


#ifdef __ENABLE_CUDA__

class MDArch : public MDShapeBase
{
protected:
    static DerivedRegister<MDArch> reg_;
	struct MD_Arch_Params* h_arc_params_;
	struct MD_Arch_Params* d_arc_params_;
public:
	MDArch();
	MDArch(struct MD_Arch_Params&);
    void ParseParams();
	void GetNodeAxisY(vec3&);
	void IntersectSphere(vec4*, vec4*, vec4*, uint, vec4*, vec4*, vec4*);
    void SetNodeOriMat(const mat3&);
    void ArchInit(struct MD_Arch_Params&);
    bool RFTiDivSegments(const vec3&, FloatType, std::vector<vec3>&, std::vector<vec3>&, std::vector<vec3>&, std::vector<vec3>&, std::vector<FloatType>&);
};

#endif
#endif /* MDSHAPE_H */
