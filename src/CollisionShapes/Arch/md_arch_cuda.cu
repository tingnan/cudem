#include "md_arch_cuda.cuh"
#include "GeneralHelper/md_cuda_helper.cuh"
#include "MathHelper/md_vec_cuda.cuh"

__global__ void CollideSphereArch(vec4* p_pos,
					  	  	  	  vec4* p_vel,
                                  vec4* p_ome,
					  	  	  	  uint  num_par,
					  	  	  	  MD_Shape_Params* p_params,
					  	  	  	  MD_Arch_Params*  a_params,
					  	  	  	  vec4* c_foc,
					  	  	  	  vec4* c_mom)
{
	uint index = iIndex();
	if(index >= num_par) return;

	// init local parameters;
	vec4 pos = p_pos[index];
	vec4 vel = p_vel[index];
	vec3 sphere_pos = make_vec3(pos.x, pos.y, pos.z);
	vec3 sphere_vel = make_vec3(vel.x, vel.y, vel.z);
	FloatType sphere_radius = pos.w;
	MD_Shape_Params com_params = p_params[0];
	MD_Arch_Params  arc_params = a_params[0];

	vec3 dr = sphere_pos - com_params.pos;
	FloatType drx = dot(dr, com_params.dirc_para);
	FloatType dry = dot(dr, com_params.dirc_perp);
	FloatType drz = dot(dr, com_params.dirc_orth);
	
	FloatType theta;
	if (abs(drz) > 10000 * abs(dry))
		theta = dc_math_pi / 2.0;
	else
	{
		if (dry > 0)
			theta = abs(atan(drz / dry));
		if (dry < 0)
			theta = dc_math_pi - abs(atan(drz / dry));
	}
	
	vec3 virtual_drv = make_vec3(0.0, 0.0, 0.0);
	vec3 virtual_pos = make_vec3(0.0, 0.0, 0.0);
	vec3 virtual_vel = make_vec3(0.0, 0.0, 0.0);
	vec3 force = make_vec3(0.0, 0.0, 0.0);
	FloatType virtual_radius = 0;
	
	if (abs(drx) > 0.5 * arc_params.width + sphere_radius)
	{
		// non-collision
        c_foc[index] = make_vec4(0, 0, 0, 0);
		c_mom[index] = make_vec4(0, 0, 0, 0);
		return;
	}
	if ((dry * dry + drz * drz) < (arc_params.radius - sphere_radius) * (arc_params.radius - sphere_radius)
			|| (dry * dry + drz * drz) > (arc_params.radius + sphere_radius) * (arc_params.radius + sphere_radius))
	{
		c_foc[index] = make_vec4(0, 0, 0, 0);
		c_mom[index] = make_vec4(0, 0, 0, 0);		
        return;
	}
	if (abs(drx) <= 0.5 * arc_params.width + sphere_radius && abs(drx) >= 0.5 * arc_params.width)
	{
		// collision with top(bottom)-edge and top(bottom)-corner
		if (theta < arc_params.theta)
		{
			virtual_drv = 0.5 * sign(drx) * arc_params.width * com_params.dirc_para + arc_params.radius * cos(theta) * com_params.dirc_perp +
			arc_params.radius * sin(theta) * sign(drz) * com_params.dirc_orth;
			virtual_radius = 0;
			goto spheresphere;
			// edge
		}
		else
		{
			// corner
			virtual_drv = 0.5 * sign(drx) * arc_params.width * com_params.dirc_para + arc_params.radius * cos(arc_params.theta) * com_params.dirc_perp +
			arc_params.radius * sin(arc_params.theta) * sign(drz) * com_params.dirc_orth;
			virtual_radius = 0;
			goto spheresphere;
		}
	}
	if (abs(drx) < 0.5 * arc_params.width)
	{
		// collision with side or side edge
		if (theta < arc_params.theta)
		{
			virtual_drv = drx * com_params.dirc_para + arc_params.radius * cos(theta) * com_params.dirc_perp +
			arc_params.radius * sin(theta) * sign(drz) * com_params.dirc_orth;
			virtual_radius = 0;
			goto spheresphere;
		}
		else
		{
			virtual_drv = drx * com_params.dirc_para + arc_params.radius * cos(arc_params.theta) * com_params.dirc_perp +
			arc_params.radius * sin(arc_params.theta) * sign(drz) * com_params.dirc_orth;
			virtual_radius = 0;
			goto spheresphere;
		}
	}
spheresphere:
	virtual_pos = com_params.pos + virtual_drv;
	virtual_vel = com_params.vel + cross(com_params.ome, virtual_drv);
	force = CollideSphereSphere(arc_params.mparams, sphere_pos, virtual_pos, sphere_vel, virtual_vel, sphere_radius, virtual_radius);
	vec4 myfoc = make_vec4(force.x, force.y, force.z, 0.0);
	c_foc[index] = myfoc;
	vec3 moment = cross(virtual_drv, force);
	vec4 mymom = make_vec4(moment.x, moment.y, moment.z, 0.0);
	c_mom[index] = mymom;
}

void CudaCollideSphereArch(vec4* p_pos,
					  	   vec4* p_vel,
                           vec4* p_ome,
					  	   uint  num_par,
					  	   MD_Shape_Params* p_params,
					  	   MD_Arch_Params*  a_params,
	  	  	  	  	  	   vec4* c_foc,
	  	  	  	  	  	   vec4* c_mom)
{
	uint nblocks, nthreads;
	ComputeGridSize(num_par, WORK_SIZE, nblocks, nthreads);
	CollideSphereArch<<<nblocks, nthreads>>>(p_pos, p_vel, p_ome, num_par, p_params, a_params, c_foc, c_mom);
	KernelError("CudaCollideSphereArch");
}
	  	  	  	  	  	   
	  	  	  	  	  	   
