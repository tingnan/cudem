/*
 * md_shape.cuh
 *
 *  Created on: Jan 14, 2013
 *      Author: tingnan
 */

#ifndef _MD_ARCH_CUH_
#define _MD_ARCH_CUH_
#include "md_shape_cuda.cuh"

struct MD_Arch_Params
{
	FloatType radius;
	FloatType width;
	FloatType theta;
	MD_Mech_Params mparams;
};


void CudaCollideSphereArch(vec4* p_pos,
					  	   vec4* p_vel,
                           vec4* p_ome,
					  	   uint  num_par,
					  	   MD_Shape_Params* p_params,
					  	   MD_Arch_Params*  a_params,
	  	  	  	  	  	   vec4* c_foc,
	  	  	  	  	  	   vec4* c_mom);

#endif /* _MD_SHAPE_CUH_ */
