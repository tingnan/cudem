#include <iostream>
#include <cmath>
using namespace std;


typedef double FloatType;

struct vec3
{
    FloatType x;
    FloatType y;
    FloatType z;
    vec3() : x(0), y(0), z(0) {}
    vec3(FloatType xx, FloatType yy, FloatType zz) : x(xx), y(yy), z(zz) {}

    vec3 operator * (FloatType s)
    {
        return vec3(x * s, y * s, z * s);
    }

    vec3 operator / (FloatType s)
    {
        return vec3(x / s, y / s, z / s);
    }

    vec3 operator - (vec3 b)
    {
        return vec3(x - b.x, y - b.y, z - b.z);
    }

    vec3 operator + (vec3 b)
    {
        return vec3(x + b.x, y + b.y, z + b.z);
    }

};



vec3 operator * (FloatType s, vec3 a)
{
    return vec3(a.x * s, a.y * s, a.z * s);
}


FloatType dot(vec3 a, vec3 b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

struct vec4
{
    FloatType x;
    FloatType y;
    FloatType z;   
    FloatType w; 
    vec4() {}
    vec4(FloatType xx, FloatType yy, FloatType zz, FloatType ww) : x(xx), y(yy), z(zz), w(ww) {}
};

vec3 make_vec3(FloatType x, FloatType y, FloatType z)
{
    return vec3(x, y, z);
};

vec4 make_vec4(FloatType x, FloatType y, FloatType z, FloatType w)
{
    return vec4(x, y, z, w);
};


ostream& operator << (ostream& os, const vec3& v)
{
    os << v.x << " ";
    os << v.y << " ";
    os << v.z << " ";
}


struct MD_Shape_Params
{
    vec3 pos;
    vec3 vel;
    vec3 dirc_para;
    vec3 dirc_perp;
    vec3 dirc_orth;
    MD_Shape_Params () : dirc_para(1., 0., 0.), dirc_perp(0., 1., 0.), dirc_orth(0., 0., 1.)
    {
    }
};


struct MD_Cone_Params
{
    FloatType radius;
    FloatType height;
    MD_Cone_Params (FloatType r, FloatType h) : radius(r), height(h) {}
};

void CollideConeSphere(vec4* p_pos,
                       vec4* p_vel,
                       MD_Shape_Params* p_params,
                       MD_Cone_Params* c_params)
{
    // init local parameters;
    vec4 pos = *p_pos;
    vec4 vel = *p_vel;
    vec3 sphere_pos = make_vec3(pos.x, pos.y, pos.z);
    vec3 sphere_vel = make_vec3(vel.x, vel.y, vel.z);
    FloatType sphere_radius = pos.w;
    MD_Shape_Params com_params = *p_params;
    MD_Cone_Params  con_params = *c_params;

    vec3 dr = sphere_pos - com_params.pos;
    vec3 virtual_drv = make_vec3(0.0, 0.0, 0.0);
    vec3 virtual_pos = make_vec3(0.0, 0.0, 0.0);
    vec3 virtual_vel = make_vec3(0.0, 0.0, 0.0);
    vec3 force = make_vec3(0.0, 0.0, 0.0);
    FloatType virtual_radius = 0;
    
    // Cone's local frame
    vec3 proj = make_vec3(dot(dr, com_params.dirc_para), dot(dr, com_params.dirc_perp), dot(dr, com_params.dirc_orth));
    cout << "proj: " << proj << endl;
    FloatType rho = sqrt(proj.y * proj.y + proj.z * proj.z);
    // side plate
    FloatType sidelen = sqrt(con_params.height * con_params.height + con_params.radius * con_params.radius);
    FloatType normal2side = (rho * con_params.height + proj.x * con_params.radius) / sidelen;
    FloatType sidehei = con_params.height * con_params.radius / sidelen;
    // normal vector to side of the triangle
    cout << "sidelen: " << sidelen << endl;
    cout << "sidehei: " << sidehei << endl;
    cout << "normal2side: " << normal2side << endl;
    vec3 normalvec = make_vec3(con_params.radius / sidelen, con_params.height / sidelen, 0.0);
    vec3 proj2dvec = make_vec3(proj.x, rho, 0.0);

    if (proj.x < -sphere_radius || (proj.x >= 0 && normal2side > sidehei + sphere_radius))
    {
        cout << "no collision\n";
        return;
    }

    // find the closest point on each of the edge and compare
    if (proj.x < 0 && proj.x >= -sphere_radius && rho < con_params.radius + sphere_radius)
    {
        // collide with the bottom plate;
        FloatType factor = rho > con_params.radius ? con_params.radius / rho : 1;
        vec3 tmpvec = make_vec3(0.0, factor * proj.y, factor * proj.z);
        virtual_drv = tmpvec.x * com_params.dirc_para + tmpvec.y * com_params.dirc_perp + tmpvec.z * com_params.dirc_orth;
        cout << "collision:bott: " << virtual_drv << endl;
        return;
    }
    
    if (proj.x >= 0 && normal2side < sidehei + sphere_radius)
    {
        // collide with a side edge
        // determine the tangent displacement
        vec3 tangent2dvec = proj2dvec - normalvec * normal2side;
        vec3 disp2d = normalvec * sidehei + tangent2dvec;
        if (disp2d.x < 0)
        {
            disp2d.x = 0;
            disp2d.y = con_params.radius;
        }
        if (disp2d.x > con_params.height)
        {
            disp2d.x = con_params.height;
            disp2d.y = 0;
        }
        // convert disp2d to disp3d
        FloatType factor = rho > 1e-4? disp2d.y / rho : 0;
        vec3 tmpvec = make_vec3(disp2d.x, proj.y * factor, proj.z * factor);
        virtual_drv = tmpvec.x * com_params.dirc_para + tmpvec.y * com_params.dirc_perp + tmpvec.z * com_params.dirc_orth;
        cout << "collision:side: " << virtual_drv << endl;
        return;
    }

    // inside
    if (proj.x > 0 && normal2side < sidehei)
    {
        cout << "inside\n";
        return;
    }
    
    cout << "nonthing happend!\n";
    return ;
}


int main()
{
    FloatType x = 0;
    FloatType rho = 2 * (1 - x) + 5;
    vec4 vel;
    MD_Cone_Params cparams(2, 1);
    MD_Shape_Params sparams;
    for (double i = 0; i < 2 * M_PI; i += 0.1)
    {
        vec4 pos(x, rho * cos(i), rho * sin(i), 0.5);
        CollideConeSphere(&pos, &vel, &sparams, &cparams);
    }
}