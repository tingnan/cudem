/*
 * File:   MDShape.cc
 * Author: tingnan
 * 
 * Created on February 8, 2011, 10:12 PM
 */

#include "md_cone.h"
#include "MathHelper/md_utility.h"
#include <cstring>
#include <cassert>

#ifdef __ENABLE_CUDA__

#include "md_cone_cuda.cuh"

DerivedRegister<MDCone> MDCone::reg_("Cone");

MDCone::MDCone()
{
    MD_CudaMalloc<MD_Cone_Params>(&d_con_params_, &h_con_params_);
}

MDCone::MDCone(MD_Cone_Params& input)
{
    MD_CudaMalloc<MD_Cone_Params>(&d_con_params_, &h_con_params_);
    ConeInit(input);
}

void MDCone::_bindGLAsset() 
{
    // initialize the vertices_ and indices_ for opengl render
    std::cout << "debug:\n";
    std::map<std::string, uint>& vbomap = gl_mesh_->getVBOs();
    std::vector<FloatType>& vertices = gl_mesh_->getVertices();
    std::vector<FloatType>& colors = gl_mesh_->getVertexColors();
    std::vector<FloatType>& normals = gl_mesh_->getVertexNormals();
    std::vector<uint>& indices = gl_mesh_->getVertexIndices();

    MeshGenerator::generateMesh("Cone", 
        (void*)h_con_params_, 
        vertices, 
        colors, 
        normals, 
        indices);
    
    // allocate the vbo

    vbomap["vertex"] = OpenGLCreateVBOWithData((void*)&vertices[0], sizeof(FloatType) * vertices.size());
    vbomap["color"] = OpenGLCreateVBOWithData((void*)&colors[0], sizeof(FloatType) * colors.size());
    vbomap["normal"] = OpenGLCreateVBOWithData((void*)&normals[0], sizeof(FloatType) * normals.size());
    vbomap["index"] = OpenGLCreateVBOWithData((void*)&indices[0], sizeof(uint) * indices.size());
}

void MDCone::IntersectSphere(vec4* p_pos,       // input
                              vec4* p_vel,       // input
                              vec4* p_ome,
                              uint  num_par,
                              vec4* c_foc,
                              vec4* c_mom,
                              vec4* p_mom)
{
    MDShape_MemCpyHostToDevice();
    CudaCollideConeSphere(p_pos, p_vel, num_par, d_params_, d_con_params_, c_foc, c_mom);
    MDShape_MemCpyDeviceToHost();
}

void MDCone::ConeInit(MD_Cone_Params& input)
{
    memcpy(h_con_params_, &input, sizeof(MD_Cone_Params));
    MD_CudaMemCpyHostToDevice<MD_Cone_Params>(d_con_params_, h_con_params_);
}

bool MDCone::RFTiDivSegments(const vec3& plane_dirc, FloatType plane_dist, std::vector<vec3>& pos_list, std::vector<vec3>& ori_list, std::vector<vec3>& nor_list, std::vector<vec3>& vel_list, std::vector<FloatType>& are_list)
{
    
    return false;
}

void MDCone::ParseParams()
{
    std::map<std::string, FloatType>::iterator it; 
    if((it = geo_map_.find("height")) == geo_map_.end())
    {
        std::cerr << "warning: height is not specified for cone \n"; 
    }
    else
    {
        h_con_params_->height = it->second;
    }
    if((it = geo_map_.find("radius")) == geo_map_.end())
    {
        std::cerr << "warning: radius is not specified for cone \n"; 
    }
    else
    {
        h_con_params_->radius = it->second;
    }
    if((it = geo_map_.find("mu")) == geo_map_.end())
    {
        std::cerr << "warning: mu is not specified for cone \n"; 
    }
    else
    {
        h_con_params_->mparams.mu = it->second;
    }
    if((it = geo_map_.find("kn")) == geo_map_.end())
    {
        std::cerr << "warning: kn is not specified for cone \n"; 
    }
    else
    {
        h_con_params_->mparams.kn = it->second;
    }
    if((it = geo_map_.find("gn")) == geo_map_.end())
    {
        std::cerr << "warning: gn is not specified for cone \n"; 
    }
    else
    {
        h_con_params_->mparams.gn = it->second;
    }
    // now copy to GPU
	MD_CudaMemCpyHostToDevice<MD_Cone_Params>(d_con_params_, h_con_params_);    
}

#endif
