/* 
 * File:   MDShape.h
 * Author: tingnan
 *
 * Created on Oct 8, 2014, 10:12 PM
 * 
 * The MDShape class is defined here. basically a surface is composed of
 * 
 */

#ifndef _MDCONE_H_
#define	_MDCONE_H_
#include "md_shape.h"

#ifdef __ENABLE_CUDA__

class MDCone : public MDShapeBase
{
protected:
    static DerivedRegister<MDCone> reg_;
	struct MD_Cone_Params* h_con_params_;
	struct MD_Cone_Params* d_con_params_;
	virtual void _bindGLAsset();
public:
	MDCone();
	MDCone(struct MD_Cone_Params&);
	void IntersectSphere(vec4*, vec4*, vec4*, uint, vec4*, vec4*, vec4*);
    void ConeInit(struct MD_Cone_Params&);
    bool RFTiDivSegments(const vec3&, FloatType, std::vector<vec3>&, std::vector<vec3>&, std::vector<vec3>&, std::vector<vec3>&, std::vector<FloatType>&);
    void ParseParams();
    void* GetGeometryParams() {return (void*) h_con_params_;}
};

#endif
#endif /* MDSHAPE_H */
