#include "md_cone_cuda.cuh"
#include "GeneralHelper/md_cuda_helper.cuh"
#include "MathHelper/md_vec_cuda.cuh"

#define MAX_FORCE 1e9


__global__ void CollideConeSphere(vec4* p_pos,
                                   vec4* p_vel,
                                   uint  num_par,
                                   MD_Shape_Params* p_params,
                                   MD_Cone_Params* c_params,
                                   vec4* c_foc,
                                   vec4* c_mom)
{
    uint index = iIndex();
    if(index >= num_par) return;

    // init local parameters;
    vec4 pos = p_pos[index];
    vec4 vel = p_vel[index];
    vec3 sphere_pos = make_vec3(pos.x, pos.y, pos.z);
    vec3 sphere_vel = make_vec3(vel.x, vel.y, vel.z);
    FloatType sphere_radius = pos.w;
    MD_Shape_Params com_params = p_params[0];
    MD_Cone_Params con_params = c_params[0];

    vec3 dr = sphere_pos - com_params.pos;
    vec3 virtual_drv = make_vec3(0.0, 0.0, 0.0);
    vec3 virtual_pos = make_vec3(0.0, 0.0, 0.0);
    vec3 virtual_vel = make_vec3(0.0, 0.0, 0.0);
    vec3 force = make_vec3(0.0, 0.0, 0.0);
    FloatType virtual_radius = 0;
    
    // Cone's local frame
    vec3 proj = make_vec3(dot(dr, com_params.dirc_para), dot(dr, com_params.dirc_perp), dot(dr, com_params.dirc_orth));
    
    FloatType rho = sqrt(proj.y * proj.y + proj.z * proj.z);
    // side plate
    FloatType sidelen = sqrt(con_params.height * con_params.height + con_params.radius * con_params.radius);
    FloatType normal2side = (rho * con_params.height + proj.x * con_params.radius) / sidelen;
    FloatType sidehei = con_params.height * con_params.radius / sidelen;
    // normal vector to side of the triangle
    vec3 normalvec = make_vec3(con_params.radius / sidelen, con_params.height / sidelen, 0.0);
    vec3 proj2dvec = make_vec3(proj.x, rho, 0.0);

    if (proj.x < -sphere_radius || (proj.x >= 0 && normal2side > sidehei + sphere_radius))
    {
        c_foc[index] = make_vec4(0., 0., 0., 0.);
        c_mom[index] = c_foc[index];
        return;
    }

    // find the closest point on each of the edge and compare
    if (proj.x < 0 && proj.x >= -sphere_radius && rho < con_params.radius + sphere_radius)
    {
        // collide with the bottom plate;
        FloatType factor = rho > con_params.radius ? con_params.radius / rho : 1;
        vec3 tmpvec = make_vec3(0.0, factor * proj.y, factor * proj.z);
        virtual_drv = tmpvec.x * com_params.dirc_para + tmpvec.y * com_params.dirc_perp + tmpvec.z * com_params.dirc_orth;
        goto spheresphere;
    }
    
    if (proj.x >= 0 && normal2side < sidehei + sphere_radius)
    {
        // collide with a side edge
        // determine the tangent displacement
        vec3 tangent2dvec = proj2dvec - normalvec * normal2side;
        vec3 disp2d = normalvec * sidehei + tangent2dvec;
        if (disp2d.x < 0)
        {
            disp2d.x = 0;
            disp2d.y = con_params.radius;
        }
        if (disp2d.x > con_params.height)
        {
            disp2d.x = con_params.height;
            disp2d.y = 0;
        }
        // convert disp2d to disp3d
        FloatType factor = rho > 1e-4? disp2d.y / rho : 0;
        vec3 tmpvec = make_vec3(disp2d.x, proj.y * factor, proj.z * factor);
        virtual_drv = tmpvec.x * com_params.dirc_para + tmpvec.y * com_params.dirc_perp + tmpvec.z * com_params.dirc_orth;
        goto spheresphere;
    }

    // inside
    if (proj.x > 0 && normal2side < sidehei)
    {
        c_foc[index] = make_vec4(MAX_FORCE, MAX_FORCE, MAX_FORCE, 0.0);
        return;
    }
    
    return ;

spheresphere:
    virtual_pos = com_params.pos + virtual_drv;
    virtual_vel = com_params.vel + cross(com_params.ome, virtual_drv);
    force = CollideSphereSphere(con_params.mparams, sphere_pos, virtual_pos, sphere_vel, virtual_vel, sphere_radius, virtual_radius);
    vec4 myfoc = make_vec4(force.x, force.y, force.z, 0.0);
    c_foc[index] = myfoc;
    vec3 moment = cross(virtual_drv, force);
    vec4 mymom = make_vec4(moment.x, moment.y, moment.z, 0.0);
    c_mom[index] = mymom;
    
    return;
}

void CudaCollideConeSphere(vec4* p_pos,
                            vec4* p_vel,
                            uint  num_par,
                            MD_Shape_Params* p_params,
                            MD_Cone_Params* c_params,
                            vec4* c_foc,
                            vec4* c_mom)
{
    uint nblocks, nthreads;
    ComputeGridSize(num_par, WORK_SIZE, nblocks, nthreads);
    // cudaPrintfInit();
    CollideConeSphere<<<nblocks, nthreads>>>(p_pos, p_vel, num_par, p_params, c_params, c_foc, c_mom);
    // cudaPrintfDisplay();
    //cudaPrintfEnd();
    KernelError("CudaCollideSphereCone");
}          
