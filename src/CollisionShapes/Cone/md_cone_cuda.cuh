/*
 * md_shape.cuh
 *
 *  Created on: Jan 14, 2013
 *      Author: tingnan
 */

#ifndef _MD_CONE_CUH_
#define _MD_CONE_CUH_

#include "md_shape_cuda.cuh"

struct MD_Cone_Params
{
    FloatType height;
    FloatType radius;
    MD_Mech_Params mparams;
};

void CudaCollideConeSphere(vec4* p_pos,
					  	  	 vec4* p_vel,
					  	  	 uint  num_par,
					  	  	 MD_Shape_Params*  p_params,
					  	  	 MD_Cone_Params* c_params,
	  	  	  	  	  	  	 vec4* c_foc,
	  	  	  	  	  	  	 vec4* c_mom);
	  	  	  	  	  	  	   
#endif /* _MD_SHAPE_CUH_ */
