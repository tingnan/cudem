/*
 * File:   MDShape.cc
 * Author: tingnan
 * 
 * Created on February 8, 2011, 10:12 PM
 */

#include "md_cuboid.h"
#include "MathHelper/md_utility.h"
#include <cstring>
#include <cassert>

#ifdef __ENABLE_CUDA__

#include "md_cuboid_cuda.cuh"

DerivedRegister<MDCuboid> MDCuboid::reg_("Cuboid");



void MDCuboid::_bindGLAsset() 
{
	// initialize the vertices_ and indices_ for opengl render

	std::map<std::string, uint>& vbomap = gl_mesh_->getVBOs();
	std::vector<FloatType>& vertices = gl_mesh_->getVertices();
	std::vector<FloatType>& colors = gl_mesh_->getVertexColors();
	std::vector<FloatType>& normals = gl_mesh_->getVertexNormals();
	std::vector<uint>& indices = gl_mesh_->getVertexIndices();

	MeshGenerator::generateMesh("Cuboid", 
		(void*)h_cud_params_, 
		vertices, 
		colors, 
		normals, 
		indices);
	
	// allocate the vbo

	vbomap["vertex"] = OpenGLCreateVBOWithData((void*)&vertices[0], sizeof(FloatType) * vertices.size());
	vbomap["color"] = OpenGLCreateVBOWithData((void*)&colors[0], sizeof(FloatType) * colors.size());
	vbomap["normal"] = OpenGLCreateVBOWithData((void*)&normals[0], sizeof(FloatType) * normals.size());
	vbomap["index"] = OpenGLCreateVBOWithData((void*)&indices[0], sizeof(uint) * indices.size());
}


MDCuboid::MDCuboid()
{
	MD_CudaMalloc<MD_Cuboid_Params>(&d_cud_params_, &h_cud_params_);
}

MDCuboid::MDCuboid(MD_Cuboid_Params& input)
{
	MD_CudaMalloc<MD_Cuboid_Params>(&d_cud_params_, &h_cud_params_);
	CuboidInit(input);
}


void MDCuboid::IntersectSphere(vec4* p_pos,       // input
								 vec4* p_vel,       // input
                                 vec4* p_ome,
								 uint  num_par,
								 vec4* c_foc,
								 vec4* c_mom,
                                 vec4* p_mom)
{
	MDShape_MemCpyHostToDevice();
	CudaCollideCuboidSphere(p_pos, p_vel, num_par, d_params_, d_cud_params_, c_foc, c_mom);
	MDShape_MemCpyDeviceToHost();
}


void MDCuboid::CuboidInit(MD_Cuboid_Params& input)
{
	memcpy(h_cud_params_, &input, sizeof(MD_Cuboid_Params));
	MD_CudaMemCpyHostToDevice<MD_Cuboid_Params>(d_cud_params_, h_cud_params_);
}

//void MDCuboid::CuboidInit(FloatType r, FloatType l)
//{
//	h_cud_params_->radius = r;
//	h_cud_params_->length = l;
//}


bool MDCuboid::RFTiDivSegments(const vec3& plane_dirc, FloatType plane_dist, std::vector<vec3>& pos_list, std::vector<vec3>& ori_list, std::vector<vec3>& nor_list, std::vector<vec3>& vel_list, std::vector<FloatType>& are_list)
{
	bool ifcollision = false;
	int npiece = pos_list.size();
	div_t divresult = div(npiece, 4);
	int npiece_b = divresult.quot + divresult.rem;
	int i;
    
	for (i = 0; i < npiece_b; i++)
	{
		vec3 disp = h_params_->dirc_para * ( (i + 0.5) * h_cud_params_->lx / double(npiece_b) - h_cud_params_->lx / 2.0) - (0.5 * h_cud_params_->lz) * h_params_->dirc_orth;
		ori_list[i] = h_params_->dirc_orth;
		pos_list[i] = h_params_->pos + disp;
		vel_list[i] = h_params_->vel + cross(h_params_->ome, disp);
		nor_list[i] = make_vec3(-ori_list[i].x, -ori_list[i].y, -ori_list[i].z);
		are_list[i] = h_cud_params_->lx * h_cud_params_->ly / double(npiece_b);
		if (dot(pos_list[i], plane_dirc) < plane_dist)
		{
			ifcollision = true;
		}
	}
    
	int npiece_t = divresult.quot;
	for (i = 0; i < npiece_t; i++)
	{
		vec3 disp = h_params_->dirc_para * ( (i + 0.5) * h_cud_params_->lx / double(npiece_t) - h_cud_params_->lx / 2.0) + (0.5 * h_cud_params_->lz) * h_params_->dirc_orth;
		ori_list[i + npiece_b] = h_params_->dirc_orth;
		pos_list[i + npiece_b] = h_params_->pos + disp;
		vel_list[i + npiece_b] = h_params_->vel + cross(h_params_->ome, disp);
		nor_list[i + npiece_b] = h_params_->dirc_orth;
		are_list[i + npiece_b] = h_cud_params_->lx * h_cud_params_->ly / double(npiece_t);
		if (dot(pos_list[i + npiece_b], plane_dirc) < plane_dist)
		{
			ifcollision = true;
		}
	}
    
	int npiece_f = divresult.quot;
	for (i = 0; i < npiece_f; ++i)
	{
		vec3 disp = h_params_->dirc_orth * ( (i + 0.5) * h_cud_params_->lz / double(npiece_f) - h_cud_params_->lz / 2.0) + (0.5 * h_cud_params_->lx) * h_params_->dirc_para;
		ori_list[i + npiece_b + npiece_t] = h_params_->dirc_para;
		pos_list[i + npiece_b + npiece_t] = h_params_->pos + disp;
		vel_list[i + npiece_b + npiece_t] = h_params_->vel + cross(h_params_->ome, disp);
		nor_list[i + npiece_b + npiece_t] = h_params_->dirc_para;
        are_list[i + npiece_b + npiece_t] = h_cud_params_->ly * h_cud_params_->lz / double(npiece_f);
		if (dot(pos_list[i + npiece_b + npiece_t], plane_dirc) < plane_dist)
		{
			ifcollision = true;
		}
	}

	int npiece_r = divresult.quot;
	for (i = 0; i < npiece_r; ++i)
	{
		vec3 disp = h_params_->dirc_orth * ( (i + 0.5) * h_cud_params_->lz / double(npiece_r) - h_cud_params_->lz / 2.0) - (0.5 * h_cud_params_->lx) * h_params_->dirc_para;
		ori_list[i + npiece_b + npiece_t + npiece_f] = h_params_->dirc_para;
		pos_list[i + npiece_b + npiece_t + npiece_f] = h_params_->pos + disp;
		vel_list[i + npiece_b + npiece_t + npiece_f] = h_params_->vel + cross(h_params_->ome, disp);
		nor_list[i + npiece_b + npiece_t + npiece_f] = -1.0 * h_params_->dirc_para;
        are_list[i + npiece_b + npiece_t + npiece_f] = h_cud_params_->ly * h_cud_params_->lz / double(npiece_r);
		if (dot(pos_list[i + npiece_b + npiece_t + npiece_f], plane_dirc) < plane_dist)
		{
			ifcollision = true;
		}
	}
    
	return ifcollision;
}


void MDCuboid::ParseParams()
{
    std::map<std::string, FloatType>::iterator it; 
    if((it = geo_map_.find("lx")) == geo_map_.end())
    {
        std::cerr << "warning: lx is not specified for cuboic \n"; 
    }
    else
    {
        h_cud_params_->lx = it->second;
    }
    if((it = geo_map_.find("ly")) == geo_map_.end())
    {
        std::cerr << "warning: lz is not specified for cuboic \n"; 
    }
    else
    {
        h_cud_params_->ly = it->second;
    }
    if((it = geo_map_.find("lz")) == geo_map_.end())
    {
        std::cerr << "warning: lz is not specified for cuboic \n"; 
    }
    else
    {
        h_cud_params_->lz = it->second;
    }
    if((it = geo_map_.find("mu")) == geo_map_.end())
    {
        std::cerr << "warning: mu is not specified for cuboic \n"; 
    }
    else
    {
        h_cud_params_->mparams.mu = it->second;
    }
    if((it = geo_map_.find("kn")) == geo_map_.end())
    {
        std::cerr << "warning: kn is not specified for cuboic \n"; 
    }
    else
    {
        h_cud_params_->mparams.kn = it->second;
    }
    if((it = geo_map_.find("gn")) == geo_map_.end())
    {
        std::cerr << "warning: gn is not specified for cuboic \n"; 
    }
    else
    {
        h_cud_params_->mparams.gn = it->second;
    }
    // now copy to GPU
	MD_CudaMemCpyHostToDevice<MD_Cuboid_Params>(d_cud_params_, h_cud_params_);    
}

#endif
