/* 
 * File:   MDShape.h
 * Author: tingnan
 *
 * Created on February 8, 2011, 10:12 PM
 * 
 * The MDShape class is defined here. basically a surface is composed of
 * 
 */

#ifndef _MDCUBOID_H_
#define	_MDCUBOID_H_
#include "md_shape.h"

#ifdef __ENABLE_CUDA__

class MDCuboid : public MDShapeBase
{
protected:
    static DerivedRegister<MDCuboid> reg_;
	struct MD_Cuboid_Params* h_cud_params_;
	struct MD_Cuboid_Params* d_cud_params_;
    virtual void _bindGLAsset();
public:
	MDCuboid();
	MDCuboid(struct MD_Cuboid_Params&);
	void IntersectSphere(vec4*, vec4*, vec4*, uint, vec4*, vec4*, vec4*);
    void CuboidInit(struct MD_Cuboid_Params&);
    bool RFTiDivSegments(const vec3&, FloatType, std::vector<vec3>&, std::vector<vec3>&, std::vector<vec3>&, std::vector<vec3>&, std::vector<FloatType>&);
    void ParseParams();
    void* GetGeometryParams() {return (void*) h_cud_params_;}
};

#endif
#endif /* MDSHAPE_H */
