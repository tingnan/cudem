#include "md_cuboid_cuda.cuh"
#include "GeneralHelper/md_cuda_helper.cuh"
#include "MathHelper/md_vec_cuda.cuh"

// notice: this function assumes the s_pos vector is already shifted and rotated according coordinates
// if virtual_pos.w == 1, then it means collision;
#define MAX_FORCE 1e9

__global__ void CollideCuboidSphere(vec4* p_pos,
					  	  	  	  	vec4* p_vel,
					  	  	  	  	uint  num_par,
					  	  	  	  	MD_Shape_Params*  p_params,
					  	  	  	  	MD_Cuboid_Params* c_params,
					  	  	  	  	vec4* c_foc,
					  	  	  	  	vec4* c_mom)
{
	uint index = iIndex();
	if(index >= num_par) return;

	// init local parameters;
	vec4 pos = p_pos[index];
	vec4 vel = p_vel[index];
	vec3 sphere_pos = make_vec3(pos.x, pos.y, pos.z);
	vec3 sphere_vel = make_vec3(vel.x, vel.y, vel.z);
	FloatType sphere_radius = pos.w;
	MD_Shape_Params  com_params = p_params[0];
	MD_Cuboid_Params cud_params = c_params[0];

	vec3 dr = sphere_pos - com_params.pos;
	vec3 virtual_drv = make_vec3(0.0, 0.0, 0.0);
	vec3 virtual_pos = make_vec3(0.0, 0.0, 0.0);
	vec3 virtual_vel = make_vec3(0.0, 0.0, 0.0);
	vec3 force = make_vec3(0.0, 0.0, 0.0);
	FloatType virtual_radius = 0;
	
	FloatType proj_array[3] = {dot(dr, com_params.dirc_para), dot(dr, com_params.dirc_perp), dot(dr, com_params.dirc_orth)};
	FloatType leng_array[3] = {cud_params.lx, cud_params.ly, cud_params.lz};
	
    if(fabs(proj_array[0]) <= leng_array[0] * 0.5 && fabs(proj_array[1]) <= 0.5 * leng_array[1] && fabs(proj_array[2]) <= 0.5 * leng_array[2] )
    {
        c_foc[index] = make_vec4(MAX_FORCE, MAX_FORCE, MAX_FORCE, 0.0);
        return;
    }
	// now try to collide with 6 faces
    
	vec4 tmpvec = make_vec4(0, 0, 0, 0);
    FloatType tmplen;
    
    for(tmplen = -0.5 * leng_array[2]; tmplen <= 0.5 * leng_array[2]; tmplen += leng_array[2])
    {
        tmpvec = CollideSquareSphereLocal(leng_array[0], leng_array[1], tmplen, sign(tmplen), make_vec3(proj_array[0], proj_array[1], proj_array[2]), sphere_radius);
        if(tmpvec.w == 1)
        {
            virtual_drv = tmpvec.x * com_params.dirc_para + tmpvec.y * com_params.dirc_perp + tmplen * com_params.dirc_orth;
            goto spheresphere;
        }
    }

    for(tmplen = -0.5 * leng_array[0]; tmplen <= 0.5 * leng_array[0]; tmplen += leng_array[0])
    {
        tmpvec = CollideSquareSphereLocal(leng_array[1], leng_array[2], tmplen, sign(tmplen), make_vec3(proj_array[1], proj_array[2], proj_array[0]), sphere_radius);
        if(tmpvec.w == 1)
        {
            virtual_drv = tmpvec.x * com_params.dirc_perp + tmpvec.y * com_params.dirc_orth + tmplen * com_params.dirc_para;
            goto spheresphere;
        }
    }

    for(tmplen = -0.5 * leng_array[1]; tmplen <= 0.5 * leng_array[1]; tmplen += leng_array[1])
    {
        tmpvec = CollideSquareSphereLocal(leng_array[2], leng_array[0], tmplen, sign(tmplen), make_vec3(proj_array[2], proj_array[0], proj_array[1]), sphere_radius);
        if(tmpvec.w == 1)
        {
            virtual_drv = tmpvec.x * com_params.dirc_orth + tmpvec.y * com_params.dirc_para + tmplen * com_params.dirc_perp;
            goto spheresphere;
        }
    }

	return ;
    
spheresphere:
	virtual_pos = com_params.pos + virtual_drv;
	virtual_vel = com_params.vel + cross(com_params.ome, virtual_drv);
	force = CollideSphereSphere(cud_params.mparams, sphere_pos, virtual_pos, sphere_vel, virtual_vel, sphere_radius, virtual_radius);
	vec4 myfoc = make_vec4(force.x, force.y, force.z, 0.0);
	c_foc[index] = myfoc;
	vec3 moment = cross(virtual_drv, force);
	vec4 mymom = make_vec4(moment.x, moment.y, moment.z, 0.0);
	c_mom[index] = mymom;
	return;
}

void CudaCollideCuboidSphere(vec4* p_pos,
	  	  	  	  	  	  	 vec4* p_vel,
	  	  	  	  	  	  	 uint  num_par,
	  	  	  	  	  	  	 MD_Shape_Params*  p_params,
	  	  	  	  	  	  	 MD_Cuboid_Params* c_params,
	  	  	  	  	  	  	 vec4* c_foc,
	  	  	  	  	  	  	 vec4* c_mom)
{
	uint nblocks, nthreads;
	ComputeGridSize(num_par, WORK_SIZE, nblocks, nthreads);
	CollideCuboidSphere<<<nblocks, nthreads>>>(p_pos, p_vel, num_par, p_params, c_params, c_foc, c_mom);
	KernelError("CudaCollideSphereCuboid");
}	  	   
