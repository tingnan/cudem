/*
 * File:   MDShape.cc
 * Author: tingnan
 * 
 * Created on February 8, 2011, 10:12 PM
 */
#include "md_cylinder.h"
#include "MathHelper/md_utility.h"
#include <cstring>
#include <cassert>

#ifdef __ENABLE_CUDA__

#include "md_cylinder_cuda.cuh"


DerivedRegister<MDCylinder> MDCylinder::reg_("Cylinder");



void MDCylinder::_bindGLAsset()
{

}


MDCylinder::MDCylinder()
{
	MD_CudaMalloc<MD_Cylinder_Params>(&d_cyl_params_, &h_cyl_params_);
}

MDCylinder::MDCylinder(MD_Cylinder_Params& input)
{
	MD_CudaMalloc<MD_Cylinder_Params>(&d_cyl_params_, &h_cyl_params_);
	CylinderInit(input);
}


void MDCylinder::IntersectSphere(vec4* p_pos,       // input
								 vec4* p_vel,       // input
                                 vec4* p_ome,
								 uint  num_par,
								 vec4* c_foc,
								 vec4* c_mom,
                                 vec4* p_mom)
{
	MDShape_MemCpyHostToDevice();
	CudaCollideCylinderSphere(p_pos, p_vel, p_ome, num_par, d_params_, d_cyl_params_, c_foc, c_mom, p_mom);
	MDShape_MemCpyDeviceToHost();
}


void MDCylinder::CylinderInit(MD_Cylinder_Params& input)
{
	memcpy(h_cyl_params_, &input, sizeof(MD_Cylinder_Params));
	h_params_->dirc_para = make_vec3(1, 0, 0);
	h_params_->dirc_perp = make_vec3(0, 1, 0);
	MD_CudaMemCpyHostToDevice<MD_Cylinder_Params>(d_cyl_params_, h_cyl_params_);

	// we will initialize the vertices in its own frame
	// ideally we will use GL_TRIANGLE_STRIP to save space
	// for now let us just use GL_TRIANGLES for fast implementation
	
}

//void MDCylinder::CylinderInit(FloatType r, FloatType l)
//{
//	h_cyl_params_->radius = r;
//	h_cyl_params_->length = l;
//}

void MDCylinder::SetNodeOriMat(const mat3& orientationMatrix)
{
	vec3 para = make_vec3(1, 0, 0);
	vec3 perp = make_vec3(0, 1, 0);
	memcpy(&h_params_->ori, orientationMatrix, 9 * sizeof(FloatType));
	h_params_->dirc_para.x =
	h_params_->ori[0][0] * para.x + h_params_->ori[0][1] * para.y +
	h_params_->ori[0][2] * para.z;
	h_params_->dirc_para.y =
	h_params_->ori[1][0] * para.x + h_params_->ori[1][1] * para.y +
	h_params_->ori[1][2] * para.z;
	h_params_->dirc_para.z =
	h_params_->ori[2][0] * para.x + h_params_->ori[2][1] * para.y +
	h_params_->ori[2][2] * para.z;
	h_params_->dirc_perp.x =
	h_params_->ori[0][0] * perp.x + h_params_->ori[0][1] * perp.y +
	h_params_->ori[0][2] * perp.z;
	h_params_->dirc_perp.y =
	h_params_->ori[1][0] * perp.x + h_params_->ori[1][1] * perp.y +
	h_params_->ori[1][2] * perp.z;
	h_params_->dirc_perp.z =
	h_params_->ori[2][0] * perp.x + h_params_->ori[2][1] * perp.y +
	h_params_->ori[2][2] * perp.z;
	//cout << orientation_vec_.x << "\t" << orientation_vec_.y << "\t" << orientation_vec_.z << endl;
}

bool MDCylinder::RFTiDivSegments(const vec3& plane_norm, FloatType plane_dist, std::vector<vec3>& pos_list, std::vector<vec3>& ori_list, std::vector<vec3>& nor_list, std::vector<vec3>& vel_list, std::vector<FloatType>& are_list)
{
	FloatType s_angle = -hc_math_pi;
	FloatType e_angle =  hc_math_pi;
	bool ifcollision = false;
	int  npiece = pos_list.size();
	for (int i = 0; i < npiece; i++)
	{
		FloatType tmpangle = s_angle + (e_angle - s_angle) * i / FloatType(npiece);
		vec3 surf_drv = h_cyl_params_->radius * rot_axis(h_params_->dirc_para, h_params_->dirc_perp, tmpangle);
		ori_list[i] = surf_drv / h_cyl_params_->radius;
        nor_list[i] = ori_list[i];
		pos_list[i] = h_params_->pos + surf_drv;
		vel_list[i] = h_params_->vel + cross(h_params_->ome, surf_drv);
		are_list[i] = (FloatType(h_cyl_params_->length * h_cyl_params_->radius * (e_angle - s_angle) / FloatType(npiece)));
		if (dot(plane_norm, pos_list[i]) < plane_dist) ifcollision = true;
	}
	return ifcollision;
}




void MDCylinder::GetNodeAxisY(vec3& ydir)
{
	 ydir = h_params_->dirc_para;
}

void MDCylinder::ParseParams()
{
    std::map<std::string, FloatType>::iterator it;
    if((it = geo_map_.find("radius")) == geo_map_.end())
    {
        std::cerr << "warning: radius is not specified for arch" << std::endl;
    }
    else
    {
        h_cyl_params_->radius = it->second;
    }
    if((it = geo_map_.find("length")) == geo_map_.end())
    {
        std::cerr << "warning: width is not specified for arch" << std::endl;
    }
    else
    {
        h_cyl_params_->length = it->second;
    }
    if((it = geo_map_.find("mu")) == geo_map_.end())
    {
        std::cerr << "warning: mu is not specified for arch" << std::endl;
    }
    else
    {
        h_cyl_params_->mparams.mu = it->second;
    }
    if((it = geo_map_.find("kn")) == geo_map_.end())
    {
        std::cerr << "warning: kn is not specified for arch" << std::endl;
    }
    else
    {
        h_cyl_params_->mparams.kn = it->second;
    }
    if((it = geo_map_.find("gn")) == geo_map_.end())
    {
        std::cerr << "warning: gn is not specified for arch" << std::endl;
    }
    else
    {
        h_cyl_params_->mparams.gn = it->second;
    }
    MD_CudaMemCpyHostToDevice<MD_Cylinder_Params>(d_cyl_params_, h_cyl_params_);
}

#endif
