/* 
 * File:   MDShape.h
 * Author: tingnan
 *
 * Created on February 8, 2011, 10:12 PM
 * 
 * The MDShape class is defined here. basically a surface is composed of
 * 
 */

#ifndef _MDCYLINDER_H_
#define	_MDCYLINDER_H_
#include "md_shape.h"

#ifdef __ENABLE_CUDA__

class MDCylinder : public MDShapeBase
{
protected:
    static DerivedRegister<MDCylinder> reg_;
	struct MD_Cylinder_Params* h_cyl_params_;
	struct MD_Cylinder_Params* d_cyl_params_;
    void _bindGLAsset();
public:
	MDCylinder();
	MDCylinder(struct MD_Cylinder_Params&);
    void ParseParams();
    bool RFTiDivSegments(const vec3& plane_norm, FloatType plane_dist, std::vector<vec3>& pos_list, std::vector<vec3>& ori_list, std::vector<vec3>& nor_list, std::vector<vec3>& vel_list, std::vector<FloatType>& are_list);
	void IntersectSphere(vec4*, vec4*, vec4*, uint, vec4*, vec4*, vec4*);
    void SetNodeOriMat(const mat3&);
    void CylinderInit(struct MD_Cylinder_Params&);
    void GetNodeAxisY(vec3& ydir);
};

#endif
#endif /* MDSHAPE_H */
