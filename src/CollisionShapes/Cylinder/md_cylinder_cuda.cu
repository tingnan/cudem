#include "md_cylinder_cuda.cuh"
#include "GeneralHelper/md_cuda_helper.cuh"
#include "MathHelper/md_vec_cuda.cuh"

__global__ void CollideCylinderSphere(vec4* p_pos,
					  	  	  	  	  vec4* p_vel,
                                      vec4* p_ome,
					  	  	  	  	  uint  num_par,
					  	  	  	  	  MD_Shape_Params*    p_params,
					  	  	  	  	  MD_Cylinder_Params* c_params,
					  	  	  	  	  vec4* c_foc,
					  	  	  	  	  vec4* c_mom,
                                      vec4* p_mom)
{
	uint index = iIndex();
	if(index >= num_par) return;

	// init local parameters;
	vec4 pos = p_pos[index];
	vec4 vel = p_vel[index];
    vec4 ome = p_ome[index];
	vec3 sphere_pos = make_vec3(pos.x, pos.y, pos.z);
	vec3 sphere_vel = make_vec3(vel.x, vel.y, vel.z);
    vec3 sphere_ome = make_vec3(ome.x, ome.y, ome.z);
	FloatType sphere_radius = pos.w;
	MD_Shape_Params    com_params = p_params[0];
	MD_Cylinder_Params cyl_params = c_params[0];

	vec3 dr = sphere_pos - com_params.pos;
	FloatType proj_dr = dot(dr, com_params.dirc_para);
	vec3 perp_drv = dr - com_params.dirc_para * proj_dr;
	FloatType perp_dr = length(perp_drv);

	vec3 virtual_drv = make_vec3(0, 0, 0);
	vec3 virtual_pos = make_vec3(0, 0, 0);
	vec3 virtual_vel = make_vec3(0, 0, 0);
    vec3 virtual_ome = make_vec3(0, 0, 0);
	vec3 forcei = make_vec3(0, 0, 0);
    vec3 momenti = make_vec3(0, 0, 0);
	FloatType virtual_radius = 0;

	if(abs(proj_dr) > sphere_radius + 0.5 * cyl_params.length)
	{
		c_foc[index] = make_vec4(0, 0, 0, 0);
		c_mom[index] = make_vec4(0, 0, 0, 0);
		return;
	}

	if(perp_dr > cyl_params.radius + sphere_radius)
	{
		c_foc[index] = make_vec4(0, 0, 0, 0);
		c_mom[index] = make_vec4(0, 0, 0, 0);
		return;
	}

	if(abs(proj_dr) > 0.5 * cyl_params.length && perp_dr > cyl_params.radius)
	{
		virtual_drv = 0.5 * sign(proj_dr) * cyl_params.length * com_params.dirc_para + perp_drv * (cyl_params.radius / perp_dr);
		virtual_radius = 0;
		goto spheresphere;
	}

	if(abs(proj_dr) > 0.5 * cyl_params.length && perp_dr < cyl_params.radius)
	{
		virtual_drv = 0.5 * sign(proj_dr) * cyl_params.length * com_params.dirc_para + perp_drv;
		virtual_radius = 0;
		goto spheresphere;
	}

	if(abs(proj_dr) < 0.5 * cyl_params.length)
	{
		virtual_drv = com_params.dirc_para * proj_dr;
		virtual_radius = cyl_params.radius;
		goto spheresphere;
	}
spheresphere:
	virtual_pos = com_params.pos + virtual_drv;
	virtual_vel = com_params.vel + cross(com_params.ome, virtual_drv);
	CollideSphereSphere(cyl_params.mparams, sphere_pos, virtual_pos, sphere_vel, virtual_vel, sphere_ome, virtual_ome, sphere_radius, virtual_radius, forcei, momenti);
	c_foc[index] = make_vec4(-forcei.x, -forcei.y, -forcei.z, 0.0);
	vec3 momentj = -cross(virtual_drv, forcei);
	c_mom[index] = make_vec4(momentj.x, momentj.y, momentj.z, 0.0);
    p_mom[index] += make_vec4(momenti.x, momenti.y, momenti.z, 0.0);
	return;
}

void CudaCollideCylinderSphere(vec4* p_pos,
	  	  	  	  	  	  	   vec4* p_vel,
                               vec4* p_ome,
	  	  	  	  	  	  	   uint  num_par,
	  	  	  	  	  	  	   MD_Shape_Params* p_params,
	  	  	  	  	  	  	   MD_Cylinder_Params* c_params,
	  	  	  	  	  	  	   vec4* c_foc,
	  	  	  	  	  	  	   vec4* c_mom,
                               vec4* p_mom)
{
	uint nblocks, nthreads;
	ComputeGridSize(num_par, WORK_SIZE, nblocks, nthreads);
	CollideCylinderSphere<<<nblocks, nthreads>>>(p_pos, p_vel, p_ome, num_par, p_params, c_params, c_foc, c_mom, p_mom);
	KernelError("CudaCollideSphereCylinder");
//	std::cout << force.x << ", " << force.y << ", " << force.z << std::endl;
}	  	   
