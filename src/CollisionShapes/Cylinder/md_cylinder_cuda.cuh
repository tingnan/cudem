/*
 * md_shape.cuh
 *
 *  Created on: Jan 14, 2013
 *      Author: tingnan
 */

#ifndef _MD_CYLINDER_CUH_
#define _MD_CYLINDER_CUH_

#include "md_shape_cuda.cuh"

struct MD_Cylinder_Params
{
    FloatType radius;
    FloatType length;
    MD_Mech_Params mparams;
};

void CudaCollideCylinderSphere(vec4* p_pos,
					  	  	   vec4* p_vel,
                               vec4* p_ome,
					  	  	   uint  num_par,
					  	  	   MD_Shape_Params*    p_params,
					  	  	   MD_Cylinder_Params* c_params,
	  	  	  	  	  	  	   vec4* c_foc,
	  	  	  	  	  	  	   vec4* c_mom,
                               vec4* p_mom);
	  	  	  	  	  	  	   
#endif /* _MD_SHAPE_CUH_ */
