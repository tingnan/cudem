/*
 * File:   MDShape.cc
 * Author: tingnan
 * 
 * Created on February 8, 2011, 10:12 PM
 */

#include "md_sphere.h"
#include "MathHelper/md_utility.h"
#include <cstring>
#include <cassert>

#ifdef __ENABLE_CUDA__

#include "md_sphere_cuda.cuh"

DerivedRegister<MDSphere> MDSphere::reg_("Sphere");

MDSphere::MDSphere()
{
	MD_CudaMalloc<MD_Sphere_Params>(&d_sph_params_, &h_sph_params_);
}

MDSphere::MDSphere(MD_Sphere_Params& input)
{
	MD_CudaMalloc<MD_Sphere_Params>(&d_sph_params_, &h_sph_params_);
	SphereInit(input);
}

void MDSphere::IntersectSphere(vec4* p_pos,       // input
							 vec4* p_vel,       // input
                             vec4* p_ome,
							 uint  num_par,
							 vec4* c_foc,
							 vec4* c_mom,
                             vec4* p_mom)
{
	MDShape_MemCpyHostToDevice();
	CudaCollideSphereSphere(p_pos, p_vel, num_par, d_params_, d_sph_params_, c_foc, c_mom);
	MDShape_MemCpyDeviceToHost();
}


void MDSphere::SphereInit(MD_Sphere_Params& input)
{
	memcpy(h_sph_params_, &input, sizeof(MD_Sphere_Params));
	MD_CudaMemCpyHostToDevice<MD_Sphere_Params>(d_sph_params_, h_sph_params_);
}

void MDSphere::SetNodeOriMat(const mat3& orientationMatrix)
{
	vec3 para = make_vec3(1,  0,  0);
	vec3 perp = make_vec3(0,  1,  0);
	memcpy(&h_params_->ori, orientationMatrix, 9 * sizeof(FloatType));
	h_params_->dirc_para.x =
	h_params_->ori[0][0] * para.x + h_params_->ori[0][1] * para.y +
	h_params_->ori[0][2] * para.z;
	h_params_->dirc_para.y =
	h_params_->ori[1][0] * para.x + h_params_->ori[1][1] * para.y +
	h_params_->ori[1][2] * para.z;
	h_params_->dirc_para.z =
	h_params_->ori[2][0] * para.x + h_params_->ori[2][1] * para.y +
	h_params_->ori[2][2] * para.z;
	h_params_->dirc_perp.x =
	h_params_->ori[0][0] * perp.x + h_params_->ori[0][1] * perp.y +
	h_params_->ori[0][2] * perp.z;
	h_params_->dirc_perp.y =
	h_params_->ori[1][0] * perp.x + h_params_->ori[1][1] * perp.y +
	h_params_->ori[1][2] * perp.z;
	h_params_->dirc_perp.z =
	h_params_->ori[2][0] * perp.x + h_params_->ori[2][1] * perp.y +
	h_params_->ori[2][2] * perp.z;
	h_params_->dirc_orth = cross(h_params_->dirc_para, h_params_->dirc_perp);
}

bool MDSphere::RFTiDivSegments(const vec3& plane_norm, FloatType plane_dist, std::vector<vec3>& pos_list, std::vector<vec3>& ori_list, std::vector<vec3>& nor_list, std::vector<vec3>& vel_list, std::vector<FloatType>& are_list)
{
	FloatType s_angle = -hc_math_pi;
	FloatType e_angle =  hc_math_pi;
	bool ifcollision = false;
	int  npiece = pos_list.size();
	for (int i = 0; i < npiece; i++)
	{
		FloatType tmpangle = s_angle + (e_angle - s_angle) * i / FloatType(npiece);
		vec3 surf_drv = h_sph_params_->radius * rot_axis(h_params_->dirc_para, h_params_->dirc_perp, tmpangle);
		ori_list[i] = surf_drv / h_sph_params_->radius;
        nor_list[i] = ori_list[i];
		pos_list[i] = h_params_->pos + surf_drv;
		vel_list[i] = h_params_->vel + cross(h_params_->ome, surf_drv);
		are_list[i] = (FloatType(h_sph_params_->radius * h_sph_params_->radius * (e_angle - s_angle) / FloatType(npiece)));
		if (dot(plane_norm, pos_list[i]) < plane_dist) ifcollision = true;
	}
	return ifcollision;
}

void MDSphere::GetNodeAxisY(vec3& ydir)
{
	 ydir = h_params_->dirc_perp;
}


void MDSphere::ParseParams()
{
    std::map<std::string, FloatType>::iterator it;
    if((it = geo_map_.find("radius")) == geo_map_.end())
    {
        std::cerr << "warning: radius is not specified for sphere \n";
    }
    else
    {
        h_sph_params_->radius = it->second;
    }
    if((it = geo_map_.find("mu")) == geo_map_.end())
    {
        std::cerr << "warning: mu is not specified for sphere \n";
    }
    else
    {
        h_sph_params_->mparams.mu = it->second;
    }
    if((it = geo_map_.find("gn")) == geo_map_.end())
    {
        std::cerr << "warning: gn is not specified for sphere \n";
    }
    else
    {
        h_sph_params_->mparams.gn = it->second;
    }
    if((it = geo_map_.find("kn")) == geo_map_.end())
    {
        std::cerr << "warning: kn is not specified for sphere \n";
    }
    else
    {
        h_sph_params_->mparams.kn = it->second;
    }
    MD_CudaMemCpyHostToDevice<MD_Sphere_Params>(d_sph_params_, h_sph_params_);
}

#endif
