/* 
 * File:   MDShape.h
 * Author: tingnan
 *
 * Created on February 8, 2011, 10:12 PM
 * 
 * The MDShape class is defined here. basically a surface is composed of
 * 
 */

#ifndef _MD_SPHERE_H_
#define	_MD_SPHERE_H_
#include "md_shape.h"


#ifdef __ENABLE_CUDA__

class MDSphere : public MDShapeBase
{
protected:
    static DerivedRegister<MDSphere> reg_;
	struct MD_Sphere_Params* h_sph_params_;
	struct MD_Sphere_Params* d_sph_params_;
public:
	MDSphere();
	MDSphere(struct MD_Sphere_Params&);
	void GetNodeAxisY(vec3&);
	void IntersectSphere(vec4*, vec4*, vec4*, uint, vec4*, vec4*, vec4*);
    void SetNodeOriMat(const mat3&);
    void SphereInit(struct MD_Sphere_Params&);
    bool RFTiDivSegments(const vec3&, FloatType, std::vector<vec3>&, std::vector<vec3>&, std::vector<vec3>&, std::vector<vec3>&, std::vector<FloatType>&);
    void ParseParams();
};

#endif
#endif /* MDSHAPE_H */
