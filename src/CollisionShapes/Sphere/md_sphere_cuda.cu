#include "md_sphere_cuda.cuh"
#include "GeneralHelper/md_cuda_helper.cuh"
#include "MathHelper/md_vec_cuda.cuh"

__global__ void CollideSpheres(vec4* p_pos,
					  	  	   vec4* p_vel,
					  	  	   uint  num_par,
					  	  	   MD_Shape_Params*  p_params,
					  	  	   MD_Sphere_Params* s_params,
					  	  	   vec4* c_foc,
					  	  	   vec4* c_mom)
{
	uint index = iIndex();
	if(index >= num_par) return;

	// init local parameters;
	vec4 pos = p_pos[index];
	vec4 vel = p_vel[index];
	vec3 sphere_pos = make_vec3(pos.x, pos.y, pos.z);
	vec3 sphere_vel = make_vec3(vel.x, vel.y, vel.z);
	FloatType sphere_radius = pos.w;
	MD_Shape_Params  com_params = p_params[0];
	MD_Sphere_Params sph_params = s_params[0];
	
	vec3 dr = sphere_pos - com_params.pos;
		
	vec3 virtual_drv = make_vec3(0.0, 0.0, 0.0);
	vec3 virtual_pos = make_vec3(0.0, 0.0, 0.0);
	vec3 virtual_vel = make_vec3(0.0, 0.0, 0.0);
	vec3 force = make_vec3(0.0, 0.0, 0.0);
	FloatType virtual_radius = 0;
	
	FloatType dist = sqrt(dot(dr, dr));
	if(dist > sph_params.radius + sphere_radius)
		return;
    if(dist < sph_params.radius)
        c_foc[index] = make_vec4(1e9, 1e9, 1e9, 0.0);
	
	virtual_drv = dr / dist * sph_params.radius;
	virtual_pos = com_params.pos + virtual_drv;
	virtual_vel = com_params.vel + cross(com_params.ome, virtual_drv);
	force = CollideSphereSphere(sph_params.mparams, sphere_pos, virtual_pos, sphere_vel, virtual_vel, sphere_radius, virtual_radius);
	vec4 myfoc = make_vec4(force.x, force.y, force.z, 0.0);
	c_foc[index] = myfoc;
	vec3 moment = cross(virtual_drv, force);
	vec4 mymom = make_vec4(moment.x, moment.y, moment.z, 0.0);
	c_mom[index] = mymom;
}

void CudaCollideSphereSphere(vec4* p_pos,
					  	   vec4* p_vel,
					  	   uint  num_par,
					  	   MD_Shape_Params*  p_params,
					  	   MD_Sphere_Params* s_params,
	  	  	  	  	  	   vec4* c_foc,
	  	  	  	  	  	   vec4* c_mom)
{
	uint nblocks, nthreads;
	ComputeGridSize(num_par, WORK_SIZE, nblocks, nthreads);
	CollideSpheres<<<nblocks, nthreads>>>(p_pos, p_vel, num_par, p_params, s_params, c_foc, c_mom);
	KernelError("CudaCollideSpheres");
}
	  	  	  	  	  	   
	  	  	  	  	  	   
