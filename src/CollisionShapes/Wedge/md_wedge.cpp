/*
 * File:   MDShape.cc
 * Author: tingnan
 * 
 * Created on February 8, 2011, 10:12 PM
 */

#include "md_wedge.h"
#include "MathHelper/md_utility.h"
#include <cstring>
#include <cassert>

#ifdef __ENABLE_CUDA__

#include "md_wedge_cuda.cuh"

DerivedRegister<MDWedge> MDWedge::reg_("Wedge");

MDWedge::MDWedge()
{
    MD_CudaMalloc<MD_Wedge_Params>(&d_wed_params_, &h_wed_params_);
}

MDWedge::MDWedge(MD_Wedge_Params& input)
{
    MD_CudaMalloc<MD_Wedge_Params>(&d_wed_params_, &h_wed_params_);
    WedgeInit(input);
}

void MDWedge::IntersectSphere(vec4* p_pos,       // input
                              vec4* p_vel,       // input
                              vec4* p_ome,
                              uint  num_par,
                              vec4* c_foc,
                              vec4* c_mom,
                              vec4* p_mom)
{
    MDShape_MemCpyHostToDevice();
    CudaCollideWedgeSphere(p_pos, p_vel, num_par, d_params_, d_wed_params_, c_foc, c_mom);
    MDShape_MemCpyDeviceToHost();
}

void MDWedge::WedgeInit(MD_Wedge_Params& input)
{
    memcpy(h_wed_params_, &input, sizeof(MD_Wedge_Params));
    MD_CudaMemCpyHostToDevice<MD_Wedge_Params>(d_wed_params_, h_wed_params_);
}

void MDWedge::SetNodeOriMat(const mat3& orientationMatrix)
{
    vec3 para = make_vec3(1, 0, 0);
    vec3 perp = make_vec3(0, 1, 0);
    memcpy(&h_params_->ori, orientationMatrix, 9 * sizeof(FloatType));
    h_params_->dirc_para.x =
    h_params_->ori[0][0] * para.x + h_params_->ori[0][1] * para.y +
    h_params_->ori[0][2] * para.z;
    h_params_->dirc_para.y =
    h_params_->ori[1][0] * para.x + h_params_->ori[1][1] * para.y +
    h_params_->ori[1][2] * para.z;
    h_params_->dirc_para.z =
    h_params_->ori[2][0] * para.x + h_params_->ori[2][1] * para.y +
    h_params_->ori[2][2] * para.z;
    h_params_->dirc_perp.x =
    h_params_->ori[0][0] * perp.x + h_params_->ori[0][1] * perp.y +
    h_params_->ori[0][2] * perp.z;
    h_params_->dirc_perp.y =
    h_params_->ori[1][0] * perp.x + h_params_->ori[1][1] * perp.y +
    h_params_->ori[1][2] * perp.z;
    h_params_->dirc_perp.z =
    h_params_->ori[2][0] * perp.x + h_params_->ori[2][1] * perp.y +
    h_params_->ori[2][2] * perp.z;
    h_params_->dirc_orth = cross(h_params_->dirc_para, h_params_->dirc_perp);
}

bool MDWedge::RFTiDivSegments(const vec3& plane_dirc, FloatType plane_dist, std::vector<vec3>& pos_list, std::vector<vec3>& ori_list, std::vector<vec3>& nor_list, std::vector<vec3>& vel_list, std::vector<FloatType>& are_list)
{
    bool ifcollision = false;
    int npiece = pos_list.size();
    div_t divresult = div(npiece, 4);
    int npiece_b = divresult.quot + divresult.rem;
    int i;
    
    for (i = 0; i < npiece_b; i++)
    {
        vec3 disp = h_params_->dirc_para * ( (i + 0.5) * h_wed_params_->lx / double(npiece_b) - h_wed_params_->lx / 2.0) - (0.5 * h_wed_params_->lz) * h_params_->dirc_orth;
        ori_list[i] = h_params_->dirc_orth;
        pos_list[i] = h_params_->pos + disp;
        vel_list[i] = h_params_->vel + cross(h_params_->ome, disp);
        nor_list[i] = make_vec3(-ori_list[i].x, -ori_list[i].y, -ori_list[i].z);
        are_list[i] = h_wed_params_->lx * h_wed_params_->ly / double(npiece_b);
        if (dot(pos_list[i], plane_dirc) < plane_dist)
        {
            ifcollision = true;
        }
    }
    
    int npiece_t = divresult.quot;
    for (i = 0; i < npiece_t; i++)
    {
        vec3 disp = h_params_->dirc_para * ( (i + 0.5) * h_wed_params_->lx / double(npiece_t) - h_wed_params_->lx / 2.0) + (0.5 * h_wed_params_->lz) * h_params_->dirc_orth;
        ori_list[i + npiece_b] = h_params_->dirc_orth;
        pos_list[i + npiece_b] = h_params_->pos + disp;
        vel_list[i + npiece_b] = h_params_->vel + cross(h_params_->ome, disp);
        nor_list[i + npiece_b] = h_params_->dirc_orth;
        are_list[i + npiece_b] = h_wed_params_->lx * h_wed_params_->ly / double(npiece_t);
        if (dot(pos_list[i + npiece_b], plane_dirc) < plane_dist)
        {
            ifcollision = true;
        }
    }
    
    int npiece_f = divresult.quot;
    for (i = 0; i < npiece_f; ++i)
    {
        vec3 disp = h_params_->dirc_orth * ( (i + 0.5) * h_wed_params_->lz / double(npiece_f) - h_wed_params_->lz / 2.0) + (0.5 * h_wed_params_->lx) * h_params_->dirc_para;
        ori_list[i + npiece_b + npiece_t] = h_params_->dirc_para;
        pos_list[i + npiece_b + npiece_t] = h_params_->pos + disp;
        vel_list[i + npiece_b + npiece_t] = h_params_->vel + cross(h_params_->ome, disp);
        nor_list[i + npiece_b + npiece_t] = h_params_->dirc_para;
        are_list[i + npiece_b + npiece_t] = h_wed_params_->lx * h_wed_params_->ly / double(npiece_f);
        if (dot(pos_list[i + npiece_b + npiece_t], plane_dirc) < plane_dist)
        {
            ifcollision = true;
        }
    }

    int npiece_r = divresult.quot;
    for (i = 0; i < npiece_r; ++i)
    {
        vec3 disp = h_params_->dirc_orth * ( (i + 0.5) * h_wed_params_->lz / double(npiece_r) - h_wed_params_->lz / 2.0) - (0.5 * h_wed_params_->lx) * h_params_->dirc_para;
        ori_list[i + npiece_b + npiece_t + npiece_f] = h_params_->dirc_para;
        pos_list[i + npiece_b + npiece_t + npiece_f] = h_params_->pos + disp;
        vel_list[i + npiece_b + npiece_t + npiece_f] = h_params_->vel + cross(h_params_->ome, disp);
        nor_list[i + npiece_b + npiece_t + npiece_f] = -1.0 * h_params_->dirc_para;
        are_list[i + npiece_b + npiece_t + npiece_f] = h_wed_params_->lx * h_wed_params_->ly / double(npiece_r);
        if (dot(pos_list[i + npiece_b + npiece_t + npiece_f], plane_dirc) < plane_dist)
        {
            ifcollision = true;
        }
    }
    
    return ifcollision;
}

void MDWedge::ParseParams()
{
    std::map<std::string, FloatType>::iterator it; 
    if((it = geo_map_.find("lx")) == geo_map_.end())
    {
        std::cerr << "warning: lx is not specified for wedge \n"; 
    }
    else
    {
        h_wed_params_->lx = it->second;
    }
    if((it = geo_map_.find("ly")) == geo_map_.end())
    {
        std::cerr << "warning: lz is not specified for wedge \n"; 
    }
    else
    {
        h_wed_params_->ly = it->second;
    }
    if((it = geo_map_.find("lz")) == geo_map_.end())
    {
        std::cerr << "warning: lz is not specified for wedge \n"; 
    }
    else
    {
        h_wed_params_->lz = it->second;
    }
    if((it = geo_map_.find("mu")) == geo_map_.end())
    {
        std::cerr << "warning: mu is not specified for wedge \n"; 
    }
    else
    {
        h_wed_params_->mparams.mu = it->second;
    }
    if((it = geo_map_.find("kn")) == geo_map_.end())
    {
        std::cerr << "warning: kn is not specified for wedge \n"; 
    }
    else
    {
        h_wed_params_->mparams.kn = it->second;
    }
    if((it = geo_map_.find("gn")) == geo_map_.end())
    {
        std::cerr << "warning: gn is not specified for wedge \n"; 
    }
    else
    {
        h_wed_params_->mparams.gn = it->second;
    }
    // now copy to GPU
	MD_CudaMemCpyHostToDevice<MD_Wedge_Params>(d_wed_params_, h_wed_params_);    
}

#endif
