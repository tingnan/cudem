/* 
 * File:   MDShape.h
 * Author: tingnan
 *
 * Created on February 8, 2011, 10:12 PM
 * 
 * The MDShape class is defined here. basically a surface is composed of
 * 
 */

#ifndef _MDWEDGE_H_
#define	_MDWEDGE_H_
#include "md_shape.h"

#ifdef __ENABLE_CUDA__

class MDWedge : public MDShapeBase
{
protected:
    static DerivedRegister<MDWedge> reg_;
	struct MD_Wedge_Params* h_wed_params_;
	struct MD_Wedge_Params* d_wed_params_;
public:
	MDWedge();
	MDWedge(struct MD_Wedge_Params&);
	void IntersectSphere(vec4*, vec4*, vec4*, uint, vec4*, vec4*, vec4*);
    void SetNodeOriMat(const mat3&);
    void WedgeInit(struct MD_Wedge_Params&);
    bool RFTiDivSegments(const vec3&, FloatType, std::vector<vec3>&, std::vector<vec3>&, std::vector<vec3>&, std::vector<vec3>&, std::vector<FloatType>&);
    void ParseParams();
};

#endif
#endif /* MDSHAPE_H */
