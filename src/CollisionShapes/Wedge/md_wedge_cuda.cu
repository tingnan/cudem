#include "md_wedge_cuda.cuh"
#include "GeneralHelper/md_cuda_helper.cuh"
#include "MathHelper/md_vec_cuda.cuh"
//#include "cuPrintf.cu"


#define MAX_FORCE 1e9

// sq_norm must be a unit vector; the w component of return vector indicate collision
__device__ vec4 CollisionSquareSphere(vec3 sq_pos, vec3 sq_lx, vec3 sq_ly, FloatType lx, FloatType ly, vec3 sp_pos, FloatType sp_rad)
{
    vec3 dr = sp_pos - sq_pos;
    vec3 sq_norm = cross(sq_lx, sq_ly);
    FloatType proj_norm = dot(dr, sq_norm);
    vec4 cpos = make_vec4(0, 0, 0, 0);
    if(proj_norm >= 0 && proj_norm <= sp_rad) 
    {
        FloatType proj_lx = dot(sq_lx, dr);
        FloatType proj_ly = dot(sq_ly, dr);
        proj_lx = sign(proj_lx) * min(abs(proj_lx), 0.5 * lx);
        proj_ly = sign(proj_ly) * min(abs(proj_ly), 0.5 * ly);
        vec3 tmp = proj_lx * sq_lx + proj_ly * sq_ly + sq_pos;
        cpos.x = tmp.x;
        cpos.y = tmp.y;
        cpos.z = tmp.z;
        cpos.w = 1;
    }
    return cpos;
}



__global__ void CollideWedgeSphere(vec4* p_pos,
                                   vec4* p_vel,
                                   uint  num_par,
                                   MD_Shape_Params* p_params,
                                   MD_Wedge_Params* w_params,
                                   vec4* c_foc,
                                   vec4* c_mom)
{
    uint index = iIndex();
    if(index >= num_par) return;

    // init local parameters;
    vec4 pos = p_pos[index];
    vec4 vel = p_vel[index];
    vec3 sphere_pos = make_vec3(pos.x, pos.y, pos.z);
    vec3 sphere_vel = make_vec3(vel.x, vel.y, vel.z);
    FloatType sphere_radius = pos.w;
    MD_Shape_Params com_params = p_params[0];
    MD_Wedge_Params wed_params = w_params[0];

    vec3 dr = sphere_pos - com_params.pos;
    vec3 virtual_drv = make_vec3(0.0, 0.0, 0.0);
    vec3 virtual_pos = make_vec3(0.0, 0.0, 0.0);
    vec3 virtual_vel = make_vec3(0.0, 0.0, 0.0);
    vec3 force = make_vec3(0.0, 0.0, 0.0);
    FloatType virtual_radius = 0;
    
    // wedge's local frame
    vec3 proj = make_vec3(dot(dr, com_params.dirc_para), dot(dr, com_params.dirc_perp), dot(dr, com_params.dirc_orth));
    vec3 leng = make_vec3(wed_params.lx, wed_params.ly, wed_params.lz);
    
    // out of the cube, this will exclude most of the particles
    if(abs(proj.y) > 0.5 * leng.y + sphere_radius || proj.x < -sphere_radius || proj.x > leng.x + sphere_radius || abs(proj.z) > 0.5 * leng.z + sphere_radius)
    {
        return;
    }
   
    // since triangle collision will not count edge cases, we will run the square collision detection first.
    vec3 sq_ly = make_vec3(0, 1, 0);
    FloatType edge_leng = sqrt(leng.x * leng.x + 0.25 * leng.z * leng.z);
    FloatType cos_lx = leng.x / edge_leng;
    FloatType cos_lz = leng.z / edge_leng * 0.5;
    
    // FloatType inve_sq = 1 / (leng.x * leng.x + leng.y * leng.z * 0.25);
    // FloatType fact_leng = edge_leng * sphere_radius / (0.5 * leng.x * leng.z);
    
    FloatType edge_dst1 = proj.x / leng.x + proj.z / leng.z * 2;
    FloatType edge_dst2 = proj.x / leng.x - proj.z / leng.z * 2;
    
    // FloatType edge_pep1 = proj.x * leng.x - proj.z * leng.z / 2;
    // FloatType edge_pep2 = proj.x * leng.x + proj.z * leng.z / 2;
    
    // for wedge surface 1
    vec3 sq_lx = make_vec3(cos_lx, 0, -cos_lz);
    vec3 sq_pos = make_vec3(leng.x * 0.5, 0, leng.z * 0.25);
    vec4 tmpvec = CollisionSquareSphere(sq_pos, sq_lx, sq_ly, edge_leng, leng.y, proj, sphere_radius);
    if(tmpvec.w == 1)
    {
        virtual_drv = tmpvec.x * com_params.dirc_para + tmpvec.y * com_params.dirc_perp + tmpvec.z * com_params.dirc_orth;
        goto spheresphere;
    }
    
    // for wedge surface 2
    sq_lx = make_vec3(-cos_lx, 0, -cos_lz);
    sq_pos = make_vec3(leng.x * 0.5, 0, -leng.z * 0.25);
    tmpvec = CollisionSquareSphere(sq_pos, sq_lx, sq_ly, edge_leng, leng.y, proj, sphere_radius);
    if(tmpvec.w == 1)
    {
        virtual_drv = tmpvec.x * com_params.dirc_para + tmpvec.y * com_params.dirc_perp + tmpvec.z * com_params.dirc_orth;
        goto spheresphere;
    }
    
    // wedge surface 3
    if(proj.x <= 0 && proj.x >= -sphere_radius)
    {
        virtual_drv = sign(proj.y) * min(abs(proj.y), 0.5 * leng.y) * com_params.dirc_perp + sign(proj.z) * min(abs(proj.z), 0.5 * leng.z) * com_params.dirc_orth;
        goto spheresphere;
    }
    
/*
    if(edge_dst1 >= 1 && edge_dst1 <= 1 + fact_ll)
    {
        virtual_drv.x = leng.x * ( edge_pep1 + leng.z * leng.z * 0.25) * inve_sq;
        virtual_drv.x = min(leng.x, max(0.0, virtual_drv.x));
        virtual_drv.y = sign(proj.y) * min(abs(proj.y), 0.5 * leng.y);
        virtual_drv.z = leng.z * (-edge_pep1 + leng.x * leng.x) * inve_sq;
        virtual_drv.z = min(0.5 * leng.z, max(0.0, virtual_drv.z));
        goto spheresphere;
    }

    if(edge_dst2 >= 1 && edge_dst2 <= 1 + fact_ll)
    {
        virtual_drv.x =  leng.x * ( edge_pep2 + leng.z * leng.z * 0.25) * inve_sq;
        virtual_drv.x = min(leng.x, max(0.0, virtual_drv.x));
        virtual_drv.y =  sign(proj.y) * min(abs(proj.y), 0.5 * leng.y);
        virtual_drv.z = -leng.z * (-edge_pep2 + leng.x * leng.x) * inve_sq;
        virtual_drv.z = max(-0.5 * leng.z, min(0.0, virtual_drv.z));
        goto spheresphere;
    }
*/
    // side
    if(edge_dst1 < 1 && edge_dst2 < 1 && proj.x > 0 && abs(proj.y) >= 0.5 * leng.y)
    {
        virtual_drv = proj.x * com_params.dirc_para + sign(proj.y) * 0.5 * leng.y * com_params.dirc_perp + proj.z * com_params.dirc_orth;
        goto spheresphere;
    }
    
    // inside
    if(edge_dst1 < 1 && edge_dst2 < 1 && proj.x > 0 && abs(proj.y) < 0.5 * leng.y)
    {
        // cuPrintf("Wedge: %d: %f, %f, %f, %f\n", index, pos.x, pos.y, pos.z, pos.w);
        c_foc[index] = make_vec4(MAX_FORCE, MAX_FORCE, MAX_FORCE, 0.0);
        return;
    }
    
spheresphere:
    virtual_pos = com_params.pos + virtual_drv;
    virtual_vel = com_params.vel + cross(com_params.ome, virtual_drv);
    force = CollideSphereSphere(wed_params.mparams, sphere_pos, virtual_pos, sphere_vel, virtual_vel, sphere_radius, virtual_radius);
    vec4 myfoc = make_vec4(force.x, force.y, force.z, 0.0);
    c_foc[index] = myfoc;
    vec3 moment = cross(virtual_drv, force);
    vec4 mymom = make_vec4(moment.x, moment.y, moment.z, 0.0);
    c_mom[index] = mymom;
    
    // if(index == 99852)
    //   cuPrintf("Wedge: %d: (%f, %f, %f, %f), (%f, %f, %f)\n", index, proj.x, proj.y, proj.z, sphere_radius, tmpvec.x, tmpvec.y, tmpvec.z);
    return;
}

void CudaCollideWedgeSphere(vec4* p_pos,
                            vec4* p_vel,
                            uint  num_par,
                            MD_Shape_Params* p_params,
                            MD_Wedge_Params* w_params,
                            vec4* c_foc,
                            vec4* c_mom)
{
    uint nblocks, nthreads;
    ComputeGridSize(num_par, WORK_SIZE, nblocks, nthreads);
    // cudaPrintfInit();
    CollideWedgeSphere<<<nblocks, nthreads>>>(p_pos, p_vel, num_par, p_params, w_params, c_foc, c_mom);
    // cudaPrintfDisplay();
    //cudaPrintfEnd();
    KernelError("CudaCollideSphereWedge");
}          
