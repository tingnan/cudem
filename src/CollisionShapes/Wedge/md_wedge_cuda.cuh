/*
 * md_shape.cuh
 *
 *  Created on: Jan 14, 2013
 *      Author: tingnan
 */

#ifndef _MD_WEDGE_CUH_
#define _MD_WEDGE_CUH_

#include "md_shape_cuda.cuh"

struct MD_Wedge_Params
{
    FloatType lx;
    FloatType ly;
    FloatType lz;
    MD_Mech_Params mparams;
};

void CudaCollideWedgeSphere(vec4* p_pos,
					  	  	 vec4* p_vel,
					  	  	 uint  num_par,
					  	  	 MD_Shape_Params*  p_params,
					  	  	 MD_Wedge_Params* c_params,
	  	  	  	  	  	  	 vec4* c_foc,
	  	  	  	  	  	  	 vec4* c_mom);
	  	  	  	  	  	  	   
#endif /* _MD_SHAPE_CUH_ */
