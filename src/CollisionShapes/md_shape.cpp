/*
 * File:   MDShape.cc
 * Author: tingnan
 * 
 * Created on February 8, 2011, 10:12 PM
 */
#include <GL/glew.h>
#include "CollisionShapes/md_shape.h"
#include "MathHelper/md_utility.h"
#include <cstring>
#include <cassert>

#ifdef __ENABLE_CUDA__
// will need the funciont to copy pointers from d_mem_ structure;

#include "md_shape_params.cuh"

BaseShapeFactory::map_type* BaseShapeFactory::map = 0;


uint OpenGLCreateVBO(uint size)
{
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, size, 0, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    return vbo;
}

uint OpenGLCreateVBOWithData(void* buffer, uint size)
{
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, size, buffer, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    return vbo;
}

void MDShapeBase::GetNodeAngVel(vec3& omega)
{
    omega = h_params_->ome;
}

void MDShapeBase::GetNodeFoc(vec3& force)
{
    force = h_params_->foc;
}

void MDShapeBase::GetNodeMass(FloatType& mass)
{
    mass = h_params_->mass;
}

void MDShapeBase::GetNodeMoF(vec3& moment)
{
    moment = h_params_->mom;
}

void MDShapeBase::GetNodeOriMat(mat3& orientation)
{
    memcpy(&orientation, h_params_->ori, 9 * sizeof(FloatType));
}

void MDShapeBase::GetNodePos(vec3& position)
{
    position = h_params_->pos;
}

void MDShapeBase::GetNodeVel(vec3& velocity)
{
    velocity = h_params_->vel;
}

void MDShapeBase::GetNodeAxisX(vec3& xdir)
{
    xdir = h_params_->dirc_para;
}
void MDShapeBase::GetNodeAxisY(vec3& ydir)
{
    ydir = h_params_->dirc_perp;
}
void MDShapeBase::GetNodeAxisZ(vec3& zdir)
{
    zdir = h_params_->dirc_orth;
}


void MDShapeBase::SetNodeAngVel(const vec3& omega)
{
    h_params_->ome = omega;
}

void MDShapeBase::SetNodeFoc(const vec3& force)
{
    h_params_->foc = force;
}

void MDShapeBase::SetNodeMass(FloatType mass)
{
    h_params_->mass = mass;
}

void MDShapeBase::SetNodeMoF(const vec3& moment)
{
    h_params_->mom = moment;
}

void MDShapeBase::SetNodeOriMat(const mat3& orientationMatrix)
{
    vec3 para = make_vec3(1.0, 0, 0);
    vec3 perp = make_vec3(0, 1.0, 0);
    memcpy(&h_params_->ori, orientationMatrix, 9 * sizeof(FloatType));
    h_params_->dirc_para.x =
    h_params_->ori[0][0] * para.x + h_params_->ori[0][1] * para.y +
    h_params_->ori[0][2] * para.z;
    h_params_->dirc_para.y =
    h_params_->ori[1][0] * para.x + h_params_->ori[1][1] * para.y +
    h_params_->ori[1][2] * para.z;
    h_params_->dirc_para.z =
    h_params_->ori[2][0] * para.x + h_params_->ori[2][1] * para.y +
    h_params_->ori[2][2] * para.z;
    h_params_->dirc_perp.x =
    h_params_->ori[0][0] * perp.x + h_params_->ori[0][1] * perp.y +
    h_params_->ori[0][2] * perp.z;
    h_params_->dirc_perp.y =
    h_params_->ori[1][0] * perp.x + h_params_->ori[1][1] * perp.y +
    h_params_->ori[1][2] * perp.z;
    h_params_->dirc_perp.z =
    h_params_->ori[2][0] * perp.x + h_params_->ori[2][1] * perp.y +
    h_params_->ori[2][2] * perp.z;
    h_params_->dirc_orth = cross(h_params_->dirc_para, h_params_->dirc_perp);

}

void MDShapeBase::SetNodeOriVec(const vec3& para, const vec3& perp, const vec3& orth)
{
    h_params_->dirc_para = para;
    h_params_->dirc_perp = perp;
    h_params_->dirc_orth = orth;
    h_params_->ori[0][0] = para.x;
    h_params_->ori[1][0] = para.y;
    h_params_->ori[2][0] = para.z;
    h_params_->ori[0][1] = perp.x;
    h_params_->ori[1][1] = perp.y;
    h_params_->ori[2][1] = perp.z;
    h_params_->ori[0][2] = orth.x;
    h_params_->ori[1][2] = orth.y;
    h_params_->ori[2][2] = orth.z;

} 

void MDShapeBase::SetNodePos(const vec3& position)
{
    h_params_->pos = position;
}

void MDShapeBase::SetNodeVel(const vec3& velocity)
{
    h_params_->vel = velocity;
}


void MDShapeBase::IntersectSphere(vec4* p_pos, vec4* p_vel, vec4* p_ome, uint  num_par, vec4* c_foc, vec4* c_mom, vec4* p_mom)       // output
{
    // this function will call the appropriate CUDA kernel related to the shape of the object
    // no_op;
}

void MDShapeBase::IntersectRFTObj(class MD_RFT*)
{
    // no_op
}


void MDShapeBase::InitNode(const vec3&, const vec3&, const vec3&, const mat3&)
{

}

MDShapeBase::MDShapeBase()
{
    MD_CudaMalloc<MD_Shape_Params>(&d_params_, &h_params_);
    std::cout << "const: " << h_params_ << std::endl;
    for(int i = 0; i < 3; ++i)
    {
        h_params_->ori[i][i] = 1;
    }
    h_params_->dirc_para = make_vec3(1.0, 0.0, 0.0);
    h_params_->dirc_perp = make_vec3(0.0, 1.0, 0.0);
    h_params_->dirc_orth = make_vec3(0.0, 0.0, 1.0);

    // initialize the visual asset
    gl_mesh_ = new MDGLMesh();
}

MDShapeBase::~MDShapeBase()
{
    // delete cuda memory;
    delete gl_mesh_;
    // free cuda memory;
    // to be done
}

void MDShapeBase::MDShape_MemCpyHostToDevice()
{
    MD_CudaMemCpyHostToDevice<MD_Shape_Params>(d_params_, h_params_);
}

void MDShapeBase::MDShape_MemCpyDeviceToHost()
{
    MD_CudaMemCpyDeviceToHost<MD_Shape_Params>(h_params_, d_params_);
}

void MDShapeBase::SetNodeFocList(const vlist& force_list)
{
    force_list_ = force_list;
}

void MDShapeBase::GetNodeFocList(vlist& force_list)
{
    force_list = force_list_;
}


bool MDShapeBase::RFTiDivSegments(const vec3&, FloatType, vlist&, vlist&, vlist&, vlist&, flist&)
{
    return false;
}

void MDShapeBase::FetchParams(const std::pair<std::string, FloatType>& mpair)
{
    geo_map_.insert(mpair);
}

void MDShapeBase::ParseParams()
{
    //no_op;
}


//---------------------------------------------------------------

#endif


