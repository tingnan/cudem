/* 
 * File:   MDShape.h
 * Author: tingnan
 *
 * Created on February 8, 2011, 10:12 PM
 * 
 * The MDShape class is defined here. basically a surface is composed of
 * 
 */

#ifndef _MDSHAPE_H_
#define	_MDSHAPE_H_
#include "md_header.h"

#ifdef __ENABLE_CUDA__

#include <vector>
#include <map>
#include "MathHelper/md_vec_cuda.cuh"
#include "Scene/md_mesh.h"

// will be put into a utility part at some point
uint OpenGLCreateVBO(uint size);
uint OpenGLCreateVBOWithData(void* buffer, uint size);

class MDShape
{
    friend class GLApp;
protected:
    typedef std::vector<vec3> vlist;
    typedef std::vector<FloatType> flist;
    // bind the assets, also only called from GLApp
    virtual void _bindGLAsset() = 0;
    virtual class MDGLMesh *_getMesh() = 0;
public:
	virtual ~MDShape() {};
    virtual bool RFTiDivSegments(const vec3&, FloatType, vlist&, vlist&, vlist&, vlist&, flist&) = 0;
    virtual void IntersectSphere(vec4*, vec4*, vec4*, uint, vec4*, vec4*, vec4*) = 0;
    virtual void SetNodeMass(FloatType) = 0;
    virtual void GetNodeMass(FloatType&) = 0;
    virtual void GetNodeFoc(vec3&) = 0;
    virtual void GetNodePos(vec3&) = 0;
    virtual void GetNodeVel(vec3&) = 0;
    virtual void GetNodeAngVel(vec3&) = 0;
    virtual void GetNodeOriMat(mat3&) = 0;
    virtual void GetNodeMoF(vec3&) = 0;
	virtual void GetNodeAxisX(vec3&) = 0;
	virtual void GetNodeAxisY(vec3&) = 0;
	virtual void GetNodeAxisZ(vec3&) = 0;
    virtual void SetNodePos(const vec3&) = 0;
    virtual void SetNodeVel(const vec3&) = 0;
    virtual void SetNodeFoc(const vec3&) = 0;
    virtual void SetNodeMoF(const vec3&) = 0;
    virtual void SetNodeAngVel(const vec3&) = 0;
    virtual void SetNodeOriMat(const mat3&) = 0;
    virtual void SetNodeOriVec(const vec3&, const vec3&, const vec3&) = 0;
    virtual void SetNodeFocList(const vlist&) = 0;
    virtual void GetNodeFocList(vlist&) = 0;
    virtual void InitNode(const vec3&, const vec3&, const vec3&, const mat3&) = 0;
    virtual void FetchParams(const std::pair<std::string, FloatType>&) = 0;
    virtual void ParseParams() = 0;
    // get parameter related to the geometry
    virtual struct MD_Shape_Params* GetPhysicsParams() = 0;
    virtual void* GetGeometryParams() = 0;
};

template<class T> MDShape* CreateInstance() 
{
    return new T;
}

class BaseShapeFactory
{
    typedef std::map<std::string, MDShape*(*)()> map_type;
private:
    static map_type* map;
protected:
    static map_type* GetMap()
    {
        if(!map) 
        {
            map = new map_type;
        }
        return map;
    }
public:
    static MDShape* CreateShape(const std::string& s)
    {
        map_type::iterator it = GetMap()->find(s);
        if(it == GetMap()->end())
        {
            return NULL;
        }
        return it->second();
    }
    static MDShape* CreateShape(const char* s)
    {
        std::string sname(s);
        return CreateShape(sname);
    }
};

template<class T> class DerivedRegister : public BaseShapeFactory
{
public:
    DerivedRegister(const std::string& s)
    {
        GetMap()->insert(std::make_pair(s, &CreateInstance<T>));
    }
};


class MDShapeBase: public MDShape
{
protected:
	struct MD_Shape_Params* h_params_;
	struct MD_Shape_Params* d_params_;
    std::map<std::string, FloatType> geo_map_;
    vlist force_list_;
	void MDShape_MemCpyHostToDevice();
	void MDShape_MemCpyDeviceToHost();
    // cannot instantiate without derived
    MDShapeBase();
    virtual ~MDShapeBase();

    // for drawing only, the visual assets
    void _bindGLAsset() {}
    class MDGLMesh *gl_mesh_;
    class MDGLMesh *_getMesh() { return gl_mesh_;}
public:
    
    bool RFTiDivSegments(const vec3&, FloatType, vlist&, vlist&, vlist&, vlist&, flist&);
    void IntersectRFTObj(class MD_RFT*);
    void IntersectSphere(vec4*, vec4*, vec4*, uint, vec4*, vec4*, vec4*);
    void SetNodeMass(FloatType);
    void GetNodeMass(FloatType&);
    void GetNodeFoc(vec3&);
    void GetNodePos(vec3&);
    void GetNodeVel(vec3&);
    void GetNodeAngVel(vec3&);
    void GetNodeOriMat(mat3&);
	void GetNodeAxisX(vec3&);
	void GetNodeAxisY(vec3&);
	void GetNodeAxisZ(vec3&);
    void GetNodeMoF(vec3&);
    void SetNodePos(const vec3&);
    void SetNodeVel(const vec3&);
    void SetNodeAngVel(const vec3&);
    void SetNodeFoc(const vec3&);
    void SetNodeOriMat(const mat3&);
    void SetNodeMoF(const vec3&);
    void SetNodeFocList(const vlist&);
    void GetNodeFocList(vlist&);
    void SetNodeOriVec(const vec3&, const vec3&, const vec3&);
    void InitNode(const vec3&, const vec3&, const vec3&, const mat3&);
    void FetchParams(const std::pair<std::string, FloatType>&);
    void ParseParams();
    struct MD_Shape_Params* GetPhysicsParams() { return h_params_;}
    void* GetGeometryParams() {return  NULL;}
};

#endif

#endif /* MDSHAPE_H */
