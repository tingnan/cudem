/*
 * md_shape.cuh
 *
 *  Created on: Jan 14, 2013
 *      Author: tingnan
 */

#ifndef _MD_SHAPE_CUDA_CUH_
#define _MD_SHAPE_CUDA_CUH_
#include "md_shape_params.cuh"
#include "MathHelper/md_vec_cuda.cuh"
#include "MemoryManagement/md_cudamem.cuh"


static __device__ void CollideSphereSphere(MD_Mech_Params mparams,
                                           vec3 posi, 
                                           vec3 posj,
                                           vec3 veli, 
                                           vec3 velj,
                                           vec3 omei, 
                                           vec3 omej,
                                           FloatType ri, 
                                           FloatType rj,
                                           vec3& force,
                                           vec3& moment)
{
    vec3 dr_ji = posj - posi;
    FloatType dist = length(dr_ji);
    FloatType collide_dist = ri + rj;
    if (dist < collide_dist)
    {
        vec3 norm = dr_ji / dist;
        vec3 tang = make_vec3(0.0, 0.0, 0.0);
        vec3 dv_ji = velj - veli;
        vec3 dv_tg = dv_ji - dot(dv_ji, norm) * norm - (cross(omej, norm) * rj + cross(omei, norm) * ri );
        FloatType abs_vt = length(dv_tg);
        if(abs_vt > 1e-8)
        {
            tang = dv_tg / abs_vt;
        }
        FloatType abs_vn = dot(dv_ji, norm);
        FloatType overlap = collide_dist - dist;
        FloatType abs_Fn = sqrt(overlap) * (mparams.kn * overlap - mparams.gn * abs_vn);
        FloatType abs_Fs = mparams.mu * abs_Fn;
        force  = -abs_Fn * norm + abs_Fs * tang;
        moment = (ri * abs_Fs) * cross(norm, tang);
    }
}


static __device__ vec3 CollideSphereSphere(MD_Mech_Params mparams,
                                    vec3 posi, vec3 posj,
                                    vec3 veli, vec3 velj,
                                    FloatType ri, FloatType rj)
{
    vec3 dr_ji = posj - posi;
    FloatType dist = length(dr_ji);
    FloatType collide_dist = ri + rj;
    vec3 force = make_vec3(0.0, 0.0, 0.0);
    if (dist < collide_dist)
    {
        vec3 norm = dr_ji / dist;
        vec3 tang = make_vec3(0.0, 0.0, 0.0);
        vec3 dv_ji = velj - veli;
        vec3 dv_tg = dv_ji - dot(dv_ji, norm) * norm;
        FloatType abs_vt = length(dv_tg);
        if(abs_vt > 1e-8)
        {
            tang = dv_tg / abs_vt;
        }
        FloatType abs_vn = dot(dv_ji, norm);
        FloatType overlap = collide_dist - dist;
        FloatType abs_Fn = sqrt(overlap) * (mparams.kn * overlap - mparams.gn * abs_vn);
        FloatType abs_Fs = mparams.mu * abs_Fn;
        force = abs_Fn * norm - abs_Fs * tang; // this is the fore on j, for the force on i we need a minus sign
    }
    return force;
}

__device__ static vec4 CollideSquareSphereLocal(FloatType lx, FloatType ly, FloatType shiftz, int pm, vec3 s_pos, FloatType radius)
{
    vec4 virtual_pos = make_vec4(0, 0, 0, -1);
    if(abs(s_pos.z - shiftz) < radius && (s_pos.z - shiftz) * pm >= 0)
    {
        virtual_pos.z = shiftz;
        
        if(abs(s_pos.x) >= lx / 2.0 && abs(s_pos.x) <= lx / 2.0 + radius)
        {
            virtual_pos.x = sign(s_pos.x) * lx * 0.5;
            virtual_pos.w += 1;
        }
        
        if(abs(s_pos.x) < lx / 2.0)
        {
            virtual_pos.x = s_pos.x;
            virtual_pos.w += 1;
        }
        
        if(abs(s_pos.y) >= ly / 2.0 && abs(s_pos.y) <= ly / 2.0 + radius)
        {
            virtual_pos.y = sign(s_pos.y) * ly * 0.5;
            virtual_pos.w += 1;
        }
        
        if(abs(s_pos.y) < ly / 2.0)
        {
            virtual_pos.y = s_pos.y;
            virtual_pos.w += 1;
        }
    }
    return virtual_pos;
}


#endif /* _MD_SHAPE_CUH_ */
