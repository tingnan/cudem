/*
 * md_shape.cuh
 *
 *  Created on: Jan 14, 2013
 *      Author: tingnan
 */

#ifndef _MD_SHAPE_CUH_
#define _MD_SHAPE_CUH_
#include "MathHelper/md_vec_cuda.cuh"
#include "MemoryManagement/md_cudamem.cuh"

struct MD_Shape_Params
{
    FloatType mass;
    vec3 pos;
    vec3 vel;
    vec3 eul;
    vec3 ome;
    vec3 mom;
    vec3 foc;
    mat3 ori;
    vec3 dirc_para;
    vec3 dirc_perp;
    vec3 dirc_orth;
    mat3 moi;
};

struct MD_Mech_Params
{
    FloatType gn;
    FloatType kn;
    FloatType mu;
};

#endif /* _MD_SHAPE_CUH_ */
