/* 
 * File:   MDsand.h
 * Author: tingnan
 *
 * Created on January 14, 2011, 1:42 PM
 */

#ifndef _MDSAND_H_
#define _MDSAND_H_
#include "md_header.h"
#include "HDF/md_hdf5.h"

#ifdef __ENABLE_CUDA__

#include "md_sand_cuda.cuh"

class MDSimpleSand
{
protected:
    SimParams params_;
    MD_Particle_Memory dh_mem_;
    uint num_part_;
    uint num_cell_;
    uint sys_step_;
    uint ios_step_;
	std::vector<uint> bdindex_;
    MD_HDF5_Handle fhandle_;
    void InitGridHash();
    void SortGridHash();
    void CollideCells();
    void IntegratePar();
    void BoundaryCond();
    void Finalize();
public:
	MDSimpleSand();
    ~MDSimpleSand();
    void Precompute();
    void OutputFile();
    void LoadFile(const char*);
    void PPCollidePhase();
    void IntegratePhase();
    void ResizeSandArray(FloatType*);
    void FetchParams();
    inline uint NumParts() {return num_part_;};
    inline uint CurrStep() {return sys_step_;};
    uint CopyDevicePtr(FloatType** p_foc, FloatType** p_mom, FloatType** p_pos, FloatType** p_vel);
};

#endif

#endif
