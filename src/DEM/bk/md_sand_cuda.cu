#include <thrust/sort.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/generate.h>
#include <thrust/sequence.h>
#include <thrust/gather.h>
#include <thrust/scan.h>
#include <thrust/count.h>
#include <thrust/unique.h>
#include <iostream>
#include "GeneralHelper/md_cuda_helper.cuh"
#include "math_constants.h"
#include "md_sand_cuda.cuh"
#include <cassert>
typedef unsigned int uint;

#define USE_TEX 1

#if USE_TEX

#define IFETCH(t, i) tex1Dfetch(t##_TEX, i)

#if USE_SINGLE
#define DFETCH(t, i) tex1Dfetch(t##_TEX, i)
#endif

#if USE_DOUBLE
#define DFETCH(t, i) fetch_double4(t##_TEX, i)
#endif

#else
#define FETCH(t, i) t[i]
#endif

#define MD_DIM_PASS 8 // 2^d where d = 3


#if USE_TEX
#if USE_SINGLE
// textures for particle position and velocity
texture<vec4, 1, cudaReadModeElementType> p_pos_TEX;
texture<vec4, 1, cudaReadModeElementType> p_vel_TEX;
texture<vec4, 1, cudaReadModeElementType> p_foc_TEX;
texture<vec4, 1, cudaReadModeElementType> s_pos_TEX;
texture<vec4, 1, cudaReadModeElementType> s_vel_TEX;
texture<vec4, 1, cudaReadModeElementType> s_foc_TEX;
#endif
#if USE_DOUBLE
texture<int4, 1, cudaReadModeElementType> p_pos_TEX;
texture<int4, 1, cudaReadModeElementType> p_vel_TEX;
texture<int4, 1, cudaReadModeElementType> p_foc_TEX;
texture<int4, 1, cudaReadModeElementType> s_pos_TEX;
texture<int4, 1, cudaReadModeElementType> s_vel_TEX;
texture<int4, 1, cudaReadModeElementType> s_foc_TEX;
#endif

texture<uint, 1, cudaReadModeElementType> s_cell_beg_TEX;
texture<uint, 1, cudaReadModeElementType> s_cell_end_TEX;
texture<uint, 1, cudaReadModeElementType> p_cell_beg_TEX;
texture<uint, 1, cudaReadModeElementType> p_cell_end_TEX;

texture<uint, 1, cudaReadModeElementType> p_grid_hash_TEX;
texture<uint, 1, cudaReadModeElementType> p_grid_indx_TEX;
texture<uint, 1, cudaReadModeElementType> s_grid_hash_TEX;
texture<uint, 1, cudaReadModeElementType> s_grid_indx_TEX;
#endif

__constant__ SimParams params;

__device__ int3 CalcGridPos(vec3 p)
{
	int3 grid_pos;
	grid_pos.x = floor((p.x - params.world_origin.x) / params.cell_size.x);
	grid_pos.y = floor((p.y - params.world_origin.y) / params.cell_size.y);
	grid_pos.z = floor((p.z - params.world_origin.z) / params.cell_size.z);
	return grid_pos;
}

__device__ uint CalcGridHash(int3 grid_pos)
{
	// this is non-collsion hash
	return grid_pos.z * params.grid_size.y * params.grid_size.x + grid_pos.y * params.grid_size.x + grid_pos.x;
}

__device__ uint CalcGridType(int3 grid_pos)
{
	return ((grid_pos.x % 2)) | ((grid_pos.y % 2)<<1) | ((grid_pos.z % 2)<<2);
}


//__device__ __constant__ int md_neighbor_offset [][3] =
//{ { 1, 0, 0}, { 0, 1, 0}, { 0, 0, 1}, {-1, 0, 0}, { 0,-1, 0}, { 0, 0,-1},     \
//  { 1, 1, 0}, { 1, 0, 1}, { 0, 1, 1}, {-1, 1, 0}, {-1, 0, 1}, { 0,-1, 1},     \
//  { 1,-1, 0}, { 0, 1,-1}, { 1, 0,-1}, {-1,-1, 0}, {-1, 0,-1}, { 0,-1,-1},     \
//  { 1, 1, 1}, {-1,-1,-1}, { 1, 1,-1}, { 1,-1, 1}, {-1, 1, 1}, {-1,-1, 1},     \
//  {-1, 1,-1}, { 1,-1,-1}
//};

__device__ void KernelErrorMsg(char* msg)
{
//	cuPrintf("%s\n", msg);
}

__global__ void CalcHashTable(const vec4* pos,            //input
							        uint  num_par,        //input
							        uint* p_grid_hash,    //output array size is num_par
							        uint* p_grid_indx)    //output array size is num_par

{
	uint index = iIndex();
	if (index >= num_par) return;
	vec4 part_pos = pos[index];
	vec3 part_cen = make_vec3(part_pos.x, part_pos.y, part_pos.z);
	int3 grid_pos = CalcGridPos(part_cen);
	p_grid_indx[index] = index;
	p_grid_hash[index] = CalcGridHash(grid_pos);
    // if(p_grid_hash[index] >= params.grid_size.y * params.grid_size.x * params.grid_size.z)
    //    cuPrintf("(%f, %f, %f), %d\n", part_pos.x, part_pos.y, part_pos.z, index);
    // cuPrintf("(%d, %d, %d), %d\n", g_pos.x, g_pos.y, g_pos.z, ctype);
    // cuPrintf("index: %d, hash: %d\t%d\t%d\t%d\n",index, par_grid_hash[index_s], par_grid_hash[index_s + 1], par_grid_hash[index_s + 2], par_grid_hash[index_s + 3]);
}

__global__ void ReorderSortedArray(const uint* s_grid_hash,    // input, needs to be sorted array
		  	  	  	  	  	  	   const uint* s_grid_indx,
								   const vec4* p_pos,
								   const vec4* p_vel,
										 uint  num_par,
									     uint* p_cell_beg,     // output, cell start, need to be filled with 0xffffffff at the beginning
									     uint* p_cell_end,
									     vec4* s_pos,
									     vec4* s_vel)
{
	// using the block shared memory, the size is nthreads + 1
	extern __device__ __shared__ uint shared_hash[];
	uint index = iIndex();
	if (index >= num_par) return;
    uint hash;
    uint thread;
    if (index < num_par)
    {
	    hash = IFETCH(s_grid_hash, index);
	    thread = iThread();
	    shared_hash[thread + 1] = hash;
	    if(thread == 0 && index > 0)
		    shared_hash[0] = IFETCH(s_grid_hash, index - 1);
    }

	syncthreads();

    if (index < num_par)
	{

        if(hash != shared_hash[thread] || index == 0)
	    {
		    p_cell_beg[hash] = index;
		    if(index > 0)
			    p_cell_end[shared_hash[thread]] = index;
	    }
	    if(index == num_par - 1)
		    p_cell_end[hash] = num_par;
	    // reorder pos, vel array.
	    uint sorted_index = IFETCH(s_grid_indx, index);
	    vec4 pos = DFETCH(p_pos, sorted_index);
	    vec4 vel = DFETCH(p_vel, sorted_index);
	    s_pos[index] = pos;
	    s_vel[index] = vel;
//	cuPrintf("(%d, %d), (%f, %f)\n", index, sorted_index, pos.x, s_pos[index].x);
    }
}


//__global__ void CreateUniqueCellTypeArray(const uint* s_cell_beg,
//										  const vec4* s_pos,
//										        uint  num_par,
//										  	  	uint* u_cell_type)
//{
//	uint index = iIndex();
//	if(index >= num_par) return;
//	uint cindx = IFETCH(s_cell_beg, index);
//	if(cindx == 0xffffffff)
//	{
//		u_cell_type[index] = 0xffffffff;
//		return;
//	}
//	vec4 pos = DFETCH(s_pos, cindx);
//	vec3 part_pos = make_vec3(pos.x, pos.y, pos.z);
//	int3 grid_pos = CalcGridPos(part_pos);
//	uint grid_type = CalcGridType(grid_pos);
//	u_cell_type[index] = grid_type;
//}


__device__ void InteracSphereParticle(vec3 posi, vec3 posj,
								  	  vec3 veli, vec3 velj,
                                      vec3 omei, vec3 omej,
								  	  FloatType ri, FloatType rj
                                      vec3& force, vec3& moment);
{
	vec3 dr_ji = posj - posi;
	FloatType dist = length(dr_ji);
	FloatType collide_dist = ri + rj;
	vec3 pairforce = make_vec3(0.0, 0.0, 0.0);
	if (dist < collide_dist)
	{
		vec3 norm = dr_ji / dist;
		vec3 tang = make_vec3(0.0, 0.0, 0.0);
		vec3 dv_ji = velj - veli;
		vec3 dv_tg = dv_ji - dot(dv_ji, norm) * norm  - (cross(omej, norm) * rj + cross(omei, norm) * ri );
		FloatType abs_vt = length(dv_tg);
		if(abs_vt > 1e-8)
		{
			tang = dv_tg / abs_vt;
		}
		FloatType abs_vn = dot(dv_ji, norm);
		FloatType overlap = collide_dist - dist;
        FloatType abs_Fn = sqrt(overlap) * (params.kn * overlap - params.gn * abs_vn);
        FloatType abs_Fs = params.mu * abs_Fn;
        pairforce = -abs_Fn * norm + abs_Fs * tang;
        force  += pairforce;
        moment += (ri * abs_Fs) * cross(norm, tang);
        }
}

__device__ __constant__ int md_neighbor_offset [][3] =
{ { 0, 0, 0}, { 1, 0, 0}, { 0, 1, 0}, { 1, 1, 0}, { 1,-1, 0},
  { 0, 0, 1}, { 1, 0, 1}, { 0, 1, 1}, { 1, 1, 1}, { 1,-1, 1},
  { 1, 0,-1}, { 1, 1,-1}, { 0, 1,-1}, { 1,-1,-1} };


__device__ bool GridLimit(int3 grid_pos)
{
	return (grid_pos.x < 0 || grid_pos.x > params.grid_size.x || grid_pos.y < 0 || grid_pos.y > params.grid_size.y || grid_pos.z < 0 || grid_pos.z > params.grid_size.z);
}

//__global__ void CollideCells(const uint* s_grid_hash,
//							 const uint* s_grid_indx,
//	  		  	  	  	  	 const uint* p_cell_beg,
//	  		  	  	  	  	 const uint* p_cell_end,
//	  		  	  	  	  	 const uint* s_cell_beg,
//	  		  	  	  	  	 const uint* s_cell_end,
//	  	  	  	  	  	  	 const vec4* s_pos,
//	  	  	  	  	  	  	 const vec4* s_vel,
//	  	  	  	  	  	 	   	   uint  num_par,
//	  	  	  	  	  	 	   	   uint  u_cell_beg,
//	  	  	  	  	  	 	   	   uint  u_cell_end,
//	  	  	  	  	  	  	 	   vec4* s_foc)
//{
////  transverse the cell hash using the sorted_cell_beginning array;
//	uint index = iIndex() + u_cell_beg;
//	if(index >= u_cell_end) return;
//
//	uint cindx = IFETCH(s_cell_beg, index);
//	//cuPrintf("%d, %d\n", s_cell_beg[index], cindx);
//	if(cindx == 0xffffffff) return;
//
//	vec4 tmpp = DFETCH(s_pos, cindx);
//	vec3 posi = make_vec3(tmpp.x, tmpp.y, tmpp.z);
//	int3 grid_pos = CalcGridPos(posi);
//	uint grid_type = CalcGridType(grid_pos);
////	cuPrintf("%d\n", grid_type);
//	if(true)
//	{
//		uint hashi = s_grid_hash[cindx];
//		for(uint i = IFETCH(s_cell_beg, index); i < IFETCH(s_cell_end, index); ++i)
//		{
//			tmpp = DFETCH(s_pos, i);
//			posi = make_vec3(tmpp.x, tmpp.y, tmpp.z);
//			vec4 tmpv = DFETCH(s_vel, i);
//			vec3 veli = make_vec3(tmpv.x, tmpv.y, tmpv.z);
//			FloatType radiusi = tmpp.w;
//
//
//			for(uint j = 0; j < 14; ++j)
//			{
//				int3 nbr_pos = grid_pos + make_int3(md_neighbor_offset[j][0], md_neighbor_offset[j][1], md_neighbor_offset[j][2]);
//				if(GridLimit(nbr_pos))
//					continue;
//				uint hashj = CalcGridHash(nbr_pos);
//				uint cbegj = IFETCH(p_cell_beg, hashj);
//				if(cbegj == 0xffffffff) // neighbor cell is empty
//					continue;
//				uint cendj = IFETCH(p_cell_end, hashj);
//				for(uint k = cbegj; k < cendj; ++k)
//				{
//					tempp = DFETCH(s_pos, k);
//					vec3 posk = make_vec3(tempp.x, tempp.y, tempp.z);
//					tempv = DFETCH(s_vel, k);
//					vec3 velk = make_vec3(tempv.x, tempv.y, tempv.z);
//					FloatType radiusk = tempp.w;
//					if(hashj == hashi)
//					{
//						if(k > i)
//						{
//							vec3 force = InteracSphereParticle(posi, posk, veli, velk, radiusi, radiusk);
//							vec4 tmp_vec = make_vec4(force.x, force.y, force.z, 0.0);
//							s_foc[i] -= tmp_vec;
//							s_foc[k] += tmp_vec;
//						}
//					}
//					else
//					{
//
//						vec3 force = InteracSphereParticle(posi, posk, veli, velk, radiusi, radiusk);
//						vec4 tmp_vec = make_vec4(force.x, force.y, force.z, 0.0);
//						s_foc[i] -= tmp_vec;
//						s_foc[k] += tmp_vec;
////						cuPrintf("%d, (%d, %d), (%f, %f), %f\n", pass, i, k, posi.x, posk.x, force.x);
//					}
//				}
//			}
//
//		}
//	}
//
//}

__global__ void CollideBoulders(int bindex, const vec4* p_pos, const vec4* p_vel, uint num_par, vec4* p_foc, vec4* b_foc)
{
	uint index = iIndex();
	// will not interact with itself
	if(index >= num_par || index == bindex) return;
	
	// get the boulder position
	vec4 tmpp = DFETCH(p_pos, bindex);
	vec4 tmpv = DFETCH(p_vel, bindex);	
	vec3 posi = make_vec3(tmpp.x, tmpp.y, tmpp.z);
	vec3 veli = make_vec3(tmpv.x, tmpv.y, tmpv.z);
	FloatType radiusi = tmpp.w;
	// now get the particle position
	tmpp = DFETCH(p_pos, index);
	tmpv = DFETCH(p_vel, index);	
	vec3 posj = make_vec3(tmpp.x, tmpp.y, tmpp.z);
	vec3 velj = make_vec3(tmpv.x, tmpv.y, tmpv.z);
	FloatType radiusj = tmpp.w;	
	
	vec3 force = InteracSphereParticle(posi, posj, veli, velj, radiusi, radiusj);
	p_foc[index] += make_vec4(force.x, force.y, force.z, 0.0);
	b_foc[index] -= make_vec4(force.x, force.y, force.z, 0.0);
}

__global__ void CollideCells(const uint* s_grid_indx,
	  		  	  	  	  	 const uint* p_cell_beg,
	  		  	  	  	  	 const uint* p_cell_end,
	  	  	  	  	  	  	 const vec4* s_pos,
	  	  	  	  	  	  	 const vec4* s_vel,
                             const vec4* s_ome,
	  	  	  	  	  	 	       uint  num_par,
	  	  	  	  	  	  	       vec4* p_foc
                                   vec4* p_mom)
{
	uint index = iIndex();
	if(index >= num_par) return;
	vec4 tmpp = DFETCH(s_pos, index);
	vec3 posi = make_vec3(tmpp.x, tmpp.y, tmpp.z);
	vec4 tmpv = DFETCH(s_vel, index);
	vec3 veli = make_vec3(tmpv.x, tmpv.y, tmpv.z);
	FloatType radiusi = tmpp.w;
	int3 grid_pos = CalcGridPos(posi);
	vec3 force = make_vec3(0.0, 0.0, 0.0);
    vec3 moment = make_vec3(0.0, 0.0, 0.0);
    for (int z=-1; z<=1; z++)
    {
        for (int y=-1; y<=1; y++)
        {
            for (int x=-1; x<=1; x++)
            {
            	int3 nbr_pos = grid_pos + make_int3(x, y, z);
            	if(GridLimit(nbr_pos)) continue;
            	uint hashj = CalcGridHash(nbr_pos);
            	uint cbegj = IFETCH(p_cell_beg, hashj);
            	if(cbegj == 0xffffffff) // neighbor cell is empty
            		continue;
            	uint cendj = IFETCH(p_cell_end, hashj);
            	for(uint j = cbegj; j < cendj; ++j)
            	{
            		if(j == index) continue;
            		tmpp = DFETCH(s_pos, j);
            		vec3 posj = make_vec3(tmpp.x, tmpp.y, tmpp.z);
            		tmpv = DFETCH(s_vel, j);
            		vec3 velj = make_vec3(tmpv.x, tmpv.y, tmpv.z);
            		FloatType radiusj = tmpp.w;
            		InteracSphereParticle(posi, posj, veli, velj, omei, omej, radiusi, radiusj, force, moment);
            	}
            }
        }
    }
    uint original_index = IFETCH(s_grid_indx, index);
    p_foc[original_index] += make_vec4(force.x, force.y, force.z, 0.0);
    p_foc[original_index] += make_vec4(moment.x, moment.y, moment.z, 0.0);
}

__global__ void CalcOverlap( FloatType factor,
                             const uint* s_grid_indx,
	  		  	  	  	  	 const uint* p_cell_beg,
	  		  	  	  	  	 const uint* p_cell_end,
	  	  	  	  	  	  	 const vec4* s_pos,
	  	  	  	  	  	 	       uint  num_par,
	  	  	  	  	  	  	  FloatType* p_lap)
{
	uint index = iIndex();
	if(index >= num_par) return;
    vec4 tmpp = DFETCH(s_pos, index);
	vec3 posi = make_vec3(tmpp.x, tmpp.y, tmpp.z);
	FloatType radiusi = factor * tmpp.w;
	int3 grid_pos = CalcGridPos(posi);
	FloatType overlap = 0;
    uint count = 0;
    
    for (int z=-1; z<=1; z++)
    {
        for (int y=-1; y<=1; y++)
        {
            for (int x=-1; x<=1; x++)
            {
            	int3 nbr_pos = grid_pos + make_int3(x, y, z);
            	if(GridLimit(nbr_pos)) continue;
            	uint hashj = CalcGridHash(nbr_pos);
            	uint cbegj = IFETCH(p_cell_beg, hashj);
            	if(cbegj == 0xffffffff) // neighbor cell is empty
            		continue;
            	uint cendj = IFETCH(p_cell_end, hashj);
            	for(uint j = cbegj; j < cendj; ++j)
            	{
            		if(j == index) continue;
            		tmpp = DFETCH(s_pos, j);
            		vec3 posj = make_vec3(tmpp.x, tmpp.y, tmpp.z);
            		FloatType radiusj = factor * tmpp.w;
                    FloatType tmp = radiusj + radiusi - length(posi - posj);
                    if (tmp > 0)
                    {
                        overlap += tmp;
                        count++;
                    }
            	}
            }
        }
    }
    uint original_index = IFETCH(s_grid_indx, index);
    if(count > 0)
        p_lap[original_index] = overlap / count / radiusi;  
}

__constant__ FloatType gfactor = -981;
__global__ void Integrate(uint  num_par,
						  vec4* p_pos,
	  	  	  	 	 	  vec4* p_vel,
	  	  	  	 	 	  vec4* p_foc)
{
	vec4 gravity = make_vec4(0.0, 0.0, gfactor, 0.0);
	uint index = iIndex();
	if(index >= num_par) return;
	vec4 foc = p_foc[index];
    //cuPrintf("%f, %f, %f\n", foc.x, foc.y, foc.z);
	FloatType radius = p_pos[index].w;
	FloatType mass = 4 * dc_math_pi / 3 * (radius * radius * radius) * params.rho;
	p_vel[index] += (foc / mass + gravity) * params.time_step;
	p_pos[index] += p_vel[index] * params.time_step;
    foc = make_vec4(0, 0, 0, 0);
	p_foc[index] = foc;
}

__global__ void Integrate(uint  time_step,
						  uint  num_par,
						  vec4* p_pos,
	  	  	  	 	 	  vec4* p_vel,
	  	  	  	 	 	  vec4* p_foc)
{
	FloatType theta = (0.25 * dc_math_pi * time_step) / 8.e5;
	vec4 gravity = make_vec4(gfactor * sin(theta), 0.0, gfactor * cos(theta), 0.0);
	uint index = iIndex();
	if(index >= num_par) return;
	vec4 foc = p_foc[index];
	FloatType radius = p_pos[index].w;
	FloatType mass = 4 * dc_math_pi / 3 * (radius * radius * radius) * params.rho;
	p_vel[index] += (foc / mass + gravity) * params.time_step;
	p_pos[index] += p_vel[index] * params.time_step;
	foc.x = 0;
	foc.y = 0;
	foc.z = 0;
	foc.w = 0;
	// make sure the force on each particle is reset to zero
	p_foc[index] = foc;
}

__global__ void Integrate(FloatType fact,
						  uint  num_par,
						  vec4* p_pos,
	  	  	  	 	 	  vec4* p_vel,
	  	  	  	 	 	  vec4* p_foc)
{
	vec4 gravity = make_vec4(0.0, 0.0, gfactor * fact, 0.0);
	uint index = iIndex();
	if(index >= num_par) return;
	vec4 foc = p_foc[index];
	FloatType radius = p_pos[index].w;
	FloatType mass = 4 * dc_math_pi / 3 * (radius * radius * radius) * params.rho;
	p_vel[index] += (foc / mass + gravity) * params.time_step;
	p_pos[index] += p_vel[index] * params.time_step;
	foc.x = 0;
	foc.y = 0;
	foc.z = 0;
	foc.w = 0;
	// make sure the force on each particle is reset to zero
	p_foc[index] = foc;
}


__global__ void BoundaryCondition(uint  num_par,
								  vec4* p_pos,
								  vec4* p_vel)
{
	uint index = iIndex();
	if(index >= num_par) return;
	vec4 pos = p_pos[index];
    vec4 vel = p_vel[index];
	if(pos.x + pos.w > params.box_length.x + params.world_origin.x)
	{
		pos.x = params.box_length.x + params.world_origin.x - pos.w;
		p_vel[index].x = -vel.x * 1;
	}
	if(pos.x - pos.w < params.world_origin.x)
	{
		pos.x = params.world_origin.x + pos.w;
		p_vel[index].x = -vel.x * 1;
	}
	if(pos.y + pos.w > params.box_length.y + params.world_origin.y)
	{
		pos.y = params.box_length.y + params.world_origin.y - pos.w;
		p_vel[index].y = -vel.y * 1;
	}
	if(pos.y - pos.w < params.world_origin.y)
	{
		pos.y = params.world_origin.y + pos.w;
		p_vel[index].y = -vel.y * 1;
	}
	if(pos.z + pos.w > params.box_length.z + params.world_origin.z)
	{
		pos.z = params.box_length.z + params.world_origin.z - pos.w;
		p_vel[index].z = -vel.z * 1;
	}
	if(pos.z - pos.w < params.world_origin.z)
	{
		pos.z = params.world_origin.z + pos.w;
		p_vel[index].z = -vel.z * 1;
	}
	p_pos[index] = pos;
}

__global__ void BoundaryCondition(uint  time_step,
								  uint  num_par,
								  vec4* p_pos,
								  vec4* p_vel)
{
	uint index = iIndex();
	if(index >= num_par) return;
	vec4 pos = p_pos[index];
	vec4 vel = p_vel[index];
	
	FloatType xlb, xub;
	FloatType wall_pos;
	
	wall_pos = pos.z * tan(dc_math_pi / 4.0 * FloatType(time_step) / 4.0e5);
	xlb = params.world_origin.x + wall_pos;
	xub = xlb + params.box_length.x;
	if(pos.x + pos.w > xub)
	{
		pos.x = xub - pos.w;
		vel.x = 0;
	}
	if(pos.x - pos.w < xlb)
	{
		pos.x = xlb + pos.w;
		vel.x = 0;
	}
	if(pos.y + pos.w > params.box_length.y + params.world_origin.y)
	{
		pos.y = params.box_length.y + params.world_origin.y - pos.w;
		vel.y = 0;
	}
	if(pos.y - pos.w < params.world_origin.y)
	{
		pos.y = pos.w;
		vel.y = 0;
	}
	if(pos.z + pos.w > params.box_length.z + params.world_origin.z)
	{
		pos.z = params.box_length.z + params.world_origin.z - pos.w;
		vel.z = 0;
	}
	if(pos.z - pos.w < params.world_origin.z)
	{
		pos.z = pos.w;
		vel.z = 0;
	}
	p_pos[index] = pos;
	p_vel[index] = vel;
}

//__global__ void ReorderAfterIntegration(const uint* s_grid_indx,
//										const vec4* s_pos,
//										const vec4* s_vel,
//											  vec4* s_foc,
//											  uint  num_par,
//											  vec4* p_pos,
//											  vec4* p_vel,
//											  vec4* p_foc)
//{
//	uint index = iIndex();
//	if(index >= num_par) return;
//	uint original_index = s_grid_indx[index];
//	vec4 pos = DFETCH(s_pos, index);
//	vec4 vel = DFETCH(s_vel, index);
//	vec4 foc = s_foc[index];
//	p_pos[original_index] = pos;
//	p_vel[original_index] = vel;
//	p_foc[original_index] = foc;
//	foc.x = 0;
//	foc.y = 0;
//	foc.z = 0;
//	foc.w = 0;
//	s_foc[index] = foc;
//}


void MD_Particle_Malloc(MD_Particle_Memory& pmem, uint npar, uint ncell)
{
	// pmem.numpar = npar;
	// using 4-vec for each of the quantity
	// pmem.veclength = npar * sizeof(FloatType) * 4; //size in bytes

	// particle array
	pmem.dfoc.resize(npar * 4);
	pmem.dmom.resize(npar * 4);
	pmem.dpos.resize(npar * 4);
	pmem.dvel.resize(npar * 4);
	pmem.p_dfoc = thrust::raw_pointer_cast(&(pmem.dfoc[0]));
	pmem.p_dmom = thrust::raw_pointer_cast(&(pmem.dmom[0]));
	pmem.p_dpos = thrust::raw_pointer_cast(&(pmem.dpos[0]));
	pmem.p_dvel = thrust::raw_pointer_cast(&(pmem.dvel[0]));
	pmem.sorted_dpos.resize(npar * 4);
	pmem.sorted_dvel.resize(npar * 4);
	pmem.sorted_dfoc.resize(npar * 4);
	pmem.sorted_dmom.resize(npar * 4);
	pmem.s_dpos = thrust::raw_pointer_cast(&(pmem.sorted_dpos[0]));
	pmem.s_dvel = thrust::raw_pointer_cast(&(pmem.sorted_dvel[0]));
	pmem.s_dfoc = thrust::raw_pointer_cast(&(pmem.sorted_dfoc[0]));
	pmem.s_dmom = thrust::raw_pointer_cast(&(pmem.sorted_dmom[0]));

	// grid(cell) array
	pmem.grid_hash.resize(npar);
	pmem.grid_indx.resize(npar);
	pmem.cell_beg.resize(ncell);
	pmem.cell_end.resize(ncell);
	pmem.p_grid_hash = thrust::raw_pointer_cast(&(pmem.grid_hash[0]));
	pmem.p_grid_indx = thrust::raw_pointer_cast(&(pmem.grid_indx[0]));
	pmem.p_cell_beg =  thrust::raw_pointer_cast(&(pmem.cell_beg[0]));
	pmem.p_cell_end =  thrust::raw_pointer_cast(&(pmem.cell_end[0]));
}

// Lexicographical_order algorithm of thrust
//template <typename KeyVector, typename PermutationVector>
//void update_permutation(KeyVector& keys, PermutationVector& permutation)
//{
//  // temporary storage for keys
//  KeyVector temp(keys.size());
//
//  // permute the keys with the current reordering
//  thrust::gather(permutation.begin(), permutation.end(), keys.begin(), temp.begin());
//
//  // stable_sort the permuted keys and update the permutation
//  thrust::stable_sort_by_key(temp.begin(), temp.end(), permutation.begin());
//}
//
//
//template <typename KeyVector, typename PermutationVector>
//void apply_permutation(KeyVector& keys, PermutationVector& permutation)
//{
//  // copy keys to temporary vector
//  KeyVector temp(keys.begin(), keys.end());
//
//  // permute the keys
//  thrust::gather(permutation.begin(), permutation.end(), temp.begin(), keys.begin());
//}


//void update_permutation_uint(thrust::device_ptr<uint> keys, thrust::device_ptr<uint> permutation, int num)
//{
//  thrust::device_vector<uint> temp(num);
//  thrust::gather(permutation, permutation + num, keys, temp.begin());
//  thrust::stable_sort_by_key(temp.begin(), temp.end(), permutation);
//}
//
//void apply_permutation_uint(thrust::device_ptr<uint> keys, thrust::device_ptr<uint> permutation, int num)
//{
//  thrust::device_vector<uint> temp(keys, keys + num);
//  thrust::gather(permutation, permutation + num, temp.begin(), keys);
//}


void GPUHashCheck( uint* p_grid_hash,
                    vec4* p_pos,
                    uint  num_par,
                    uint  num_cell)
{
    /*
    std::cout << "hashcheck" << std::endl;
    // thrust::device_ptr<uint> hash_ptr( p_grid_hash );
    thrust::device_ptr<vec4> pos4_ptr( p_pos );
    // thrust::host_vector<uint> h_hash(hash_ptr, hash_ptr + num_par);
    thrust::host_vector<vec4> h_pos4(pos4_ptr, pos4_ptr + num_par);
    for(uint i = 0; i < num_par; ++i)
    {
        if(i == 204713 || i == 362062)
        {
            std::cout  << i << " " << h_pos4[i].x << " " <<  h_pos4[i].y << " " << h_pos4[i].z  << " " << h_pos4[i].w << std::endl;
        }
    }*/
}

void GPUSortArray(  uint*  p_grid_hash,
                    uint* p_grid_indx,
                    uint  num_par )
{

    thrust::device_ptr<uint> hash_ptr( p_grid_hash );
    // thrust::host_vector<uint> host_hash(hash_ptr, hash_ptr + num_par);
    // KernelError( "kernel:GPUSortArray:HashAssignment" );
    thrust::device_ptr<uint> indx_ptr( p_grid_indx );
    // KernelError( "kernel:GPUSortArray:IndxAssignment" );
    // std::cout << hash_ptr[num_par - 1] << "\n";
    // thrust::host_vector<uint> tmp_hash( hash_ptr, hash_ptr + num_par );
    // KernelError( "Kernel:GPUSortArray:AfterHostVector" );
    // std::cout << tmp_hash[num_par - 1] << "\n";
    // KernelError( "Kernel:GPUSortArray:BeforeStableSort" );
    thrust::stable_sort_by_key( hash_ptr, hash_ptr + num_par, indx_ptr );
    KernelError( "Kernel:GPUSortArray:AfterStableSort" );
    // KernelError( "kernel:GPUSortArray:After" );
}

void GPUReorderArray( uint* s_grid_hash,
                       uint* s_grid_indx,
                       uint  num_par,
                       uint  num_cell,
                       uint* p_cell_beg,
                       uint* p_cell_end,
                       vec4* p_pos,
                       vec4* p_vel,
                       vec4* s_pos,
                       vec4* s_vel )
//void GPUReorderArray(uint* s_grid_hash,
//                    uint* s_grid_indx,
//                    uint  num_par,
//                    uint  num_cell,
//                    uint* p_cell_beg,
//                    uint* p_cell_end,
//                    uint* s_cell_beg,
//                    uint* s_cell_end,
//                    vec4* p_pos,
//                    vec4* p_vel,
//                    vec4* s_pos,
//                    vec4* s_vel)
{
#if USE_TEX
    cudaBindTexture( 0, p_pos_TEX, p_pos, num_par * sizeof( vec4 ) );
    cudaBindTexture( 0, p_vel_TEX, p_vel, num_par * sizeof( vec4 ) );
    cudaBindTexture( 0, s_grid_hash_TEX, s_grid_hash, num_par * sizeof( uint ) );
    cudaBindTexture( 0, s_grid_indx_TEX, s_grid_indx, num_par * sizeof( uint ) );
#endif
    uint nblocks, nthreads;
    ComputeGridSize( num_par, WORK_SIZE, nblocks, nthreads );
    uint smem_size = sizeof( uint ) * ( nthreads + 1 );
    cudaMemset( p_cell_beg, 0xffffffff, num_cell * sizeof( uint ) );
//  cudaPrintfInit();
    ReorderSortedArray <<< nblocks, nthreads, smem_size>>>( s_grid_hash, s_grid_indx, p_pos, p_vel, num_par, p_cell_beg, p_cell_end, s_pos, s_vel );
//  cudaPrintfDisplay(stdout, 1);
//  cudaPrintfEnd();
    KernelError( "kernel: ReorderSortedArray;" );
//  try
//  {
//      cudaMemcpy(s_cell_beg, p_cell_beg, num_cell*sizeof(uint), cudaMemcpyDeviceToDevice);
//      cudaMemcpy(s_cell_end, p_cell_end, num_cell*sizeof(uint), cudaMemcpyDeviceToDevice);
//      thrust::device_ptr<uint> cell_beg_ptr(s_cell_beg);
//      thrust::device_ptr<uint> cell_end_ptr(s_cell_end);
//      thrust::stable_sort_by_key(cell_beg_ptr, cell_beg_ptr + num_cell, cell_end_ptr);
//  }
//  catch(thrust::system_error &e)
//  {
//      std::cerr << "Error accessing vector element: " << e.what() << std::endl;
//      exit(-1);
//  }
#if USE_TEX
    cudaUnbindTexture( p_pos_TEX );
    cudaUnbindTexture( p_vel_TEX );
    cudaUnbindTexture( s_grid_hash_TEX );
    cudaUnbindTexture( s_grid_indx_TEX );
#endif
//  thrust::device_ptr<uint> hash_ptr(s_grid_hash);
//  thrust::device_ptr<uint> cell_beg(s_cell_beg);
//  thrust::device_ptr<uint> cell_end(s_cell_end);
//  thrust::host_vector<uint> hvector(hash_ptr, hash_ptr + num_par);
//  thrust::host_vector<uint> bvector(cell_beg, cell_beg + num_cell);
//  thrust::host_vector<uint> evector(cell_end, cell_end + num_cell);
//  for(int i = 0; i < num_par; ++i)
//  {
//      std::cout << hvector[i] << ", " << int(bvector[i]) << ", " << int(evector[i]) << std::endl;
//  }
//      thrust::device_ptr<vec4> p_pos_ptr(p_pos);
//      thrust::device_ptr<vec4> s_pos_ptr(s_pos);
//      thrust::host_vector<vec4> hvector(p_pos_ptr, p_pos_ptr + num_par);
//      thrust::host_vector<vec4> bvector(s_pos_ptr, s_pos_ptr + num_par);
//      for(int i = 0; i < num_par; ++i)
//      {
//          std::cout << hvector[i].x << ", " << bvector[i].x << std::endl;
//      }
}

uint u_cell_beg[8];
uint u_cell_end[8];

//void GPUUniqueCellTypeArray(uint* s_cell_beg,
//                           uint* s_cell_end,
//                           vec4* s_pos,
//                           uint  num_par,
//                           uint* u_cell_type) // host_vector of size 8;
//{
//#if USE_TEX
//  cudaBindTexture(0, s_pos_TEX, s_pos, num_par*sizeof(vec4));
//  // as we have sorted the cell beginning array, its effective size should be at most num_par
//  cudaBindTexture(0, s_cell_beg_TEX, s_cell_beg, num_par*sizeof(uint));
//#endif
//  uint nblocks, nthreads;
//  ComputeGridSize(num_par, WORK_SIZE, nblocks, nthreads);
//  // padding the array with 0xffffffff is important!
//  cudaMemset(u_cell_type, 0xffffffff, num_par*sizeof(uint));
//  CreateUniqueCellTypeArray<<<nblocks, nthreads>>>(s_cell_beg, s_pos, num_par, u_cell_type);
//
//#if USE_TEX
//  cudaUnbindTexture(s_pos_TEX);
//  cudaUnbindTexture(s_cell_beg_TEX);
//#endif
//  // now we need to sort the cell_beg array so we need to unbind it from texture
//  thrust::device_ptr<uint> ctype_ptr(u_cell_type);
//  thrust::device_ptr<uint> c_beg_ptr(s_cell_beg);
//  thrust::device_ptr<uint> c_end_ptr(s_cell_end);
//  thrust::stable_sort_by_key(ctype_ptr, ctype_ptr + num_par, thrust::make_zip_iterator(make_tuple(c_beg_ptr, c_end_ptr)));
//  for(uint i = 0; i < MD_DIM_PASS; ++i)
//  {
//      thrust::device_ptr<uint> tmp = thrust::find(ctype_ptr, ctype_ptr + num_par, i);
////        std::cout << int(tmp - ctype_ptr) << ", ";
//      int index = int(tmp - ctype_ptr);
//      if(index == num_par)
//      {
//          u_cell_beg[i] = 0xffffffff;
//      }
//      else
//      {
//          u_cell_beg[i] = index;
//      }
//  }
//  for(uint i = 0; i < MD_DIM_PASS - 1; ++i)
//  {
//      u_cell_end[i] = u_cell_beg[i + 1];
//  }
//  u_cell_end[MD_DIM_PASS - 1] = num_par;
////    std::cout << std::endl;
//
////    thrust::host_vector<uint> vector1(ctype_ptr, ctype_ptr + num_par);
////    thrust::host_vector<uint> vector2(c_beg_ptr, c_beg_ptr + num_par);
////    thrust::host_vector<uint> vector3(c_end_ptr, c_end_ptr + num_par);
////    for (int i = 0; i < num_par; ++i)
////    {
////        std::cout << int(vector1[i]) <<", " << int(vector2[i]) <<", " << int(vector3[i]) <<", " << std::endl;
////    }
//
//}

// #include <bitset>
void GPUCalcHash( vec4* pos,            //input
                   uint  num_par,        //input
                   uint* p_grid_hash,    //output array size is num_par * 2^d
                   uint* p_grid_indx )   //output array size is num_par * 2^d
{
//  cudaPrintfInit();
    uint nblocks, nthreads;
    ComputeGridSize( num_par, WORK_SIZE, nblocks, nthreads );
    CalcHashTable <<< nblocks, nthreads>>>( pos, num_par, p_grid_hash, p_grid_indx );
//  cudaPrintfDisplay(stdout, 1);
//  cudaPrintfEnd();
    KernelError( "kernel: CalcHashTable;" );
    //      thrust::device_ptr<uint> ctrl_ptr(p_part_ctrl);
    //      thrust::host_vector<uint> part_ctrl(ctrl_ptr, ctrl_ptr + num_par);
    //      for(int i = 0; i < num_par; ++i)
    //      {
    //          std::bitset<32> x(part_ctrl[i]);
    //          std::cout << x << std::endl;
    //      }
}

void SetParameters( SimParams *hparams )
{
    // copy parameters to constant memory
    GPUCheckErrors( cudaMemcpyToSymbol( params, hparams, sizeof( SimParams ) ) );
}

void GPUCollideBoulders( uint* index, uint isize, vec4* p_pos, vec4* p_vel, uint num_par, vec4* p_foc )
{
#if USE_TEX
    cudaBindTexture( 0, p_pos_TEX, p_pos, num_par * sizeof( vec4 ) );
    cudaBindTexture( 0, p_vel_TEX, p_vel, num_par * sizeof( vec4 ) );
#endif
    uint nblocks, nthreads;
    ComputeGridSize( num_par, WORK_SIZE, nblocks, nthreads );
    for( uint i = 0; i < isize; ++i )
    {
        thrust::device_vector<vec4> bfvec( num_par, make_vec4(0, 0, 0, 0) );
        vec4* b_foc = thrust::raw_pointer_cast( bfvec.data() );
        uint bindex = index[i];
        CollideBoulders <<< nblocks, nthreads>>>( bindex, p_pos, p_vel, num_par, p_foc, b_foc );
        thrust::device_ptr<vec4> tp_foc = thrust::device_pointer_cast( p_foc );
        tp_foc[bindex] += thrust::reduce( bfvec.begin(), bfvec.end(), make_vec4( 0, 0, 0, 0 ) );
    }
#if USE_TEX
    cudaUnbindTexture( p_pos_TEX );
    cudaUnbindTexture( p_vel_TEX );
#endif
    KernelError( "kernel: CollideBoulders;" );
}

void GPUCollideCells( uint* s_grid_indx,
                       uint* p_cell_beg,
                       uint* p_cell_end,
                       uint  num_par,
                       uint  num_cell,
                       vec4* s_pos,
                       vec4* s_vel,
                       vec4* p_foc )
//
//void GPUCollideCells(uint* s_grid_hash,
//                    uint* s_grid_indx,
//                    uint* p_cell_beg,
//                    uint* p_cell_end,
//                    uint* s_cell_beg,
//                    uint* s_cell_end,
//                    uint  num_par,
//                    uint  num_cell,
//                    vec4* s_pos,
//                    vec4* s_vel,
//                    vec4* s_foc)
{
#if USE_TEX
    cudaBindTexture( 0, s_pos_TEX, s_pos, num_par * sizeof( vec4 ) );
    cudaBindTexture( 0, s_vel_TEX, s_vel, num_par * sizeof( vec4 ) );
    cudaBindTexture( 0, p_cell_beg_TEX, p_cell_beg, num_cell * sizeof( uint ) );
    cudaBindTexture( 0, p_cell_end_TEX, p_cell_end, num_cell * sizeof( uint ) );
    cudaBindTexture( 0, s_grid_indx_TEX, s_grid_indx, num_par * sizeof( uint ) );
//        cudaBindTexture(0, s_cell_beg_TEX, s_cell_beg, num_cell*sizeof(uint));
//        cudaBindTexture(0, s_cell_end_TEX, s_cell_end, num_cell*sizeof(uint));
#endif
//  cudaPrintfInit();
    uint nblocks, nthreads;
    ComputeGridSize( num_par, WORK_SIZE, nblocks, nthreads );
    CollideCells <<< nblocks, nthreads>>>( s_grid_indx, p_cell_beg, p_cell_end, s_pos, s_vel, num_par, p_foc );
//  for(uint i = 0; i < MD_DIM_PASS; ++i)
//  {
//      if(u_cell_beg[i] == 0xffffffff)
//          continue;
////        std::cout << u_cell_end[i] - u_cell_beg[i] << std::endl;
//      ComputeGridSize(u_cell_end[i] - u_cell_beg[i], WORK_SIZE, nblocks, nthreads);
//      CollideCells<<<nblocks, nthreads>>>(s_grid_hash, s_grid_indx, p_cell_beg, p_cell_end, s_cell_beg, s_cell_end, s_pos, s_vel, num_par, u_cell_beg[i], u_cell_end[i], s_foc);
//  }
//  cudaPrintfDisplay(stdout, 1);
//  cudaPrintfEnd();
    KernelError( "kernel: CollideCells;" );
#if USE_TEX
    cudaUnbindTexture( s_pos_TEX );
    cudaUnbindTexture( s_vel_TEX );
    cudaUnbindTexture( p_cell_beg_TEX );
    cudaUnbindTexture( p_cell_end_TEX );
    cudaUnbindTexture( s_grid_indx_TEX );
//  cudaUnbindTexture(s_cell_beg_TEX);
//  cudaUnbindTexture(s_cell_end_TEX);
#endif
//  thrust::device_ptr<vec4> tmp_ptr(foc);
//  thrust::host_vector<vec4> tmp_vec(tmp_ptr, tmp_ptr + num_par);
//  for(int i = 0; i < num_par; ++i)
//  {
//      std::cout << tmp_vec[i].x << ", ";
//  }
//  std::cout << std::endl;
}

void GPUIntegrate( uint  num_par,
                    vec4* p_pos,
                    vec4* p_vel,
                    vec4* p_foc )
{
    /*
    std::cout << "Integrate" << std::endl;
    thrust::device_ptr<vec4> foc_ptr(p_foc);
    thrust::host_vector<vec4> h_foc(foc_ptr, foc_ptr + num_par);
    for(uint i = 0; i < num_par; ++i)
    {
        if(h_foc[i].w != 0)
            std::cout << i << " " << h_foc[i].w << std::endl;
    }
    */
    KernelError( "kernel:DEMIntegration;" );
    uint nblocks, nthreads;
    ComputeGridSize( num_par, WORK_SIZE, nblocks, nthreads );
    // cudaPrintfInit();
    Integrate <<< nblocks, nthreads>>>( num_par, p_pos, p_vel, p_foc );
    // cudaPrintfDisplay();
    // cudaPrintfEnd();
}

void GPUIntegrate( uint  time_step,
                    uint  num_par,
                    vec4* p_pos,
                    vec4* p_vel,
                    vec4* p_foc )
{
    uint nblocks, nthreads;
    ComputeGridSize( num_par, WORK_SIZE, nblocks, nthreads );
    Integrate <<< nblocks, nthreads>>>( time_step, num_par, p_pos, p_vel, p_foc );
    KernelError( "kernel:DEMIntegration;" );
}


void GPUBoundary( uint  num_par,
                   vec4* p_pos,
                   vec4* p_vel )

{
    uint nblocks, nthreads;
    ComputeGridSize( num_par, WORK_SIZE, nblocks, nthreads );
    BoundaryCondition <<< nblocks, nthreads>>>( num_par, p_pos, p_vel );
    KernelError( "kernel:BoundaryCond;" );
}

void GPUBoundary( uint  time_step,
                   uint  num_par,
                   vec4* p_pos,
                   vec4* p_vel )
{
    uint nblocks, nthreads;
    ComputeGridSize( num_par, WORK_SIZE, nblocks, nthreads );
    BoundaryCondition <<< nblocks, nthreads>>>( time_step, num_par, p_pos, p_vel );
    KernelError( "kernel:BoundaryCond;" );
}

void GPUResizeSandArray(FloatType* fnorm, uint& num_par, MD_Particle_Memory& dh_mem)
{
    thrust::device_ptr<FloatType> pfnorm(fnorm);
    uint tmpnpar = thrust::count(pfnorm, pfnorm + num_par, 0);
    thrust::device_ptr<vec4> p_pos4((vec4*)dh_mem.p_dpos);
    // thrust::device_vector<vec4> d_pos4(p_pos4, p_pos4 + num_par);
    thrust::sort_by_key(pfnorm, pfnorm + num_par, p_pos4);
    thrust::host_vector<FloatType> h_pos4(pfnorm, pfnorm + num_par);
    dh_mem.Resize(tmpnpar);
    // thrust::copy(d_pos4.begin(), d_pos4.end(), p_pos4);
    std::cout << tmpnpar << "  " << num_par << std::endl;
    num_par = tmpnpar;
    /*
    for(uint i = 0; i < num_par; ++i)
    {
        if(h_pos4[i] > 0)
            std::cout << i << " " << h_pos4[i] << std::endl;
    }
    exit(0);
    */
    KernelError( "kernel:ResizeArray;" );
}

//void GPUReorderAfterIntegration(uint* s_grid_indx, uint num_par,
//                               vec4* s_pos, vec4* s_vel, vec4* s_foc,
//                               vec4* p_pos, vec4* p_vel, vec4* p_foc)
//{
//#if USE_TEX
//    cudaBindTexture(0, s_pos_TEX, s_pos, num_par*sizeof(vec4));
//    cudaBindTexture(0, s_vel_TEX, s_vel, num_par*sizeof(vec4));
//#endif
//  uint nblocks, nthreads;
//  ComputeGridSize(num_par, WORK_SIZE, nblocks, nthreads);
//  ReorderAfterIntegration<<<nblocks, nthreads>>>(s_grid_indx,s_pos, s_vel, s_foc, num_par, p_pos, p_vel, p_foc);
//#if USE_TEX
//  cudaUnbindTexture(s_pos_TEX);
//  cudaUnbindTexture(s_vel_TEX);
//#endif
//}
