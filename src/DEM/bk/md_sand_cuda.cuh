/*
 * md_sand_cuda.cuh
 *
 *  Created on: Nov 26, 2012
 *      Author: tingnan
 */
#ifndef _MD_SAND_CUDA_CUH_
#define _MD_SAND_CUDA_CUH_
#include "MathHelper/md_vec_cuda.cuh"
#include <thrust/device_vector.h>
#include <vector>


struct MD_Particle_Memory
{
	typedef std::vector<FloatType> farray;
	typedef std::vector<uint> iarray;
	typedef std::vector<vec3> varray;
	friend void MD_Particle_Malloc(MD_Particle_Memory&, uint, uint);
protected:
	farray hf;
	farray hm;
	farray hp;
	farray hv;
    farray hw;
	thrust::device_vector<FloatType> dfoc;
	thrust::device_vector<FloatType> dmom;
	thrust::device_vector<FloatType> dpos;
	thrust::device_vector<FloatType> dvel;
    thrust::device_vector<FloatType> dome;
	thrust::device_vector<FloatType> sorted_dpos;
	thrust::device_vector<FloatType> sorted_dvel;
    thrust::device_vector<FloatType> sorted_dome;
	thrust::device_vector<FloatType> sorted_dfoc;
	thrust::device_vector<FloatType> sorted_dmom;
	thrust::device_vector<uint> grid_hash;
	thrust::device_vector<uint> grid_indx;
	thrust::device_vector<uint> cell_beg;
	thrust::device_vector<uint> cell_end;
	void PushInternalMemory()
	{
		for(int i = 0; i < numpar; ++i)
		{
			hf[i * 4 + 0] = hfoc[i].x;
			hf[i * 4 + 1] = hfoc[i].y;
			hf[i * 4 + 2] = hfoc[i].z;
			hm[i * 4 + 0] = hmom[i].x;
			hm[i * 4 + 1] = hmom[i].y;
			hm[i * 4 + 2] = hmom[i].z;
			hv[i * 4 + 0] = hvel[i].x;
			hv[i * 4 + 1] = hvel[i].y;
			hv[i * 4 + 2] = hvel[i].z;
			hw[i * 4 + 0] = home[i].x;
			hw[i * 4 + 1] = home[i].y;
			hw[i * 4 + 2] = home[i].z;
			hp[i * 4 + 0] = hpos[i].x;
			hp[i * 4 + 1] = hpos[i].y;
			hp[i * 4 + 2] = hpos[i].z;
			hp[i * 4 + 3] = hrad[i];
		}
	}
	void PullInternalMemory()
	{
		for(int i = 0; i < numpar; ++i)
		{
			hfoc[i].x = hf[i * 4 + 0];
			hfoc[i].y = hf[i * 4 + 1];
			hfoc[i].z = hf[i * 4 + 2];
			hmom[i].x = hm[i * 4 + 0];
			hmom[i].y = hm[i * 4 + 1];
			hmom[i].z = hm[i * 4 + 2];
			hvel[i].x = hv[i * 4 + 0];
			hvel[i].y = hv[i * 4 + 1];
			hvel[i].z = hv[i * 4 + 2];
			home[i].x = hw[i * 4 + 0];
			home[i].y = hw[i * 4 + 1];
			home[i].z = hw[i * 4 + 2];
			hpos[i].x = hp[i * 4 + 0];
			hpos[i].y = hp[i * 4 + 1];
			hpos[i].z = hp[i * 4 + 2];
			hrad[i]   = hp[i * 4 + 3];
		}
	}
	// length;
	uint numpar;
	uint numcell;
	uint veclength;
	
public:
	// Device memory;
	FloatType* p_dfoc;
	FloatType* p_dmom;
	FloatType* p_dpos;
	FloatType* p_dvel;
    FloatType* p_dome;
	FloatType* s_dpos;
	FloatType* s_dvel;
    FloatType* s_dome;
	FloatType* s_dfoc;
	FloatType* s_dmom;
	uint* p_grid_hash;
	uint* p_grid_indx;
	uint* p_cell_beg;
	uint* p_cell_end;

	// host memory;
	farray hrad;
	farray mass;
	varray hfoc;
	varray hmom;
	varray hpos;
	varray hvel;
    varray home;

	void MemCpyHostToDevice()
	{
		PushInternalMemory();
		cudaMemcpy(p_dfoc, &hf[0], veclength * sizeof(FloatType), cudaMemcpyHostToDevice);
		cudaMemcpy(p_dmom, &hm[0], veclength * sizeof(FloatType), cudaMemcpyHostToDevice);
		cudaMemcpy(p_dpos, &hp[0], veclength * sizeof(FloatType), cudaMemcpyHostToDevice);
		cudaMemcpy(p_dvel, &hv[0], veclength * sizeof(FloatType), cudaMemcpyHostToDevice);
		cudaMemcpy(p_dome, &hw[0], veclength * sizeof(FloatType), cudaMemcpyHostToDevice);
	}
	void MemCpyDeviceToHost()
	{
		cudaMemcpy(&hf[0], p_dfoc, veclength * sizeof(FloatType), cudaMemcpyDeviceToHost);
		cudaMemcpy(&hm[0], p_dmom, veclength * sizeof(FloatType), cudaMemcpyDeviceToHost);
		cudaMemcpy(&hp[0], p_dpos, veclength * sizeof(FloatType), cudaMemcpyDeviceToHost);
		cudaMemcpy(&hv[0], p_dvel, veclength * sizeof(FloatType), cudaMemcpyDeviceToHost);
		cudaMemcpy(&hw[0], p_dome, veclength * sizeof(FloatType), cudaMemcpyDeviceToHost);
		PullInternalMemory();
	}
	void Malloc(uint npar, uint ncell)
	{
		numpar = npar;
		veclength = npar * 4;
		numcell = ncell;
		hf.resize(veclength);
		hm.resize(veclength);
		hp.resize(veclength);
		hv.resize(veclength);
        hw.resize(veclength);
		hrad.resize(npar);
		hpos.resize(npar);
		hvel.resize(npar);
        home.resize(npar);
		hfoc.resize(npar);
		hmom.resize(npar);
		mass.resize(npar);
		MD_Particle_Malloc(*this, npar, ncell);
	}
	void Resize(int npar)
	{
		Malloc(npar, numcell);
	}
};

// the host memory structure has protected memory arrays that has the same size as device memory

struct SimParams
{
    vec3 box_length;
    vec3 world_origin;
    vec3 cell_size;
    uint3 grid_size;
    FloatType radius_min;
    FloatType radius_max;
    FloatType mass_min;
    FloatType mass_max;
    FloatType rho;
    FloatType kn;
    FloatType kt;
    FloatType gn;
    FloatType gt;
    FloatType mu;
    FloatType time_step;
};

void GPUSortArray( uint* gridHash,
        uint* gridIndex,
        uint  numberParticles );

void GPUHashCheck(  uint*  p_grid_hash,
                    vec4* p_pos,
                    uint  num_par,
                    uint  num_cell);

void GPUReorderArray( uint* sortedGridHash,
        uint* sortedGridIndex,
        uint  numberParticles,
        uint  numberCells,
        uint* cellBeginning,
        uint* cellEnding,
        vec4* position,
        vec4* velocity,
        vec4* avelocity,
        vec4* sortedPosition,
        vec4* sortedVelocity
        vec4* sortedAvelocity);

void GPUCalcHash( vec4* position,
        uint  numberParticles,
        uint* gridHash,
        uint* gridIndex );

void SetParameters( SimParams* hparams );

void GPUCollideCells( uint* sortedGridHash,
        uint* cellBeginning,
        uint* cellEnding,
        uint  numberParticles,
        uint  numberCells,
        vec4* sortedPosition,
        vec4* sortedVelocity,
        vec4* sortedAvelocity,
        vec4* force 
        vec4* moment);

void GPUCollideBoulders( uint* boulderIndex,
        uint  boulderArraySize,
        vec4* position,
        vec4* velocity,
        uint  numberParticles,
        vec4* force );


void GPUIntegrate( uint  numberParticles,
        vec4* position,
        vec4* velocity,
        vec4* force );

void GPUIntegrate( uint  currentTimeStep,
        uint  numberParticles,
        vec4* position,
        vec4* velocity,
        vec4* force );

void GPUBoundary( uint  numberParticles,
        vec4* position,
        vec4* velocity );

void GPUBoundary( uint  currentTimeStep,
        uint  numberParticles,
        vec4* position,
        vec4* velocity );

void GPUResizeSandArray(FloatType* fnorm, uint& num_par, MD_Particle_Memory& dh_mem);
#endif /* MD_SAND_CUDA_CUH_ */
