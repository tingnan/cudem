// The units in this program are second, centimeter, gram.


#include <cstdlib>
#include <iomanip>
#include <fstream>
#include <cassert>
#include "md_sand.h"
#include "CollisionShapes/md_shape.h"
#include "MathHelper/md_utility.h"
#include "MathHelper/md_vec.h"
#include "RFT/md_rft.h"
#include <algorithm>

#ifdef __ENABLE_CUDA__

#include "md_sand_cuda.cuh"
// this is going to impact performance depending on problem types
#define MD_CUDA_CELL_SCALE 1 

// a stable set of params for 3mm glass particles
#define MD_TIME_STEP 1.e-5
#define MD_SAVE_STEP 100
#define MD_DT 1.05
#define MD_KN 2e7
#define MD_GN 500
#define MD_MU 0.3



MDSimpleSand::MDSimpleSand()
{
    sys_step_ = 0;
    ios_step_ = 0;
    params_.time_step = MD_TIME_STEP;	
}

MDSimpleSand::~MDSimpleSand()
{
    Finalize();
}

void MDSimpleSand::Finalize()
{
    num_part_ = 0;
}

void MDSimpleSand::SortGridHash()
{
    
    GPUSortArray(dh_mem_.p_grid_hash, dh_mem_.p_grid_indx, num_part_);
    GPUReorderArray(dh_mem_.p_grid_hash, dh_mem_.p_grid_indx, num_part_, num_cell_, dh_mem_.p_cell_beg,  dh_mem_.p_cell_end,
            (vec4*)dh_mem_.p_dpos, (vec4*)dh_mem_.p_dvel, (vec4*)dh_mem_.p_dome, (vec4*)dh_mem_.s_dpos, (vec4*)dh_mem_.s_dvel, (vec4*)dh_mem_.s_dome);
}

void MDSimpleSand::InitGridHash()
{
    GPUCalcHash((vec4*)dh_mem_.p_dpos, num_part_, dh_mem_.p_grid_hash, dh_mem_.p_grid_indx);
}

void MDSimpleSand::CollideCells()
{
    GPUCollideCells(dh_mem_.p_grid_indx, dh_mem_.p_cell_beg, dh_mem_.p_cell_end, num_part_, num_cell_, (vec4*)dh_mem_.s_dpos, (vec4*)dh_mem_.s_dvel, (vec4*)dh_mem_.s_dome, (vec4*)dh_mem_.p_dfoc, (vec4*)dh_mem_.p_dmom);
    GPUCollideBoulders(&(bdindex_[0]), bdindex_.size(), (vec4*)dh_mem_.p_dpos, (vec4*)dh_mem_.p_dvel, num_part_, (vec4*)dh_mem_.p_dfoc);				 
}

void MDSimpleSand::IntegratePar()
{
    GPUIntegrate(num_part_, (vec4*)dh_mem_.p_dpos, (vec4*)dh_mem_.p_dvel, (vec4*)dh_mem_.p_dome, (vec4*)dh_mem_.p_dfoc, (vec4*)dh_mem_.p_dmom);
}

void MDSimpleSand::BoundaryCond()
{
    GPUBoundary(num_part_, (vec4*)dh_mem_.p_dpos, (vec4*)dh_mem_.p_dvel);
    GPUHashCheck(dh_mem_.p_grid_hash, (vec4*)dh_mem_.p_dpos, num_part_, num_cell_);
}

MDSimpleSand::MDSimpleSand(uint npar, FloatType lx, FloatType ly, FloatType lz)
{
    sys_step_ = 0;
    ios_step_ = 0;
    params_.time_step = MD_TIME_STEP;
    SysInit(npar, lx, ly, lz);
}

void MDSimpleSand::SysInit(uint npar, FloatType lx, FloatType ly, FloatType lz)
{
    num_part_ = npar;
    params_.box_length.x = lx;
    params_.box_length.y = ly;
    params_.box_length.z = lz;

    params_.radius_min = 0.255;
    params_.radius_max = 0.275;
    params_.rho = MD_DT;
    params_.mass_min = params_.rho * pow(params_.radius_min, 3);
    params_.mass_max = params_.rho * pow(params_.radius_min, 3);

    params_.kn = MD_KN;
    params_.kt = 0;
    params_.gn = MD_GN;
    params_.mu = MD_MU;
    // region geometry
    params_.cell_size.x = MD_CUDA_CELL_SCALE * params_.radius_max * 2;
    params_.cell_size.y = MD_CUDA_CELL_SCALE * params_.radius_max * 2;
    params_.cell_size.z = MD_CUDA_CELL_SCALE * params_.radius_max * 2;
    params_.grid_size.x = params_.box_length.x / params_.cell_size.x + 2;
    params_.grid_size.y = params_.box_length.y / params_.cell_size.y + 2;
    params_.grid_size.z = params_.box_length.z / params_.cell_size.z + 2;
    // initial system time and time step, time step length
    params_.world_origin.x = 0;
    params_.world_origin.y = 0;
    params_.world_origin.z = 0;
    SetParameters(&params_);
    num_cell_ = params_.grid_size.x * params_.grid_size.y * params_.grid_size.z;
    dh_mem_.Malloc(num_part_, num_cell_);
    std::cout << "finished function: SysInit\n";
}

void MDSimpleSand::GenerateGrid()
{
    double gridx = 10.0;
    double gridy = 10.0;
    double gridz = 5.5;
    srand(0);
    uint m;
    double lastx = 0;
    double y_row = gridy;
    for (uint i = 0; i < num_part_; ++i)
    {
        m = static_cast<double> (rand()) / RAND_MAX;
        dh_mem_.hrad[i] = params_.radius_min + (params_.radius_max - params_.radius_min) * m;
        dh_mem_.mass[i] = pow((dh_mem_.hrad[i] / params_.radius_min), 3) * params_.mass_min;
        dh_mem_.hpos[i].x = lastx + gridx;
        if(dh_mem_.hpos[i].x + dh_mem_.hrad[i] > params_.box_length.x)
        {
            dh_mem_.hpos[i].x = gridx;
            lastx = gridx;
            y_row = y_row + gridy;
            if(y_row + dh_mem_.hrad[i] > params_.box_length.y)
                break;
        }
        lastx = dh_mem_.hpos[i].x;
        dh_mem_.hpos[i].y = y_row;
        dh_mem_.hpos[i].z = gridz;
    }
    dh_mem_.MemCpyHostToDevice();
    unsigned ngrp = 1;
    std::vector<unsigned> nbin(ngrp, 9);
    fhandle_.CreateFile("san.h5", 1, nbin);
    fhandle_.SetGroup(0);
    std::vector<FloatType> plist(6);
    plist[0] = MD_DT;
    plist[1] = MD_KN;
    plist[2] = MD_GN;
    plist[3] = 0;
    plist[4] = 0;
    plist[5] = MD_MU;

    fhandle_.WriteAttr(plist);
    std::vector<FloatType> boxd(6);
    boxd[3] = params_.box_length.x;
    boxd[4] = params_.box_length.y;
    boxd[5] = params_.box_length.z;
    fhandle_.WriteBoxD(boxd);
    fhandle_.CloseFile();
    fhandle_.OpenFile("san.h5", MD_HDF5_Handle::h5_rdwr);
}

void MDSimpleSand::GenerateSampleSand()
{
    srand(0);
    int i; 
    double m;
    double inner_x_lb = params_.radius_max;
    double inner_x_ub = params_.box_length.x - params_.radius_max;
    double inner_y_lb = params_.radius_max;
    double inner_y_ub = params_.box_length.y - params_.radius_max;
    double inner_z_lb = params_.radius_max;
    double lastx = inner_x_lb;
    double z_row = inner_z_lb, y_row = inner_y_lb;

    for (i = 0; i < num_part_; ++i)
    {
        m = static_cast<double> (rand()) / RAND_MAX;
        dh_mem_.hrad[i] = params_.radius_min + (params_.radius_max - params_.radius_min) * m;
        dh_mem_.mass[i] = pow((dh_mem_.hrad[i] / params_.radius_min), 3) * params_.mass_min;
        m = static_cast<double> (rand()) / RAND_MAX;
        dh_mem_.hpos[i].x = lastx + dh_mem_.hrad[i] * (1 + 0.1 * m);
        if (dh_mem_.hpos[i].x + dh_mem_.hrad[i] > inner_x_ub)
        {
            dh_mem_.hpos[i].x = inner_x_lb + dh_mem_.hrad[i] * (1 + 0.1 * m);
            lastx = inner_x_lb;
            y_row += 2 * params_.radius_max;
            if (y_row +dh_mem_.hrad[i] > inner_y_ub)
            {
                m = static_cast<double> (rand()) / RAND_MAX;
                y_row = inner_y_lb + dh_mem_.hrad[i] * (1 + 0.1 * m);
                z_row += 2 * params_.radius_max;
            }
        }
        lastx = dh_mem_.hpos[i].x + dh_mem_.hrad[i];
        dh_mem_.hpos[i].y = z_row + dh_mem_.hrad[i];
        dh_mem_.hpos[i].z = y_row + dh_mem_.hrad[i];
        dh_mem_.hvel[i].x = 0 * static_cast<double> (rand()) / RAND_MAX;
        dh_mem_.hvel[i].z = 0 * static_cast<double> (rand()) / RAND_MAX;
    }
    dh_mem_.MemCpyHostToDevice();

    unsigned ngrp = 1;
    std::vector<unsigned> nbin(ngrp, 9);
    fhandle_.CreateFile("san.h5", 1, nbin);
    fhandle_.SetGroup(0);
    std::vector<FloatType> plist(6);
    plist[0] = MD_DT;
    plist[1] = MD_KN;
    plist[2] = MD_GN;
    plist[3] = 0;
    plist[4] = 0;
    plist[5] = MD_MU;
    fhandle_.WriteAttr(plist);
    std::vector<FloatType> boxd(6);
    boxd[3] = params_.box_length.x;
    boxd[4] = params_.box_length.y;
    boxd[5] = params_.box_length.z;
    fhandle_.WriteBoxD(boxd);
}

void MDSimpleSand::PPCollidePhase()
{
    dh_mem_.mapPosPtr();
    InitGridHash();
    SortGridHash();
    CollideCells();
}

void MDSimpleSand::IntegratePhase()
{
    IntegratePar();
    BoundaryCond();
    sys_step_++;
    dh_mem_.unMapPosPtr();
}


void MDSimpleSand::BenchMark(uint steps)
{
    GenerateSampleSand();
    for(uint i = 0; i < steps; ++i)
    {
        if(i % MD_SAVE_STEP == 0)
        {
            dh_mem_.MemCpyDeviceToHost();
            OutputFile();
            std::cout << sys_step_ * params_.time_step << std::endl; 
        }
        PPCollidePhase();
        IntegratePhase();
    }
}

void MDSimpleSand::ResizeSandArray(FloatType* fnorm)
{
    GPUResizeSandArray(fnorm, num_part_, dh_mem_);
}

void MDSimpleSand::LoadFile(const char* name)
{
    fhandle_.OpenFile(name, MD_HDF5_Handle::h5_rdonly);
    fhandle_.SetGroup(0);
    num_part_ = fhandle_.ReadNumberParticles();
    std::vector<FloatType> buffer_bd(6);
    std::vector<FloatType> buffer_rr(num_part_);	
    fhandle_.ReadBoxD(buffer_bd);
    fhandle_.ReadGeoR(buffer_rr);
    FloatType min_radius = *std::min_element(buffer_rr.begin(), buffer_rr.end());
    FloatType max_radius = *std::max_element(buffer_rr.begin(), buffer_rr.end());
    //	
    params_.box_length.x = buffer_bd[3];
    params_.box_length.y = buffer_bd[4] + 10;
    params_.box_length.z = buffer_bd[5];
    std::cout << params_.box_length.z << std::endl;
    //
    params_.radius_min = min_radius;
    params_.radius_max = max_radius;
    fhandle_.ReadParDensity(params_.rho);
    params_.mass_min = params_.rho * pow(params_.radius_min, 3);
    params_.mass_max = params_.rho * pow(params_.radius_max, 3);
    //
    fhandle_.ReadNSpringCoe(params_.kn);
    params_.kt = 0;
    fhandle_.ReadNViscosity(params_.gn);
    fhandle_.ReadPPFriction(params_.mu);
    params_.mu = MD_MU;
    std::cout << params_.rho << " " << params_.kn << " " << params_.gn << " " << params_.mu << "\n";
    // region geometry
    params_.cell_size.x = MD_CUDA_CELL_SCALE * params_.radius_max * 2;
    params_.cell_size.y = MD_CUDA_CELL_SCALE * params_.radius_max * 2;
    params_.cell_size.z = MD_CUDA_CELL_SCALE * params_.radius_max * 2;
    params_.grid_size.x = params_.box_length.x / params_.cell_size.x + 2;
    params_.grid_size.y = params_.box_length.y / params_.cell_size.y + 2;
    params_.grid_size.z = params_.box_length.z / params_.cell_size.z + 2;
    // initial system time and time step, time step length
    params_.world_origin.x = 0;
    params_.world_origin.y = 0;
    params_.world_origin.z = 0;
    SetParameters(&params_);
    num_cell_ = params_.grid_size.x * params_.grid_size.y * params_.grid_size.z;
    dh_mem_.Malloc(num_part_, num_cell_);

    std::vector<FloatType> buffer_rx(num_part_);
    std::vector<FloatType> buffer_ry(num_part_);
    std::vector<FloatType> buffer_rz(num_part_);
    std::vector<FloatType> buffer_px(num_part_);
    std::vector<FloatType> buffer_py(num_part_);
    std::vector<FloatType> buffer_pz(num_part_);

    size_t offset[2] = {0, 0};
    size_t stride[2] = {1, 1};
    size_t count[2]  = {1, num_part_};
    size_t block[2]  = {1, 1};
    fhandle_.SetSlice(offset, stride, count, block);
    fhandle_.ReadData(MD_HDF5_Handle::rx, buffer_rx);
    fhandle_.ReadData(MD_HDF5_Handle::ry, buffer_ry);
    fhandle_.ReadData(MD_HDF5_Handle::rz, buffer_rz);
    fhandle_.ReadData(MD_HDF5_Handle::px, buffer_px);
    fhandle_.ReadData(MD_HDF5_Handle::py, buffer_py);
    fhandle_.ReadData(MD_HDF5_Handle::pz, buffer_pz);

    for(uint i = 0; i < num_part_; ++i)
    {
        dh_mem_.hpos[i].x = buffer_rx[i];
        dh_mem_.hpos[i].y = buffer_ry[i];
        dh_mem_.hpos[i].z = buffer_rz[i];
        dh_mem_.hrad[i]   = buffer_rr[i];
        if(buffer_rr[i] > 2 * params_.cell_size.x)
        {
            bdindex_.push_back(i);
        }
    }
    dh_mem_.MemCpyHostToDevice();
    fhandle_.CloseFile();
    std::cout << "done: loading file\n";
    std::vector<uint> nbin(1, 9);
    fhandle_.CreateFile("new.h5", 1, nbin);
    fhandle_.SetGroup(0);
    std::vector<FloatType> plist(6);
    plist[0] = params_.rho;
    plist[1] = params_.kn;
    plist[2] = params_.gn;
    plist[3] = params_.kt;
    plist[4] = params_.gt;
    plist[5] = params_.mu;
    fhandle_.WriteAttr(plist);
    std::vector<FloatType> boxd(6);
    boxd[3] = params_.box_length.x;
    boxd[4] = params_.box_length.y;
    boxd[5] = params_.box_length.z - 10;
    fhandle_.WriteBoxD(boxd);
}

void MDSimpleSand::OutputFile()
{
    dh_mem_.MemCpyDeviceToHost();
    std::vector<FloatType> buffer_rx(num_part_);
    std::vector<FloatType> buffer_ry(num_part_);
    std::vector<FloatType> buffer_rz(num_part_);
    std::vector<FloatType> buffer_px(num_part_);
    std::vector<FloatType> buffer_py(num_part_);
    std::vector<FloatType> buffer_pz(num_part_);
    std::vector<FloatType> buffer_wx(num_part_);
    std::vector<FloatType> buffer_wy(num_part_);
    std::vector<FloatType> buffer_wz(num_part_);
    std::vector<FloatType> buffer_rr(num_part_);
    for(uint i = 0; i < num_part_; ++i)
    {
        buffer_rx[i] = dh_mem_.hpos[i].x;
        buffer_ry[i] = dh_mem_.hpos[i].y;
        buffer_rz[i] = dh_mem_.hpos[i].z;
        buffer_px[i] = dh_mem_.hvel[i].x;
        buffer_py[i] = dh_mem_.hvel[i].y;
        buffer_pz[i] = dh_mem_.hvel[i].z;
        buffer_wx[i] = dh_mem_.home[i].x;
        buffer_wy[i] = dh_mem_.home[i].y;
        buffer_wz[i] = dh_mem_.home[i].z;
        buffer_rr[i] = dh_mem_.hrad[i];
    }
    //std::cout << buffer_wx[0] << "" << std::endl;
    size_t offset[2] = {ios_step_,0};
    size_t stride[2] = {1, 1};
    size_t count[2]  = {1, num_part_};
    size_t block[2]  = {1, 1};
    fhandle_.SetSlice(offset, stride, count, block);
    fhandle_.WriteData(MD_HDF5_Handle::rx, buffer_rx);
    fhandle_.WriteData(MD_HDF5_Handle::ry, buffer_ry);
    fhandle_.WriteData(MD_HDF5_Handle::rz, buffer_rz);
    fhandle_.WriteData(MD_HDF5_Handle::px, buffer_px);
    fhandle_.WriteData(MD_HDF5_Handle::py, buffer_py);
    fhandle_.WriteData(MD_HDF5_Handle::pz, buffer_pz);
    fhandle_.WriteData(MD_HDF5_Handle::wx, buffer_wx);
    fhandle_.WriteData(MD_HDF5_Handle::wy, buffer_wy);
    fhandle_.WriteData(MD_HDF5_Handle::wz, buffer_wz);
    fhandle_.WriteGeoR(buffer_rr);
    std::vector<FloatType> current_time(1);
    current_time[0] = sys_step_ * params_.time_step;
    fhandle_.WriteTime(current_time);
    ios_step_++;
}

uint MDSimpleSand::CopyDevicePtr(FloatType** p_pos_ptr, FloatType** p_vel_ptr, FloatType** p_ome_ptr, FloatType** p_foc_ptr, FloatType** p_mom_ptr)
{
    *p_foc_ptr = dh_mem_.p_dfoc;
    *p_mom_ptr = dh_mem_.p_dmom;
    *p_pos_ptr = dh_mem_.p_dpos;
    *p_vel_ptr = dh_mem_.p_dvel;
    *p_ome_ptr = dh_mem_.p_dome;
    return num_part_;
}

#endif
