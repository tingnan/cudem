/* 
 * File:   MDsand.h
 * Author: tingnan
 *
 * Created on January 14, 2011, 1:42 PM
 */

#ifndef _MDSAND_H_
#define _MDSAND_H_
#include "md_header.h"
#include "HDF/md_hdf5.h"

#ifdef __ENABLE_CUDA__

#include "md_sand_cuda.cuh"

class MDSimpleSand
{
protected:
    SimParams params_;
    MD_Particle_Memory dh_mem_;
    uint num_part_;
    uint num_cell_;
    uint sys_step_;
    uint ios_step_;
	std::vector<uint> bdindex_;
    MD_HDF5_Handle fhandle_;
    void InitGridHash();
    void SortGridHash();
    void CollideCells();
    void IntegratePar();
    void BoundaryCond();
    void Finalize();
    void SysInit(uint npar, FloatType lx, FloatType ly, FloatType lz);
public:
	MDSimpleSand();
    MDSimpleSand(uint npar, FloatType lx, FloatType ly, FloatType lz);
    ~MDSimpleSand();
    void GenerateSampleSand();
    void GenerateGrid();
    void BenchMark(uint);
    void Precompute();
    void OutputFile();
    void LoadFile(const char*);
    void PPCollidePhase();
    void IntegratePhase();
    void ResizeSandArray(FloatType*);
    uint NumParts() {return num_part_;};
    FloatType CurrStep() {return sys_step_ * params_.time_step;};
    uint CopyDevicePtr(FloatType** , FloatType**, FloatType**, FloatType**, FloatType**);
    uint getVBO()
    {
        return dh_mem_.getVBO();
    }
};

#endif

#endif
