/*
 * md_sand_cuda.cuh
 *
 *  Created on: Nov 26, 2012
 *      Author: tingnan
 */
#ifndef _MD_SAND_CUDA_CUH_
#define _MD_SAND_CUDA_CUH_


#include "MathHelper/md_vec_cuda.cuh"
#include <thrust/device_vector.h>
#include <vector>

typedef unsigned int uint;


struct MD_Particle_Memory
{
	typedef std::vector<FloatType> farray;
	typedef std::vector<uint> iarray;
	typedef std::vector<vec3> varray;
	friend void MD_Particle_Malloc(MD_Particle_Memory&, uint, uint);
protected:
	farray hf;
	farray hm;
	farray hp;
	farray hv;
    farray hw;
	thrust::device_vector<FloatType> dfoc;
	thrust::device_vector<FloatType> dmom;
	thrust::device_vector<FloatType> dvel;
    thrust::device_vector<FloatType> dome;
	thrust::device_vector<FloatType> sorted_dpos;
	thrust::device_vector<FloatType> sorted_dvel;
    thrust::device_vector<FloatType> sorted_dome;
	thrust::device_vector<FloatType> sorted_dfoc;
	thrust::device_vector<FloatType> sorted_dmom;
	thrust::device_vector<uint> grid_hash;
	thrust::device_vector<uint> grid_indx;
	thrust::device_vector<uint> cell_beg;
	thrust::device_vector<uint> cell_end;
	struct cudaGraphicsResource *dpos_vbo_resource;
	uint dpos_vbo;
	void PushInternalMemory();
	void PullInternalMemory();

	// length;
	uint numpar;
	uint numcell;
	uint veclength;
	
public:
	// Device memory;
	FloatType* p_dfoc;
	FloatType* p_dmom;
	FloatType* p_dpos;
	FloatType* p_dvel;
    FloatType* p_dome;
	FloatType* s_dpos;
	FloatType* s_dvel;
    FloatType* s_dome;
	FloatType* s_dfoc;
	FloatType* s_dmom;
	uint* p_grid_hash;
	uint* p_grid_indx;
	uint* p_cell_beg;
	uint* p_cell_end;

	// host memory;
	farray hrad;
	farray mass;
	varray hfoc;
	varray hmom;
	varray hpos;
	varray hvel;
    varray home;

	void MemCpyHostToDevice();
	void MemCpyDeviceToHost();
	void Malloc(uint npar, uint ncell);

	void Resize(int npar)
	{
		Malloc(npar, numcell);
	}
	uint getVBO()
	{
		return dpos_vbo;
	}
	// they have to be called in pairs in the update function, or p_dpos pointer is not going to work;
	void mapPosPtr();
	void unMapPosPtr();
};

// the host memory structure has protected memory arrays that has the same size as device memory

struct SimParams
{
    vec3 box_length;
    vec3 world_origin;
    vec3 cell_size;
    uint3 grid_size;
    FloatType radius_min;
    FloatType radius_max;
    FloatType mass_min;
    FloatType mass_max;
    FloatType rho;
    FloatType kn;
    FloatType kt;
    FloatType gn;
    FloatType gt;
    FloatType mu;
    FloatType time_step;
};

void GPUSortArray( uint* gridHash,
        uint* gridIndex,
        uint  numberParticles );

void GPUHashCheck(  uint*  p_grid_hash,
                    vec4* p_pos,
                    uint  num_par,
                    uint  num_cell);

void GPUReorderArray( uint* sortedGridHash,
        uint* sortedGridIndex,
        uint  numberParticles,
        uint  numberCells,
        uint* cellBeginning,
        uint* cellEnding,
        vec4* position,
        vec4* velocity,
        vec4* avelocity,
        vec4* sortedPosition,
        vec4* sortedVelocity,
        vec4* sortedAvelocity);

void GPUCalcHash( vec4* position,
        uint  numberParticles,
        uint* gridHash,
        uint* gridIndex );

void SetParameters( SimParams* hparams );

void GPUCollideCells( uint* sortedGridHash,
        uint* cellBeginning,
        uint* cellEnding,
        uint  numberParticles,
        uint  numberCells,
        vec4* sortedPosition,
        vec4* sortedVelocity,
        vec4* sortedAvelocity,
        vec4* force, 
        vec4* moment);

void GPUCollideBoulders( uint* boulderIndex,
        uint  boulderArraySize,
        vec4* position,
        vec4* velocity,
        uint  numberParticles,
        vec4* force );


void GPUIntegrate( uint  numberParticles,
        vec4* position,
        vec4* velocity,
        vec4* avelocity,
        vec4* force,
        vec4* moment);

void GPUIntegrate( uint  currentTimeStep,
        uint  numberParticles,
        vec4* position,
        vec4* velocity,
        vec4* force );

void GPUBoundary( uint  numberParticles,
        vec4* position,
        vec4* velocity );

void GPUBoundary( uint  currentTimeStep,
        uint  numberParticles,
        vec4* position,
        vec4* velocity );

void GPUResizeSandArray(FloatType* fnorm, uint& num_par, MD_Particle_Memory& dh_mem);
#endif /* MD_SAND_CUDA_CUH_ */
