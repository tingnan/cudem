#include <GL/glew.h>

#include <cuda_runtime.h>
#include <cuda_gl_interop.h>
#include "md_sand_cuda.cuh"
#include "GeneralHelper/md_cuda_helper.cuh"


GLuint createVBO(uint size)
{
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, size, 0, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    return vbo;
}

void registerGLBufferObject(uint vbo, struct cudaGraphicsResource **cuda_vbo_resource)
{
    GPUCheckErrors(cudaGraphicsGLRegisterBuffer(cuda_vbo_resource, vbo,
                                                 cudaGraphicsMapFlagsNone));
}

void unregisterGLBufferObject(struct cudaGraphicsResource *cuda_vbo_resource)
{
    GPUCheckErrors(cudaGraphicsUnregisterResource(cuda_vbo_resource));
}

void *mapGLBufferObject(struct cudaGraphicsResource **cuda_vbo_resource)
{
    void *ptr;
    GPUCheckErrors(cudaGraphicsMapResources(1, cuda_vbo_resource, 0));
    size_t num_bytes;
    GPUCheckErrors(cudaGraphicsResourceGetMappedPointer((void **)&ptr, &num_bytes,
                                                         *cuda_vbo_resource));
    return ptr;
}

void unmapGLBufferObject(struct cudaGraphicsResource *cuda_vbo_resource)
{
    GPUCheckErrors(cudaGraphicsUnmapResources(1, &cuda_vbo_resource, 0));
}


void copyArrayDeviceToHost(void *host, const void *device,
                         struct cudaGraphicsResource **cuda_vbo_resource, int size)
{
    if (cuda_vbo_resource)
    {
        device = mapGLBufferObject(cuda_vbo_resource);
    }

    GPUCheckErrors(cudaMemcpy(host, device, size, cudaMemcpyDeviceToHost));

    if (cuda_vbo_resource)
    {
    	unmapGLBufferObject(*cuda_vbo_resource);
    }
}

void MD_Particle_Memory::MemCpyHostToDevice()
{
	PushInternalMemory();
	cudaMemcpy(p_dfoc, &hf[0], veclength * sizeof(FloatType), cudaMemcpyHostToDevice);
	cudaMemcpy(p_dmom, &hm[0], veclength * sizeof(FloatType), cudaMemcpyHostToDevice);
	cudaMemcpy(p_dvel, &hv[0], veclength * sizeof(FloatType), cudaMemcpyHostToDevice);
	cudaMemcpy(p_dome, &hw[0], veclength * sizeof(FloatType), cudaMemcpyHostToDevice);

	// the position is using vbo
	unregisterGLBufferObject(dpos_vbo_resource);
    glBindBuffer(GL_ARRAY_BUFFER, dpos_vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, veclength * sizeof(FloatType), &hp[0]);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    registerGLBufferObject(dpos_vbo, &dpos_vbo_resource);

}


void MD_Particle_Memory::MemCpyDeviceToHost()
{
	cudaMemcpy(&hf[0], p_dfoc, veclength * sizeof(FloatType), cudaMemcpyDeviceToHost);
	cudaMemcpy(&hm[0], p_dmom, veclength * sizeof(FloatType), cudaMemcpyDeviceToHost);
	copyArrayDeviceToHost(&hp[0], p_dpos, &dpos_vbo_resource, veclength * sizeof(FloatType));
	cudaMemcpy(&hv[0], p_dvel, veclength * sizeof(FloatType), cudaMemcpyDeviceToHost);
	cudaMemcpy(&hw[0], p_dome, veclength * sizeof(FloatType), cudaMemcpyDeviceToHost);
	PullInternalMemory();
}


void MD_Particle_Memory::PushInternalMemory()
{
	for(int i = 0; i < numpar; ++i)
	{
		hf[i * 4 + 0] = hfoc[i].x;
		hf[i * 4 + 1] = hfoc[i].y;
		hf[i * 4 + 2] = hfoc[i].z;
		hm[i * 4 + 0] = hmom[i].x;
		hm[i * 4 + 1] = hmom[i].y;
		hm[i * 4 + 2] = hmom[i].z;
		hv[i * 4 + 0] = hvel[i].x;
		hv[i * 4 + 1] = hvel[i].y;
		hv[i * 4 + 2] = hvel[i].z;
		hw[i * 4 + 0] = home[i].x;
		hw[i * 4 + 1] = home[i].y;
		hw[i * 4 + 2] = home[i].z;
		hp[i * 4 + 0] = hpos[i].x;
		hp[i * 4 + 1] = hpos[i].y;
		hp[i * 4 + 2] = hpos[i].z;
		hp[i * 4 + 3] = hrad[i];
	}
}
void MD_Particle_Memory::PullInternalMemory()
{
	for(int i = 0; i < numpar; ++i)
	{
		hfoc[i].x = hf[i * 4 + 0];
		hfoc[i].y = hf[i * 4 + 1];
		hfoc[i].z = hf[i * 4 + 2];
		hmom[i].x = hm[i * 4 + 0];
		hmom[i].y = hm[i * 4 + 1];
		hmom[i].z = hm[i * 4 + 2];
		hvel[i].x = hv[i * 4 + 0];
		hvel[i].y = hv[i * 4 + 1];
		hvel[i].z = hv[i * 4 + 2];
		home[i].x = hw[i * 4 + 0];
		home[i].y = hw[i * 4 + 1];
		home[i].z = hw[i * 4 + 2];
		hpos[i].x = hp[i * 4 + 0];
		hpos[i].y = hp[i * 4 + 1];
		hpos[i].z = hp[i * 4 + 2];
		hrad[i]   = hp[i * 4 + 3];
	}
}

void MD_Particle_Memory::mapPosPtr()
{
	p_dpos = (FloatType*) mapGLBufferObject(&dpos_vbo_resource);
}

void MD_Particle_Memory::unMapPosPtr()
{
	unmapGLBufferObject(dpos_vbo_resource);
	p_dpos = NULL;
}



void MD_Particle_Memory::Malloc(uint npar, uint ncell)
{
	numpar = npar;
	veclength = npar * 4;
	numcell = ncell;
	hf.resize(veclength);
	hm.resize(veclength);
	hp.resize(veclength);
	hv.resize(veclength);
    hw.resize(veclength);
	hrad.resize(npar);
	hpos.resize(npar);
	hvel.resize(npar);
    home.resize(npar);
	hfoc.resize(npar);
	hmom.resize(npar);
	mass.resize(npar);
	// dpos.resize(npar * 4);
	dvel.resize(veclength);
    dome.resize(veclength);
	dfoc.resize(veclength);
	dmom.resize(veclength);
	// p_dpos = thrust::raw_pointer_cast(&(dpos[0]));
	// use vbo instead
	dpos_vbo = createVBO(veclength * sizeof(FloatType));
	registerGLBufferObject(dpos_vbo, &dpos_vbo_resource);

	// we will not do p_dpos untill we are going to compute;
	p_dvel = thrust::raw_pointer_cast(&(dvel[0]));
	p_dome = thrust::raw_pointer_cast(&(dome[0]));
	p_dfoc = thrust::raw_pointer_cast(&(dfoc[0]));
	p_dmom = thrust::raw_pointer_cast(&(dmom[0]));
	sorted_dpos.resize(veclength);
	sorted_dvel.resize(veclength);
    sorted_dome.resize(veclength);
	sorted_dfoc.resize(veclength);
	sorted_dmom.resize(veclength);
	s_dpos = thrust::raw_pointer_cast(&(sorted_dpos[0]));
	s_dvel = thrust::raw_pointer_cast(&(sorted_dvel[0]));
    s_dome = thrust::raw_pointer_cast(&(sorted_dome[0]));
	s_dfoc = thrust::raw_pointer_cast(&(sorted_dfoc[0]));
	s_dmom = thrust::raw_pointer_cast(&(sorted_dmom[0]));

	// grid(cell) array
	grid_hash.resize(npar);
	grid_indx.resize(npar);
	cell_beg.resize(ncell);
	cell_end.resize(ncell);
	p_grid_hash = thrust::raw_pointer_cast(&(grid_hash[0]));
	p_grid_indx = thrust::raw_pointer_cast(&(grid_indx[0]));
	p_cell_beg =  thrust::raw_pointer_cast(&(cell_beg[0]));
	p_cell_end =  thrust::raw_pointer_cast(&(cell_end[0]));
	std::cout << "done Malloc\n";
}
