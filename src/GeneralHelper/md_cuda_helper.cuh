/*
 * md_cuda_helper.cuh
 *
 *  Created on: Jan 15, 2013
 *      Author: tingnan
 */

#ifndef _MD_CUDA_HELPER_CUH_
#define _MD_CUDA_HELPER_CUH_
#include "md_header.h"
#include "MathHelper/md_vec_cuda.cuh"
#include <cstdio>


#define GPUCheckErrors(error)                                        \
{                                                                                 \
	if(error != cudaSuccess)                                                      \
	{                                                                             \
		fprintf(stderr,"GPU error: %s in file '%s' in line %i : %s.\n",          \
			"memory error:", __FILE__, __LINE__, cudaGetErrorString(error));      \
		exit(-1);                                                                 \
	}                                                                             \
}

void inline KernelError(const char* msg)
{
	cudaError_t error = cudaGetLastError();
	if(error != cudaSuccess)
	{
		// print the CUDA error message and exit
		std::cout << msg << std::endl;
		printf("CUDA error: %s\n", cudaGetErrorString(error));
		exit(-1);
	}
}

inline __device__ __host__ uint idiv(uint a, uint b)
{
	return (a % b != 0) ? (a / b + 1) : (a / b);
}

void inline ComputeGridSize(uint n, uint blockSize, uint &nblocks, uint &nthreads)
{
	nthreads = blockSize < n? blockSize : n;
	nblocks =  idiv(n, nthreads);
}

#if USE_SINGLE
static const int WORK_SIZE = 512;
#endif

#if USE_DOUBLE
static const int WORK_SIZE = 256;
#endif


#if USE_DOUBLE
static inline __device__ double int2_to_double(int2 v)

{
	return __hiloint2double(v.y, v.x);
}

static inline __device__ double4 fetch_double4(texture<int4, 1> t, int i)
{
	int4 v1 = tex1Dfetch(t, 2 * i);
	int4 v2 = tex1Dfetch(t, 2 * i + 1);
	double d1 = int2_to_double(make_int2(v1.x, v1.y));
	double d2 = int2_to_double(make_int2(v1.z, v1.w));
	double d3 = int2_to_double(make_int2(v2.x, v2.y));
	double d4 = int2_to_double(make_int2(v2.z, v2.w));
	return make_double4(d1, d2, d3, d4);
}
#endif

static inline __device__ uint iIndex()
{
#if __CUDA_ARCH__ <= 200
	return __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
#else
	return blockIdx.x * blockDim.x + threadIdx.x;
#endif
}

static inline __device__ uint iThread()
{
	return threadIdx.x;
}

static inline __device__ uint iBlock()
{
	return blockDim.x;
}


#endif /* _MD_CUDA_HELPER_CUH_ */
