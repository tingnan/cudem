#include "md_hdf5.h"
#include <cstdlib>
#include <sstream>
#include <cassert>
#include <iomanip>
#include "hdf5.h"
using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::pair;
string switch_bin_name(int i)
{
    string bname;
    switch (i) 
    {
    case md_hdf5_posx:
        bname = "rx";
        break;
    case md_hdf5_posy:
        bname = "ry";
        break;
    case md_hdf5_posz:
        bname = "rz";
        break;
    case md_hdf5_velx:
        bname = "px";
        break;
    case md_hdf5_vely:
        bname = "py";
        break;
    case md_hdf5_velz:
        bname = "pz";
        break;
    case md_hdf5_omex:
        bname = "wx";
        break;
    case md_hdf5_omey:
        bname = "wy";
        break;
    case md_hdf5_omez:
        bname = "wz";
        break;
    case md_hdf5_focx:
        bname = "fx";
        break;
    case md_hdf5_focy:
        bname = "fy";
        break;
    case md_hdf5_focz:
        bname = "fz";
        break;
    default:
        assert(0 && "bin number is out of bound!!");
    }
    return bname;
}

#ifdef USE_HDF5

#define CHUNK_DIM_1 1
#define CHUNK_DIM_2 1000

typedef std::pair<string, hid_t> h5pair;


herr_t MD_HDF5::md_h5_fopen(hid_t g_id, const char* name, const struct H5L_info_t* statbuf)
{
    string tmp;
    size_t found;
    hid_t loc_id = H5Oopen_by_addr(g_id, statbuf->u.address);
    H5I_type_t obj_type = H5Iget_type(loc_id);
    if (name[0] == '.');
    else
        switch (obj_type)
        {
            case H5I_GROUP: 
                gid_.push_back(H5Gopen(fid_, name, H5P_DEFAULT));
                gnm_.push_back(name);
                grp_.push_back(MD_HDF5_GRP());
                cout << "Object is a group with name: /" << name << "\n";
                cout << "Iterate into subgroups now...\n";
                break;
            case H5I_DATASET:
                tmp = name;
                // we only need to store the relative name to the map
                found = tmp.find_last_of("/");
                cout << "Object is a dataset with name: /" << name << "\n";
                grp_.back().dtmap.insert(h5pair(tmp.substr(found + 1), H5Dopen(fid_, name, H5P_DEFAULT)));
                break;
            case H5I_DATATYPE: 
                cout << "Object is a type with name: /" << name << "\n";
                break;
            default:
                cout << " Unable to identify an object \n";
        }
    return 0;
}


#if USE_SINGLE == 1
#define MD_NATIVE_FLOAT H5T_IEEE_F32LE
#endif

#if USE_DOUBLE == 1
#define MD_NATIVE_FLOAT H5T_IEEE_F64LE
#endif

herr_t MD_HDF5::md_h5_grp_aopen(MD_HDF5_GRP& grp, hid_t gid)
{
	hid_t status;
	hid_t loc_id;
	loc_id = H5Aopen(gid, "rho", H5P_DEFAULT);
    status = H5Aread(loc_id, MD_NATIVE_FLOAT, &(grp.rho));
    H5Aclose(loc_id);
    loc_id = H5Aopen(gid, "kn", H5P_DEFAULT);
    status = H5Aread(loc_id, MD_NATIVE_FLOAT, &(grp.kn));
    H5Aclose(loc_id);
    loc_id = H5Aopen(gid, "gn", H5P_DEFAULT);
    status = H5Aread(loc_id, MD_NATIVE_FLOAT, &(grp.gn));
    H5Aclose(loc_id);
    loc_id = H5Aopen(gid, "kt", H5P_DEFAULT);
    status = H5Aread(loc_id, MD_NATIVE_FLOAT, &(grp.kt));
    H5Aclose(loc_id);
    loc_id = H5Aopen(gid, "gt", H5P_DEFAULT);
    status = H5Aread(loc_id, MD_NATIVE_FLOAT, &(grp.gt));
    H5Aclose(loc_id);
    loc_id = H5Aopen(gid, "mu", H5P_DEFAULT);
    status = H5Aread(loc_id, MD_NATIVE_FLOAT, &(grp.mu));
    H5Aclose(loc_id);
    //loc_id = H5Aopen(gid, "cellsize", H5P_DEFAULT);
    //status = H5Aread(loc_id, MD_NATIVE_FLOAT, &(grp.cellsize));
    //H5Aclose(loc_id);
    return status;
}


MD_HDF5::MD_HDF5()
{
    //void
    fid_ = 0;
    //default precision
    SetPrecision(H5T_IEEE_F32LE);
}

void MD_HDF5::SetPrecision(hid_t pre)
{
	h5_precision_ = pre;
}

MD_HDF5::~MD_HDF5()
{
	CloseFile();
}



// MD_HDF5::~MD_HDF5()
// {
//     //void
//     delete [] coor_;
//     H5close();
// }

// need some rework to use std::map
void MD_HDF5::CreateFile(string fname, unsigned ngrp, const std::vector<unsigned>& nbin)
{
    assert(ngrp == nbin.size());
    ngrp_ = ngrp;
    gid_.resize(ngrp_);
    grp_.resize(ngrp_);
    fid_ = H5Fcreate(fname.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    int i;
    string gname;
    std::stringstream buffer;
    hsize_t d1[1] = {1}, d2[2] = {1, 1}, d3[1] = {6};
    hsize_t dm[2] = {H5S_UNLIMITED, H5S_UNLIMITED};
    hid_t dsid1 = H5Screate_simple(1, d1, NULL);
    hid_t msid1 = H5Screate_simple(1, d1, NULL);
    hid_t dsid2 = H5Screate_simple(2, d2, dm);
    hid_t dsid3 = H5Screate_simple(1, d3, NULL);
    
    hsize_t chunk_dims[2] = {CHUNK_DIM_1, 1};
    hid_t cparms = H5Pcreate (H5P_DATASET_CREATE);
    H5Pset_chunk (cparms, 2, chunk_dims);
    
    unsigned int avail = H5Zfilter_avail(H5Z_FILTER_DEFLATE);
    if (!avail) {
        printf ("gzip filter not available.\n");
        exit(1);
    }
    unsigned int filter_info;
    int status = H5Zget_filter_info (H5Z_FILTER_DEFLATE, &filter_info);
    if ( !(filter_info & H5Z_FILTER_CONFIG_ENCODE_ENABLED) ||
                !(filter_info & H5Z_FILTER_CONFIG_DECODE_ENABLED) ) {
        printf ("gzip filter not available for encoding and decoding.\n");
        exit(1);
    }
 	H5Pset_shuffle (cparms);
	H5Pset_deflate (cparms, 4);
    
//     int szip_options_mask = H5_SZIP_NN_OPTION_MASK;
//     int szip_pixels_per_block = 128;
//     H5Pset_szip(cparms, szip_options_mask, szip_pixels_per_block);
    
    for(i = 0; i < ngrp_; ++i)
    {
        assert(nbin[i] >= 6 && "at least should have x, y, z, px, py, pz");
        grp_[i].nbin = nbin[i];
        buffer.str("");
        buffer << i;
        gname = "/Group" + buffer.str();
        gid_[i] = H5Gcreate(fid_, gname.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        hid_t aid = H5Acreate(gid_[i] , "rho", h5_precision_, dsid1, H5P_DEFAULT, H5P_DEFAULT);
        H5Aclose(aid);
        aid = H5Acreate(gid_[i] , "kn", h5_precision_, dsid1, H5P_DEFAULT, H5P_DEFAULT);
        H5Aclose(aid);
        aid = H5Acreate(gid_[i] , "kt", h5_precision_, dsid1, H5P_DEFAULT, H5P_DEFAULT);
        H5Aclose(aid);
        aid = H5Acreate(gid_[i] , "gn", h5_precision_, dsid1, H5P_DEFAULT, H5P_DEFAULT);
        H5Aclose(aid);
        aid = H5Acreate(gid_[i] , "gt", h5_precision_, dsid1, H5P_DEFAULT, H5P_DEFAULT);
        H5Aclose(aid);
        aid = H5Acreate(gid_[i] , "mu", h5_precision_, dsid1, H5P_DEFAULT, H5P_DEFAULT);
        H5Aclose(aid);
        //aid = H5Acreate(gid_[i] , "cellsize", h5_precision_, dsid1, H5P_DEFAULT, H5P_DEFAULT);
        //H5Aclose(aid);

        //
        // no data except for nbin is filled at the moment of creation
        // create timeseries 
        grp_[i].ttid = H5Dcreate(gid_[i], "timeseries", h5_precision_, dsid2, H5P_DEFAULT, cparms, H5P_DEFAULT);
        grp_[i].dtmap.insert(h5pair("timeseries", grp_[i].ttid));
        // create geo
        chunk_dims[1] = CHUNK_DIM_2;
        H5Pset_chunk(cparms, 2, chunk_dims); 
        grp_[i].geid = H5Dcreate(gid_[i], "geo", h5_precision_, dsid2, H5P_DEFAULT, cparms, H5P_DEFAULT);
        grp_[i].dtmap.insert(h5pair("geo", grp_[i].geid));
        // create ntime
        grp_[i].ntid = H5Dcreate(gid_[i], "ntime", H5T_STD_I64LE, dsid1, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        grp_[i].dtmap.insert(h5pair("ntime", grp_[i].ntid));
        // create npar
        grp_[i].npid = H5Dcreate(gid_[i], "nparticle", H5T_STD_I64LE, dsid1, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        grp_[i].dtmap.insert(h5pair("npar", grp_[i].npid));
        // create boundary
        grp_[i].bbid = H5Dcreate(gid_[i], "boxsize", h5_precision_, dsid3, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        grp_[i].dtmap.insert(h5pair("boxsize", grp_[i].bbid));
                
        int j = 0;
        string bname;
        for(; j < grp_[i].nbin; ++j)
        {
            // now create x y z px py pz
            bname = switch_bin_name(j);
            grp_[i].dtmap.insert(h5pair(bname, H5Dcreate(gid_[i], bname.c_str(), h5_precision_, dsid2, H5P_DEFAULT, cparms, H5P_DEFAULT)));
        }
		//cout << grp_[i].dtmap.size() << endl;
    }
    H5Sclose(dsid1);
    H5Sclose(msid1);
    H5Sclose(dsid2);
    H5Sclose(dsid3);  
    H5Pclose(cparms);  
}

herr_t MD_HDF5::FopenPtr(hid_t loc_id, const char* name, const struct H5L_info_t* statbuf, void* opdata)
{
	return static_cast<MD_HDF5*>(opdata)->md_h5_fopen(loc_id, name, statbuf);
}

void MD_HDF5::OpenFile(string fname, int flag)
{
    if(fid_ != 0)
    {
        cout << "Already opened a file. Please close it first!" << endl;
		H5Fclose(fid_);
    }
    
    fid_ = H5Fopen(fname.c_str(), flag, H5P_DEFAULT);
    
    // then interate over all groups and datasets in the groups;
    // this needs some rework at times in case we have subgroups in the data
    gid_.resize(0);
    grp_.resize(0);
    //herr_t H5Lvisit( hid_t group_id, H5_index_t index_type, H5_iter_order_t order, H5L_iterate_t op, void *op_data ) 
    H5Lvisit(fid_, H5_INDEX_NAME, H5_ITER_NATIVE, &MD_HDF5::FopenPtr, (void*)this);
    cout << endl;
    int i;
    for (i = 0; i < int(gid_.size()); ++i)
    {

    	md_h5_grp_aopen(grp_[i], gid_[i]);
        grp_[i].geid = grp_[i].dtmap.find("geo")->second;
        grp_[i].ttid = grp_[i].dtmap.find("timeseries")->second;
        grp_[i].ntid = grp_[i].dtmap.find("ntime")->second;
        H5Dread(grp_[i].ntid, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &(grp_[i].dim_t));
        grp_[i].npid = grp_[i].dtmap.find("nparticle")->second;
        H5Dread(grp_[i].npid, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &(grp_[i].dim_n));
        grp_[i].pxid = grp_[i].dtmap.find("px")->second;
        grp_[i].pyid = grp_[i].dtmap.find("py")->second;
        grp_[i].pzid = grp_[i].dtmap.find("pz")->second;
        grp_[i].rxid = grp_[i].dtmap.find("rx")->second;
        grp_[i].ryid = grp_[i].dtmap.find("ry")->second;
        grp_[i].rzid = grp_[i].dtmap.find("rz")->second;
		grp_[i].bbid = grp_[i].dtmap.find("boxsize")->second;
		grp_[i].nbin = grp_[i].dtmap.size() - 5;
    }
    ngrp_ = gid_.size();
}

void MD_HDF5::CloseFile()
{
    int i = 0;
    for(; i < ngrp_; ++i)
    {
		cout << "Closing /Group"<< i << ":" << endl;
        std::map <string, hid_t>::iterator tmpit;
        for(tmpit = grp_[i].dtmap.begin(); tmpit != grp_[i].dtmap.end(); ++tmpit)
        {
            cout << std::setw(15) << std::left << tmpit->first << ", ";
            cout << std::setw(15) << std::left << tmpit->second << endl;
            H5Dclose(tmpit->second);
        }        
        H5Gclose(gid_[i]);
    }
    cout << endl;
    H5Fclose(fid_);
    gid_.resize(0);
    grp_.resize(0);
    ngrp_ = 0;
    fid_ = 0;
}

 
MD_HDF5_Handle::MD_HDF5_Handle()
{
    //void
    ngrp_ = 0;
    igrp_ = 0;
    for(int i = 0; i < MAX_MD_SEEKVAL; ++i)
    {
        string bname = switch_bin_name(i);
        seekstr_.push_back(bname);
    }
    SetPrecision(single_float);
}

MD_HDF5_Handle::~MD_HDF5_Handle()
{
	
}


void  MD_HDF5_Handle::OpenFile(std::string fname, int flag)
{
	if(flag == h5_rdwr)   MD_HDF5::OpenFile(fname, H5F_ACC_RDWR);
	if(flag == h5_rdonly) MD_HDF5::OpenFile(fname, H5F_ACC_RDONLY);
	if(flag == h5_trunc)  MD_HDF5::OpenFile(fname, H5F_ACC_TRUNC);
	if(flag == h5_excl)   MD_HDF5::OpenFile(fname, H5F_ACC_EXCL);
}

void MD_HDF5_Handle::SetPrecision(_mdrw_precision precision)
{
	if(precision == md_h5_single)
		MD_HDF5::SetPrecision(H5T_IEEE_F32LE);
	if(precision == md_h5_double)
		MD_HDF5::SetPrecision(H5T_IEEE_F64LE);
}

void MD_HDF5_Handle::SetNumberParticles(int n)
{
    grp_[igrp_].dim_n = n;
    hsize_t ndims_[2] = {1, hsize_t(n)};
    H5Dset_extent(grp_[igrp_].geid, ndims_);
}

int MD_HDF5_Handle::ReadNumberParticles()
{
	int npar;
	H5Dread(grp_[igrp_].npid, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &npar);
	return npar;
}

void MD_HDF5_Handle::SetNumberFrames(int n)
{
    grp_[igrp_].dim_t = n;
    hsize_t tdims_[2] = {hsize_t(n), 1};
    H5Dset_extent(grp_[igrp_].ttid, tdims_);
}

void MD_HDF5_Handle::SetGroup(int n)
{
	igrp_ = n;
}

void MD_HDF5_Handle::SetSlice(size_t* offset, size_t* stride, size_t* count, size_t* block)
{
	memcpy(offset_, offset, 2 * sizeof(size_t));
	memcpy(stride_, stride, 2 * sizeof(size_t));
	memcpy(count_,  count,  2 * sizeof(size_t));
	memcpy(block_,  block,  2 * sizeof(size_t));
	size_t d0 = offset_[0] + stride_[0] * (count_[0] - 1) + 1;
    size_t d1 = offset_[1] + stride_[1] * (count_[1] - 1) + 1;
    if(grp_[igrp_].dim_t < d0) grp_[igrp_].dim_t = d0;
    if(grp_[igrp_].dim_n < d1) grp_[igrp_].dim_n = d1;
    
    hsize_t mdims_[2] = {hsize_t(grp_[igrp_].dim_t), hsize_t(grp_[igrp_].dim_n)};
    hsize_t tdims_[2] = {mdims_[0], 1};
    hsize_t ndims_[2] = {1, mdims_[1]};
    //herr_t H5Fget_intent(hid_t file_id, unsigned *intent)
    unsigned intent;
    H5Fget_intent(fid_, &intent);
    if(intent == H5F_ACC_RDWR)
    {
        H5Dset_extent(grp_[igrp_].ttid, tdims_);
        H5Dset_extent(grp_[igrp_].geid, ndims_);
    }
	for(int i = 0; i < MAX_MD_SEEKVAL; ++i)
	{
		string valname = seekstr_[i];
		std::map<string, hid_t>::iterator iter = grp_[igrp_].dtmap.find(valname);
		if(iter == grp_[igrp_].dtmap.end()) continue;
        if(intent == H5F_ACC_RDWR)
        {
		  H5Dset_extent(iter->second, mdims_);
        }
	}
	hid_t stat;
    if(intent == H5F_ACC_RDWR)
    {
	   stat = H5Dwrite(grp_[igrp_].ntid, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &(grp_[igrp_].dim_t));
	   stat = H5Dwrite(grp_[igrp_].npid, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &(grp_[igrp_].dim_n));
    }
}

void MD_HDF5_Handle::WriteData(_mdrw_seekval val, const dmat<FloatType>& buffer)
{
	WriteData(val, buffer.container_->array);
}

void MD_HDF5_Handle::WriteData(_mdrw_seekval val, const vector<FloatType>& buffer)
{
	string valname = seekstr_[val];
	std::map<string, hid_t>::iterator iter = grp_[igrp_].dtmap.find(valname);
	if(iter == grp_[igrp_].dtmap.end()) 
	{	cout << "variable not found!\n"; 
		return;
	}
	hid_t dtid = iter->second;
    hid_t dsid = H5Dget_space(dtid);
    hid_t msid = H5Screate_simple(2, (hsize_t*)count_, NULL);
    
    H5Sselect_hyperslab(dsid, H5S_SELECT_SET, (hsize_t*)offset_, (hsize_t*)stride_, (hsize_t*)count_, (hsize_t*)block_);
    hid_t stat = H5Dwrite(dtid, MD_NATIVE_FLOAT, msid, dsid, H5P_DEFAULT, &(buffer[0]));
    H5Sclose(dsid);
    H5Sclose(msid); 
}

void MD_HDF5_Handle::WriteTime(vector<FloatType>& time)
{
	hsize_t offset[2] = {offset_[0], 0};
	hsize_t stride[2] = {stride_[0], 1};
	hsize_t count[2] =  {count_[0],  1};
	
	hid_t dsid = H5Dget_space(grp_[igrp_].ttid);
    hid_t msid = H5Screate_simple(2, count, NULL);
    H5Sselect_hyperslab(dsid, H5S_SELECT_SET, offset, stride, count, NULL);
    hid_t stat = H5Dwrite(grp_[igrp_].ttid, MD_NATIVE_FLOAT, msid, dsid, H5P_DEFAULT, &(time[0]));
    H5Sclose(dsid);
    H5Sclose(msid); 
}


// only need to be called one time
void MD_HDF5_Handle::ReadGeoR(vector<FloatType>& radius)
{
    hid_t stat = H5Dread(grp_[igrp_].geid, MD_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &(radius[0]));
}

void MD_HDF5_Handle::WriteGeoR(vector<FloatType>& radius)
{
    hid_t stat = H5Dwrite(grp_[igrp_].geid, MD_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &(radius[0]));
}

void MD_HDF5_Handle::WriteBoxD(vector<FloatType>& vec)
{
	hid_t stat = H5Dwrite(grp_[igrp_].bbid, MD_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &(vec[0]));
}

void MD_HDF5_Handle::ReadBoxD(vector<FloatType>& vec)
{
	hid_t stat = H5Dread(grp_[igrp_].bbid, MD_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &(vec[0]));
}



void MD_HDF5_Handle::WriteAttr(vector<FloatType>& vec)
{
	grp_[igrp_].rho = vec[0];
	grp_[igrp_].kn  = vec[1];
	grp_[igrp_].gn  = vec[2];
	grp_[igrp_].kt  = vec[3];
	grp_[igrp_].gt  = vec[4];
	grp_[igrp_].mu  = vec[5];
	hid_t status;
	hid_t loc_id;
	hid_t gid = gid_[igrp_];
	loc_id = H5Aopen(gid, "rho", H5P_DEFAULT);
    status = H5Awrite(loc_id, MD_NATIVE_FLOAT, &(grp_[igrp_].rho));
    H5Aclose(loc_id);
    loc_id = H5Aopen(gid, "kn", H5P_DEFAULT);
    status = H5Awrite(loc_id, MD_NATIVE_FLOAT, &(grp_[igrp_].kn));
    H5Aclose(loc_id);
    loc_id = H5Aopen(gid, "gn", H5P_DEFAULT);
    status = H5Awrite(loc_id, MD_NATIVE_FLOAT, &(grp_[igrp_].gn));
    H5Aclose(loc_id);
    loc_id = H5Aopen(gid, "kt", H5P_DEFAULT);
    status = H5Awrite(loc_id, MD_NATIVE_FLOAT, &(grp_[igrp_].kt));
    H5Aclose(loc_id);
    loc_id = H5Aopen(gid, "gt", H5P_DEFAULT);
    status = H5Awrite(loc_id, MD_NATIVE_FLOAT, &(grp_[igrp_].gt));
    H5Aclose(loc_id);
    loc_id = H5Aopen(gid, "mu", H5P_DEFAULT);
    status = H5Awrite(loc_id, MD_NATIVE_FLOAT, &(grp_[igrp_].mu));
    H5Aclose(loc_id);
}

void MD_HDF5_Handle::ReadParDensity(FloatType& rho)
{
	hid_t loc_id;
	hid_t gid = gid_[igrp_];
    loc_id = H5Aopen(gid, "rho", H5P_DEFAULT);
    herr_t status = H5Aread(loc_id, MD_NATIVE_FLOAT, &rho);
    H5Aclose(loc_id);
}
void MD_HDF5_Handle::ReadPPFriction(FloatType& mu)
{
	hid_t loc_id;
	hid_t gid = gid_[igrp_];
    loc_id = H5Aopen(gid, "mu", H5P_DEFAULT);
    herr_t status = H5Aread(loc_id, MD_NATIVE_FLOAT, &mu);
    H5Aclose(loc_id);
}
void MD_HDF5_Handle::ReadNSpringCoe(FloatType& kn)
{
	hid_t loc_id;
	hid_t gid = gid_[igrp_];
    loc_id = H5Aopen(gid, "kn", H5P_DEFAULT);
    herr_t status = H5Aread(loc_id, MD_NATIVE_FLOAT, &kn);
    H5Aclose(loc_id);
}
void MD_HDF5_Handle::ReadNViscosity(FloatType& gn)
{
	hid_t loc_id;
	hid_t gid = gid_[igrp_];
    loc_id = H5Aopen(gid, "gn", H5P_DEFAULT);
    herr_t status = H5Aread(loc_id, MD_NATIVE_FLOAT, &gn);
    H5Aclose(loc_id);
}


void MD_HDF5_Handle::ReadData(_mdrw_seekval val, dmat<FloatType>& buffer)
{
	buffer.resize(count_[0], count_[1]);
	ReadData(val, buffer.container_->array);
}

void MD_HDF5_Handle::ReadData(_mdrw_seekval val, vector<FloatType>& buffer)
{
	string valname = seekstr_[val];
	std::map<string, hid_t>::iterator iter = grp_[igrp_].dtmap.find(valname);
	if(iter == grp_[igrp_].dtmap.end()) 
	{	cout << "variable not found!\n"; 
		return;
	}
	hid_t dtid = iter->second;
    hid_t dsid = H5Dget_space(dtid);
    hid_t msid = H5Screate_simple(2, (hsize_t*)count_, NULL);
    H5Sselect_hyperslab(dsid, H5S_SELECT_SET, (hsize_t*)offset_, (hsize_t*)stride_, (hsize_t*)count_, (hsize_t*)block_);
	hid_t stat = H5Dread(dtid, MD_NATIVE_FLOAT, msid, dsid, H5P_DEFAULT, &(buffer[0]));
	H5Sclose(dsid);
    H5Sclose(msid); 
}

// void MD_HDF5_Handle::PullData(_mdrw_seekval val, dmat<FloatType>& buffer)
// {
// 	//container size should match the count_[0] * count_[1]
// 	buffer = *seekptr_[val];
// }
// 
// void MD_HDF5_Handle::PushData(_mdrw_seekval val, const dmat<FloatType>& buffer)
// {
// 	*seekptr_[val] = buffer;
// }
// 
// 
// void MD_HDF5_Handle::PullData(_mdrw_seekval val, vector<FloatType>& buffer)
// {
// 	buffer = seekptr_[val]->container_.array;
// }
// 
// void MD_HDF5_Handle::PushData(_mdrw_seekval val, const vector<FloatType>& buffer)
// {
// 	//container size should match the count_[0] * count_[1]
// 	seekptr_[val]->container_.array = buffer;
// }


// 
// 
// // Flags could be H5F_ACC_TRUNC or H5F_ACC_RDONLY
// void MD_HDF5_Handle::OpenFile(string fname, hid_t flag)
// {
//     // first check if fname exist
//     file_.OpenFile(fname, flag);
// }
// 
// 
// void MD_HDF5_Handle::SetSlice(const std::vector <int> &ts , const std::vector <int> &ps)
// {
//     int nt = ts.size();
//     int np = ps.size();
//     InitSlice(nt, np);
//     idt_ = ts;
//     idp_ = ps;
//     npar_ = np;
//     nframe_ = nt;
//     
// }
// // 
// void MD_HDF5_Handle::SetSlice(int np, int offset_np, int nt, int offset_nt)
// {
//     if (npar_ != np || nframe_ != nt)
//     {
//         idp_.resize(np);
//         idt_.resize(nt);
//         InitSlice(nt, np);
//     }
//     int i;
//     for (i = 0; i < idp_.size(); ++i)
//     {
//         idp_[i] = (i + offset_np);
//     }
//     for (i = 0; i < idt_.size(); ++i)
//     {
//         idt_[i] = (i + offset_nt);
//     }    
//     
// }
// // 
// // 
// void MD_HDF5_Handle::InitSlice(int nt, int np)
// {
//     // need to be modified for hdf5 interface
//     int i;
//     for(i = 0; i < 6; ++i)
//     {
//         seekptr_[i]->resize(nt);
//     }
//        
//     dmat::iterator it;
//     for(i = 0; i < 6; ++i)
//     {
//         for(it = seekptr_[i]->begin(); it!= seekptr_[i]->end(); ++it)
//         {
//             it->resize(np);
//         }
//     }
//     
// }
// 
// //old api for different binary file
// void MD_HDF5_Handle::ReadData(std::streamoff offset, std::ios::seekdir direction)
// {
//     // need to be modified for hdf5 interface
//     int nbin = 5;
//     float * tmp = new float [npar_ * nbin];
//     if(direction == std::ios::beg)
//         filehandle_.seekg(offset, std::ios::beg);
//     if(direction == std::ios::cur)
//         filehandle_.seekg(offset, std::ios::cur);
//     if(direction == std::ios::end)
//         filehandle_.seekg(offset, std::ios::end);
//     curpos_ = filehandle_.tellg();
//     cout << "current filehandle position: " << curpos_ / ((npar_ * nbin + 1) * sizeof(float))  << endl;
//     filehandle_.read((char*)tmp, (npar_ * nbin + 1) * sizeof(float));
//     int idx = 0;
//     //cout << tmp[0] << endl;
//     for(;idx < npar_; ++idx)
//     {
//         rx_[0][idx] = tmp[idx * nbin + 1];
//         ry_[0][idx] = tmp[idx * nbin + 2];
//         rz_[0][idx] = tmp[idx * nbin + 3];
//     }
// }


// 
// #define MD_HDF5_BUFFER_TO_VEC(mytype) \
// { \
//         int i = 0; \
//         while(tmpit!=tmped) \
//         { \
//             vector<FloatType>::iterator pit = tmpit->begin(); \
//             for (;pit != tmpit->end(); ++pit) \
//             { \
//                 *pit = mytype##buf_.buffer[i]; \
//                 ++i; \
//             } \
//             ++tmpit; \
//         } \
// }
// 
// 
// void MD_HDF5_Handle::ReadData(seekval tmpval)
// {
//     string bname;
//     dmat::iterator tmpit = seekptr_[tmpval]->begin();
//     dmat::iterator tmped = seekptr_[tmpval]->end();
//     bname = switch_bin_name(tmpval);
//     // need error handling when igrp_ is not initialized;
//     hid_t tmpid = file_.GetDatasetID(igrp_, bname);
//     ResizeBuffer();
//     hid_t datatype = H5Dget_type(tmpid); 
//     hid_t native_type = H5Tget_native_type(datatype, H5T_DIR_ASCEND);
//     void* buffer = NULL;
//     int size;
//     if(H5Tequal(native_type, H5T_NATIVE_INT))
//     {
//         buffer = Ibuf_.buffer;
//     }
//     if(H5Tequal(native_type, H5T_NATIVE_FLOAT))
//     {
//         buffer = Fbuf_.buffer;
//     }
//     if(H5Tequal(native_type, H5T_NATIVE_DOUBLE))
//     {
//         buffer = Dbuf_.buffer;
//     }
//     assert(buffer);
//     file_.ReadData(tmpid, native_type, idt_, idp_, buffer);
//     if (H5Tequal(native_type, H5T_NATIVE_INT))
//     {
//         MD_HDF5_BUFFER_TO_VEC(I);
//     }
//     if (H5Tequal(native_type, H5T_NATIVE_FLOAT))
//     {
//         MD_HDF5_BUFFER_TO_VEC(F);
//     }
//     if (H5Tequal(native_type, H5T_NATIVE_DOUBLE))
//     {
//         MD_HDF5_BUFFER_TO_VEC(D);
//     } 
//     
//     H5Tclose(datatype);
//     H5Tclose(native_type);   
// }
// 
// void MD_HDF5_Handle::ReadData(void)
// {
//     // for hdf5;
//     for(unsigned i = 0; i < 6; ++i)
//     {
//         // the first 6 values are rx-rz, px-pz
//         ReadData(seekval(i));
//     }    
// }
// 
// void MD_HDF5_Handle::SetWritePrecision(_mdrw_precision pre)
// {
//     iprecision_ = pre;
// }
// 
// void MD_HDF5_Handle::WriteData(seekval)
// {
//     
// }
// 
// void MD_HDF5_Handle::ResizeBuffer()
// {
//     int size = idt_.size() * idp_.size();
//     if (Fbuf_.size < size)
//     {
//         delete Fbuf_.buffer;
//         Fbuf_.buffer = new float [size];
//         Fbuf_.size = size;
//     }
//     if (Dbuf_.size < size)
//     {
//         delete Dbuf_.buffer;
//         Dbuf_.buffer = new double [size];
//         Dbuf_.size = size;
//     }
//     if (Ibuf_.size < size)
//     {
//         delete Ibuf_.buffer;
//         Ibuf_.buffer = new int [size];
//         Ibuf_.size = size;
//     }
// }
// 
// void MD_HDF5_Handle::PullData(dmat& data, seekval flag)
// {
//     data = *seekptr_[flag];
// }

#endif

// #ifdef MD_NETCDF_HEADER
// nc_set_log_level(3);
// #define NETCDF_ERR(e) { cout << "Error: " << nc_strerror(e) << endl; exit(2);}
// #define MD_DIM_TIME "d_time"
// #define MD_DIM_INDX "d_indx"
// #define MD_DIM_OTHR "d_othr"
// static size_t dim_np_nt[2];
// static size_t dim_comm = 1;
// 
// MD_netCDF::MD_netCDF(): fid_(0), md_precision_(NC_DOUBLE)
// {
//     // no_op
// }
// 
// void MD_netCDF::SetPrecision(_mdrw_precision val)
// {
//     switch(val)
//     {
//     case md_int:
//         md_precision_ = NC_INT;
//         break;
//     case md_float:
//         md_precision_ = NC_FLOAT;
//         break;
//     case md_double:
//         md_precision_ = NC_DOUBLE;
//         break;
//     default:
//         assert(0);
//     }
// }
// 
// void MD_netCDF::CloseFile()
// {
//     int retval;
//     if(retval = nc_close(fid_)) NETCDF_ERR(retval);
// }
// 
// MD_netCDF::~MD_netCDF()
// {
//     if(fid_) CloseFile();
// }
// 
// 
// void MD_netCDF::CreateFile(string fname, int ngrp, const vector<int>& nbin)
// {  
//     if(fid_) exit(-1); // need more work here
//     if(nbin.size() != ngrp) exit(-1);
//     hid_t status;
//     if(status = nc_create(fname.c_str(), NC_NETCDF4, &fid_)) NETCDF_ERR(status);
//     grp_.resize(ngrp);
//     gmp_.resize(ngrp);
//     int shuffle, deflate, deflate_level;
//     shuffle = NC_SHUFFLE;
//     deflate = 1;
//     deflate_level = 4;
// 
//     for(int i = 0; i < ngrp; ++i)
//     {
//         std::stringstream tmpnum;
//         tmpnum.str("");
//         tmpnum << i;
//         string gname = "Group" + tmpnum.str();
//         hid_t tmp_gid;
//         if (status = nc_def_grp(fid_, gname.c_str(), &tmp_gid)) NETCDF_ERR(status);
//         gmp_[i] = pair<string, hid_t>(gname, tmp_gid);
//         if(status = nc_def_dim(tmp_gid, MD_DIM_TIME, NC_UNLIMITED, &grp_[i].dim_np_nt[0])) NETCDF_ERR(status);
//         if(status = nc_def_dim(tmp_gid, MD_DIM_INDX, NC_UNLIMITED, &grp_[i].dim_np_nt[1])) NETCDF_ERR(status);
//         if(status = nc_def_dim(tmp_gid, MD_DIM_OTHR, 1, &grp_[i].dim_comm)) NETCDF_ERR(status);
//         if(status = nc_def_var(tmp_gid, "nbin", NC_UINT64, 1, &grp_[i].dim_comm, &grp_[i].nbid)) NETCDF_ERR(status);
//         if(status = nc_def_var(tmp_gid, "ntime", NC_UINT64, 1, &grp_[i].dim_comm, &grp_[i].ntid)) NETCDF_ERR(status);
//         if(status = nc_def_var(tmp_gid, "npar", NC_UINT64, 1, &grp_[i].dim_comm, &grp_[i].npid)) NETCDF_ERR(status);
//         if(status = nc_def_var(tmp_gid, "timeseries", md_precision_, 1, &grp_[i].dim_np_nt[0], &grp_[i].ttid)) NETCDF_ERR(status);
//         if(status = nc_def_var(tmp_gid, "geo", md_precision_, 1, &grp_[i].dim_np_nt[1], &grp_[i].geid)) NETCDF_ERR(status);
//         unsigned tmpnbin = nbin[i] > 6? nbin[i]:6;
//         unsigned tmpval = 1000;
//         if ((status = nc_put_var_uint(tmp_gid, grp_[i].nbid, &tmpnbin))) NETCDF_ERR(status);
//         if ((status = nc_put_var_uint(tmp_gid, grp_[i].ntid, &tmpval))) NETCDF_ERR(status);
//         if ((status = nc_put_var_uint(tmp_gid, grp_[i].npid, &tmpval))) NETCDF_ERR(status);
//         //now create all other variable
//         for (int j = 0; j < tmpnbin; ++j)
//         {
//             string bname = switch_bin_name(j);
//             hid_t tmp_varid;
//             if(status = nc_def_var(tmp_gid, bname.c_str(), md_precision_, 2, grp_[i].dim_np_nt, &tmp_varid)) NETCDF_ERR(status);
//             if(status = nc_def_var_chunking(tmp_gid, tmp_varid, NULL, grp_[i].chunks)) NETCDF_ERR(status);
//             if (status = nc_def_var_deflate(tmp_gid, tmp_varid, shuffle, deflate, deflate_level)) NETCDF_ERR(status);
//             MD_netCDF_Var tmpvar;
//             tmpvar.id = tmp_varid;
//             tmpvar.type = md_precision_;
//             tmpvar.ndim = 2;
//             grp_[i].dtmap[bname] =  tmpvar;  
//         }
//         grp_[i].grid = tmp_gid;
//     }
// }
// 
// void MD_netCDF::OpenFile(string fname, int ioflag)
// {
//     hid_t status;
//     if(status = nc_open(fname.c_str(), ioflag, &fid_)) NETCDF_ERR(status);
//     int num_grps;
//     if(status = nc_inq_grps(fid_, &num_grps, NULL)) NETCDF_ERR(status);
//     hid_t* gid = new hid_t[num_grps];
//     grp_.resize(num_grps);
//     gmp_.resize(num_grps);
//     nc_inq_grps(fid_, &num_grps, gid);
//     char* nbuffer = new char[NC_MAX_NAME + 1];
//     for(int i = 0; i < num_grps; ++i)
//     {
//         if (status = nc_inq_grpname(gid[i], nbuffer)) NETCDF_ERR(status);
//         cout << nbuffer << " here!" << endl;
//         gmp_[i] = pair<string, hid_t>(nbuffer, gid[i]);
//         // now processing the contents of each group
//         int ndims, nvars, ngatts, unlimdimid;
//         if(status = nc_inq(gid[i], &ndims, &nvars, &ngatts, &unlimdimid)) NETCDF_ERR(status);
//         cout << ndims << "\t" << nvars << "\t" << ngatts << "\t" << unlimdimid << endl;
//         
//         if (ndims < 3) exit(-1);
//         int tmp_did;
//         size_t dim_length;
//         if(status = nc_inq_dimid(gid[i], MD_DIM_TIME, &tmp_did)) NETCDF_ERR(status);
//         if(status = nc_inq_dimlen(gid[i], tmp_did, &dim_length)) NETCDF_ERR(status);
//         cout << MD_DIM_TIME << ": " << dim_length << endl;
//         if(status = nc_inq_dimid(gid[i], MD_DIM_INDX, &tmp_did)) NETCDF_ERR(status);
//         if(status = nc_inq_dimlen(gid[i], tmp_did, &dim_length)) NETCDF_ERR(status);
//         cout << MD_DIM_INDX << ": " << dim_length << endl;
//         if(status = nc_inq_dimid(gid[i], MD_DIM_OTHR, &tmp_did)) NETCDF_ERR(status);
//         if(status = nc_inq_dimlen(gid[i], tmp_did, &dim_length)) NETCDF_ERR(status);
//         cout << MD_DIM_OTHR << ": " << dim_length << endl;
//         
//         int* var_ids = new int[nvars];
//         if(status = nc_inq_varids(gid[i], &nvars, var_ids)) NETCDF_ERR(status);
//         MD_netCDF_Var tmpvar;
//         for(int j  =  0; j < nvars; ++j)
//         {
//             if(status = nc_inq_varname(gid[i], var_ids[j], nbuffer)) NETCDF_ERR(status);            
//             tmpvar.id = var_ids[j];
//             if(status = nc_inq_vartype(gid[i], var_ids[j], &tmpvar.type)) NETCDF_ERR(status);	                  
//             if(status =  nc_inq_varndims(gid[i], var_ids[j], &tmpvar.ndim)) NETCDF_ERR(status);
//             grp_[i].dtmap[nbuffer] = tmpvar;
//             cout << grp_[i].dtmap[nbuffer].id << "\t" << grp_[i].dtmap[nbuffer].ndim << "\t" << nbuffer << "\n";
//         }
//         // now we want to pop the geid, ntid, ttid, nbid, npid;
//         std::map<string, MD_netCDF_Var>& mymap = grp_[i].dtmap;
//         grp_[i].geid = mymap.find("geo")->second.id;
//         grp_[i].ntid = mymap.find("ntime")->second.id;
//         grp_[i].nbid = mymap.find("nbin")->second.id;
//         grp_[i].ttid = mymap.find("timeseries")->second.id;
//         grp_[i].npid = mymap.find("npar")->second.id;
//         grp_[i].grid = gid[i];
//         delete var_ids;
//     }
//     cout << endl;
//     delete gid;
//     delete nbuffer;
// }
// 
// void MD_netCDF_Handle::Init()
// {
//     seekptr_.push_back(&rx_);
//     seekptr_.push_back(&ry_);
//     seekptr_.push_back(&rz_);
//     seekptr_.push_back(&px_);
//     seekptr_.push_back(&py_);
//     seekptr_.push_back(&pz_);
//     for(int i = 6; i < MAX_MD_SEEKVAL; ++i)
//     {
//         seekptr_.push_back(&otherd_);
//     }
//     ntp_ = 0;
//     pgrp_ = NULL;
//     stride_.resize(4);
// }
// 
// MD_netCDF_Handle::MD_netCDF_Handle()
// {
//     //
//     Init();
//     //cout << "MD_netCDF_Handle constructor" << endl;
//     
// }
// 
// MD_netCDF_Handle::MD_netCDF_Handle(_mdrw_precision precision)
// {
//     //
//     Init();
//     SetPrecision(precision);
//     //cout << "MD_netCDF_Handle constructor with precision" << endl;
// }
// 
// MD_netCDF_Handle::~MD_netCDF_Handle()
// {
//     //
// 
// }
// 
// void MD_netCDF_Handle::SetPrecision(_mdrw_precision precision)
// {
//     MD_netCDF::SetPrecision(precision);
// }
// 
// 
// void MD_netCDF_Handle::InitSlice()
// {
//     ntp_ = idxelm_.size();
//     int i;
//     otherd_.resize(ntp_);
//     for(i = 0; i < 6; ++i)
//     {
//         seekptr_[i]->resize(ntp_);
//     }
// //    int i;
// //    nt_ = nt;
// //    np_ = np;
// //    for(i = 0; i < 6; ++i)
// //    {
// //        seekptr_[i]->resize(nt);
// //    }
// //       
// //    dmat::iterator it;
// //    for(i = 0; i < 6; ++i)
// //    {
// //        for(it = seekptr_[i]->begin(); it!= seekptr_[i]->end(); ++it)
// //        {
// //            it->resize(np);
// //        }
// //    }
// }
// 
// void MD_netCDF_Handle::SetSlice(const std::vector<partrajdim>& elements)
// {
// //    int nt = ts.size();
// //    int np = ps.size();
// //    InitSlice(nt, np);
// //    idt_ = ts;
// //    idp_ = ps;
//     idxelm_ = elements;
//     reading_stat_ = 1;
//     InitSlice();
// }
// 
// void MD_netCDF_Handle::SetSlice(const partrajdim& start, const partrajdim& count, const partrajdim& stride)
// {
//     stride_[0] = start;
//     stride_[1] = count;
//     stride_[2] = stride;
//     idxelm_.resize(count[0] * count[1]);
//     for(int i = 0; i < count[0]; ++i)
//     {
//         for(int j = 0; j < count[1]; ++j)
//         {
//             partrajdim index(2);
//             index[0] = start[0] + i * stride[1];
//             index[1] = start[0] + i * stride[1];
//             idxelm_[j + i * count[1]] = index;
//         }
//     }
//     reading_stat_ = 0;
//     InitSlice();
// //    if (np_ != np || nt_ != nt)
// //    {
// //        idp_.resize(np);
// //        idt_.resize(nt);
// //        InitSlice(nt, np);
// //    }
// //    int i;
// //    for (i = 0; i < idp_.size(); ++i)
// //    {
// //        idp_[i] = (i + offset_np);
// //    }
// //    for (i = 0; i < idt_.size(); ++i)
// //    {
// //        idt_[i] = (i + offset_nt);
// //    }
// }
// 
// int MD_netCDF_Handle::NumberofGroups()
// {
//     return grp_.size();
// }
// 
// void MD_netCDF_Handle::ListofGroups(vector<string>& list)
// {
//     list.resize(grp_.size());
//     for(int i = 0; i < grp_.size(); ++i)
//     {
//         list[i] = gmp_[i].first;
//     }
// }
// 
// 
// void MD_netCDF_Handle::SetReadingGroup(string gname)
// {
//     for(int i = 0; i < grp_.size(); ++i)
//     {
//         if(gmp_[i].first == gname)
//         {
//             pgrp_ = &grp_[i];
//             break;
//         }
//     }
// }
// 
// void MD_netCDF_Handle::SetReadingGroup(int gid)
// {
//     for(int i = 0; i < grp_.size(); ++i)
//     {
//         if(gmp_[i].second == gid)
//         {
//             pgrp_ = &grp_[i];
//             break;
//         }
//     }
// }
// 
// void MD_netCDF_Handle::ReadData()
// {
//     for(int i = 0; i < 6; ++i)
//         ReadData(seekval(i));
// }
// 
// 
// 
// void MD_netCDF_Handle::ReadData(seekval val)
// {
//     if(pgrp_ == NULL)
//     {
//         cout << "warning: reading group not set" << endl;
//         assert(0);
//     }
//     string valname = switch_bin_name(val);
//     MD_netCDF_Var tmpvar;
//     tmpvar = pgrp_->dtmap[valname];
//     assert(tmpvar.ndim == 2);
//     hid_t status;
//     if (reading_stat_ == 0)
//     {
//         size_t start[] = {stride_[0][0], stride_[0][1]};
//         size_t count[] = {stride_[1][0], stride_[1][1]};
//         ptrdiff_t stride[] = {stride_[2][0], stride_[2][1]};
//         if (status = nc_get_vars_double(pgrp_->grid, tmpvar.id, start, count, stride, &(*seekptr_[val])[0])) NETCDF_ERR(status);
//     }
//     // no need for type check, as we have done this before
//     
// }
// 
// void MD_netCDF_Handle::WriteData()
// {
//     for(int i = 0; i < 6; ++i)
//         WriteData(seekval(i));
// }
// 
// void MD_netCDF_Handle::WriteData(seekval val)
// {
//     if(pgrp_ == NULL)
//     {
//         cout << "warning: reading group not set" << endl;
//         assert(0);
//     }
//     string valname = switch_bin_name(val);
//     MD_netCDF_Var tmpvar;
//     tmpvar = pgrp_->dtmap[valname];
//     assert(tmpvar.ndim == 2);
//     hid_t status;
//     // see which dimension is larger and read along that dimension
//     if (reading_stat_ == 0) 
//     {
//         //cout << "buffer is:\n" << *seekptr_[val] << endl;
//         
//         size_t start[] = {stride_[0][0], stride_[0][1]};
//         size_t count[] = {stride_[1][0], stride_[1][1]};
//         ptrdiff_t stride[] = {stride_[2][0], stride_[2][1]};
//         if (status = nc_put_vars_double(pgrp_->grid, tmpvar.id, start, count, stride, &(*seekptr_[val])[0])) NETCDF_ERR(status);
//     }       
// }
// 
// 
// 
// void MD_netCDF_Handle::GetInfo()
// {
// 
// }
// 
// void MD_netCDF_Handle::SyncBuffer()
// {
//     nc_sync(fid_);	
// }
// 
// 
// void MD_netCDF_Handle::PushData(const dmat& tmp, seekval value)
// {
//     *seekptr_[value] = tmp;
// }
// 
// void MD_netCDF_Handle::PullData(dmat& tmp, seekval value)
// {
//     tmp = *seekptr_[value];
// }

//#endif
