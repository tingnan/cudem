#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <vector>
#include <map>
#include "md_header.h"
#ifndef _MD_HDF_HEADER_
#define _MD_HDF_HEADER_

template<typename T>
class container2d
{
public:
	int idx_;
	int xsize_;  // 1st dimension size
	int ysize_;  // 2nd dimension size
	std::vector<T> array;
	T& operator [] (int);
	const T& operator [] (int) const;
	void resize(int, int);
};

template<typename T>
void container2d<T>::resize(int nx, int ny)
{
	xsize_ = nx;
	ysize_ = ny;
	(array).resize(nx * ny);
}

template<typename T>
const T& container2d<T>::operator [] (int j) const
{
	return (array)[idx_ * ysize_ + j];
}

template<typename T>
T& container2d<T>::operator [] (int j)
{
	return (array)[idx_ * ysize_ + j];
}

template<typename T>
class dmat
{
public:
	container2d<T>* container_;
	dmat();
	void cpymem(std::vector<T>&);
	void resize(int, int);
	container2d<T>& operator [] (int);
	const container2d<T>& operator [] (int) const;
    friend std::ostream& operator<<(std::ostream& os, const dmat& mat)
    {
        for(int i = 0; i < mat.container_->xsize_; ++i)
        {
        	for(int j = 0; j < mat.container_->ysize_; ++j)
        	{
        		os << std::setprecision(5) << std::setw(10) << mat[i][j];
        	}
        	os << std::endl;
        }
        return os;
    }
};

template<typename T>
dmat<T>::dmat()
{
	container_ = new container2d<T>(); 
}

template<typename T>
void dmat<T>::resize(int nx, int ny)
{
	container_->resize(nx, ny);
}

template<typename T>
void dmat<T>::cpymem(std::vector<T>& buffer)
{
	buffer = container_->array;
}

template<typename T>
container2d<T>& dmat<T>::operator [] (int i)
{
	container_->idx_ = i;
	return (*container_);
}

template<typename T>
const container2d<T>& dmat<T>::operator [] (int i) const
{
	container_->idx_ = i;
	return (*container_);
}


#define MAX_MD_SEEKVAL 12
// the following 15 value
enum _mdrw_seekval
{
    md_hdf5_posx = 0,
    md_hdf5_posy,
    md_hdf5_posz,
    md_hdf5_velx,
    md_hdf5_vely,
    md_hdf5_velz,
    md_hdf5_omex,
    md_hdf5_omey,
    md_hdf5_omez,
    md_hdf5_focx,
    md_hdf5_focy,
    md_hdf5_focz
};

template < class T >
std::ostream& operator << (std::ostream& os, const std::vector<std::vector<T> >& v) 
{
    os << std::right;
    os << std::fixed;
    os << std::setprecision(3);
    for (typename std::vector<std::vector<T> >::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        for (typename std::vector<T>::const_iterator jj = ii->begin(); jj != ii->end(); ++jj)
        {
            os << std::setw(10) << *jj;
        }
        os << std::endl;
    }
    return os;
}

template < class T >
std::ostream& operator << (std::ostream& os, const std::vector<T>& v) 
{
    os << std::right;
    os << std::fixed;
    os << std::setprecision(3);
    for (typename std::vector<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {

       os << std::setw(10) << *ii;
        
    }
    os << std::endl;
    return os;
}

#ifndef USE_HDF5
#define USE_HDF5
#endif
#ifdef  USE_HDF5

enum _mdrw_precision
{
    md_h5_single,
    md_h5_double
};

// wrapper for hdf5 
// 1 file, multiple groups, multiple dataset(each dataset has a dataspace), 1 memspace for copy buffer
// MD_HDF5_GRP is a single group structure 
// need to also include sand property as group attribute
struct MD_HDF5_GRP
{
   std::map<std::string, int> dtmap;
   // dataset ids for geometry, ntime, time_series, nbin, num_particle 
   int geid, ntid, ttid, npid;
   // dataset ids for boundary and world origin
   int bbid;
   // those are just alias to the variables stored in the dtmap, 
   int rxid, ryid, rzid, pxid, pyid, pzid;
   int nbin;
   // dimensions of datasets in the group
   int dim_t, dim_n;
   // other attributes
   FloatType rho, cellsize, kn, gn, kt, gt, mu;
   MD_HDF5_GRP()
   {
   		nbin  = 0;
   		dim_t = 0;
   		dim_n = 0;
   }
};

// struct md_hdf5_finfodata
// {
//     std::vector<int> gid;
// };

class MD_HDF5
{
protected:
	int h5_precision_;
    int fid_;
    std::vector<int> gid_;
    std::vector<MD_HDF5_GRP> grp_; 
    std::vector<std::string> gnm_;
    unsigned ngrp_;
    int md_h5_fopen(int, const char*, const struct H5L_info_t*);
    int md_h5_grp_aopen(MD_HDF5_GRP&, int);
public:
	static int FopenPtr(int, const char*, const struct H5L_info_t*, void*);
    MD_HDF5();
    void SetPrecision(int);
    void CreateFile(std::string fname, unsigned ngrp, const std::vector<unsigned>& nbin);
    void OpenFile(std::string, int);
    void CloseFile();
    ~MD_HDF5();
};

class MD_HDF5_Handle: public MD_HDF5
{
// typedef std::vector <std::vector <FloatType> > dmat;
protected:
    int ngrp_;
    int igrp_; // reading&writing group index
    size_t offset_[2];
    size_t stride_[2];
    size_t count_[2];
    size_t block_[2];
    std::vector<std::string> seekstr_;
public:
    static const _mdrw_seekval rx = md_hdf5_posx;
    static const _mdrw_seekval ry = md_hdf5_posy;
    static const _mdrw_seekval rz = md_hdf5_posz;
    static const _mdrw_seekval px = md_hdf5_velx;
    static const _mdrw_seekval py = md_hdf5_vely;
    static const _mdrw_seekval pz = md_hdf5_velz;
    static const _mdrw_seekval wx = md_hdf5_omex;
    static const _mdrw_seekval wy = md_hdf5_omey;
    static const _mdrw_seekval wz = md_hdf5_omez;
    static const _mdrw_seekval fx = md_hdf5_focx;
    static const _mdrw_seekval fy = md_hdf5_focy;
    static const _mdrw_seekval fz = md_hdf5_focz;
    static const _mdrw_precision single_float = md_h5_single;
    static const _mdrw_precision double_float = md_h5_double;
	static const int h5_rdwr = 0;
	static const int h5_rdonly = 1;
	static const int h5_trunc = 2;
	static const int h5_excl = 3;
	
	
	MD_HDF5_Handle();
	~MD_HDF5_Handle();
	void OpenFile(std::string, int);
	void SetPrecision(_mdrw_precision);
    void SetGroup(int);
	int ReadNumberParticles();
    void SetNumberParticles(int);
    void SetNumberFrames(int);
    void SetSlice(size_t*, size_t*, size_t*, size_t*); // start stride count
    // the read and write api could only be called after the SetSlice command is called
    // read & write from hdf5 file.
    void ReadData(_mdrw_seekval, dmat<FloatType>&);                
    void ReadData(_mdrw_seekval, std::vector<FloatType>&);         // read specified variable
    void WriteData(_mdrw_seekval, const dmat<FloatType>&);          
    void WriteData(_mdrw_seekval, const std::vector<FloatType>&);  //write specific variable
    void WriteTime(std::vector<FloatType>&);
	void ReadGeoR(std::vector<FloatType>&);
    void WriteGeoR(std::vector<FloatType>&);
	void ReadBoxD(std::vector<FloatType>&);
    void WriteBoxD(std::vector<FloatType>&);
    void WriteAttr(std::vector<FloatType>&);
	void ReadParDensity(FloatType&);
	void ReadPPFriction(FloatType&);
	void ReadNSpringCoe(FloatType&);
	void ReadNViscosity(FloatType&);
};


// currently use vector<vector> matrix to store all particle information at the designated time points. 
// might have out-of-memory issue as matrix size grows.
// need to think about hdf5 memory management. 
// need to incorporate those reading/writing interfaces to main DEM code.  
// class MD_HDF5_Handle
// {
//     typedef _mdrw_seekval seekval;
//     typedef std::vector <std::vector <FloatType> > dmat;
// private:
//     struct
//     {
//         int* buffer;
//         int size;
//     }Ibuf_;
//     struct
//     {
//         float* buffer;
//         int size;
//     }Fbuf_;
//     struct
//     {
//         double* buffer;
//         int size;
//     }Dbuf_;
//     int npar_;
//     int nframe_;
//     int npar_max_;
//     int nframe_max_;
//     int igrp_;
//     int iprecision_;
//     std::ifstream filehandle_;
//     std::streampos curpos_;
//     MD_HDF5 file_;
//     std::vector <int> idt_; // time count series
//     std::vector <int> idp_; // particle number series
//     std::vector<dmat*> seekpointer_;
//     dmat rx_;
//     dmat ry_;
//     dmat rz_;
//     dmat px_;
//     dmat py_;
//     dmat pz_;
//     dmat otherd_;
//     std::map<seekval, std::string> seekmap_;
//     void InitSlice(int nt, int np);
//     void ResizeBuffer();
//     void ReadSingle();
//     void ReadDouble();
// public:
//     
//     MD_HDF5_Handle();
//     void SetWritePrecision(_mdrw_precision);
//     void SetGroup(unsigned);
//     void SetNumberParticles(int);
//     void SetNumberFrames(int);
//     void OpenFile(std::string, int);
//     void CreateFile(std::string);
//     // the first int vector gives particle index array, the second gives time index array
//     void SetSlice(int, int, int, int);
//     void SetSlice(const std::vector <int>& , const std::vector <int>&);
//     // API for backward compatibility, will be deprecated soon
//     void ReadData(std::streamoff, std::ios::seekdir);
//     // the read and write api could only be called after the SetSlice command is called
//     // read & write from hdf5 file.
//     void ReadData();          // pop all the rx-rz, px-pz
//     void ReadData(seekval);   // read specified variable
//     void WriteData();         //write all the rx-rz, px-pz 
//     void WriteData(seekval);  //write specific variable
//     // interface to extract and retrieve data.
//     void PullData(dmat&, seekval);
//     void PushData(const dmat&, seekval);
//     
//     static const seekval rx = md_hdf5_posx;
//     static const seekval ry = md_hdf5_posy;
//     static const seekval rz = md_hdf5_posz;
//     static const seekval px = md_hdf5_velx;
//     static const seekval py = md_hdf5_vely;
//     static const seekval pz = md_hdf5_velz;
//     static const seekval fx = md_hdf5_focx;
//     static const seekval fy = md_hdf5_focy;
//     static const seekval fz = md_hdf5_focz;
//     
//     static const _mdrw_precision single_float = md_h5_single;
//     static const _mdrw_precision double_float = md_h5_double;
//     
//     friend std::ostream& operator<<(std::ostream& os, const MD_HDF5_Handle& handle)
//     {
//         std::string bname;
//         for(int k = 0; k < 6; ++k)
//         {
//             bname = handle.seekmap_.find(seekval(k))->second;
//             os << bname+":" << std::endl;
//             os << *handle.seekpointer_[k];
//             os << std::endl;
//         }
//         return os;
//     }
// 
// };

#endif


//#define MD_NETCDF_HEADER
#ifdef  MD_NETCDF_HEADER

typedef int int;

struct MD_netCDF_Var
{
    int id;
    nc_type type;
    int ndim;
    std::vector<int> dim; 
};

struct MD_netCDF_GRP
{
    std::map<std::string, MD_netCDF_Var> dtmap;
    int grid;
    int geid, ntid, ttid, nbid, npid;
    int dim_np_nt[2];
    int dim_comm;
    size_t chunks[2];
    MD_netCDF_GRP()
    {
        chunks[0] = 10;
        chunks[1] = 1e3;
    };
};

enum _mdrw_precision
{
    md_int = 0,
    md_float,
    md_double
};


class MD_netCDF
{
protected:
    typedef _mdrw_precision _mdrw_precision;
    int fid_;
    int md_precision_;
    std::vector<std::pair<std::string, int> > gmp_;
    std::vector<MD_netCDF_GRP> grp_;
    MD_netCDF();
    void SetPrecision(_mdrw_precision);
    ~MD_netCDF();
public:
    void CreateFile(std::string fname, int ngrp, const std::vector<int>& nbin);
    void OpenFile(std::string, int);
    void CloseFile();
    static const _mdrw_precision integer = md_int;
    static const _mdrw_precision single_float = md_float;
    static const _mdrw_precision double_float = md_double;
};



class MD_netCDF_Handle: public MD_netCDF
{
    typedef std::vector<int> partrajdim;
protected:
    typedef std::vector<FloatType> dmat;
    typedef _mdrw_seekval seekval;
    int reading_stat_; //if we are reading individual elements all a stride array;
    std::vector<partrajdim> idxelm_;
    std::vector<partrajdim> stride_; // time count series
    std::vector<dmat*> seekptr_;
    dmat rx_;
    dmat ry_;
    dmat rz_;
    dmat px_;
    dmat py_;
    dmat pz_;
    dmat otherd_;
    int ntp_;
    MD_netCDF_GRP* pgrp_;
    void InitSlice();
    void Init();
public:
    MD_netCDF_Handle();
    MD_netCDF_Handle(_mdrw_precision);
    ~MD_netCDF_Handle();
    void SetPrecision(_mdrw_precision);
    //group api
    int NumberofGroups();
    void ListofGroups(std::vector<std::string>&);
    void SetReadingGroup(int);
    void SetReadingGroup(std::string);
    //set a hypserslab to read
    void SetSlice(const std::vector<partrajdim>&);
    void SetSlice(const partrajdim&, const partrajdim&, const partrajdim&);
    void ReadData();
    void ReadData(seekval); 
    void WriteData();
    void WriteData(seekval);
    // interface to extract and retrieve data.
    void PullData(dmat&, seekval);
    void PushData(const dmat&, seekval);
    //
    void GetInfo(); // print infomation of the file
    void SyncBuffer();
    static const seekval rx = md_hdf5_posx;
    static const seekval ry = md_hdf5_posy;
    static const seekval rz = md_hdf5_posz;
    static const seekval px = md_hdf5_velx;
    static const seekval py = md_hdf5_vely;
    static const seekval pz = md_hdf5_velz;
    static const seekval fx = md_hdf5_focx;
    static const seekval fy = md_hdf5_focy;
    static const seekval fz = md_hdf5_focz;
};

#endif

#endif
