/* 
 * File:   MDUtility.h
 * Author: tingnan
 *
 * Created on February 8, 2011, 10:24 PM
 */

#include "md_header.h"
#include "md_vec.h"
#include <string>
#include <sstream>
#include <cmath>
#include <iostream>

#ifndef _MD_UTILITY_H_
#define	_MD_UTILITY_H_

std::string ftoa (FloatType);
std::string int2str (int);

inline FloatType heaviside (FloatType number)
{
    if (number >= 0)
        return 1.0;
    else
        return 0.0;

}

#ifndef __ENABLE_CUDA__
inline FloatType min (FloatType a, FloatType b)
{
	return a < b? a : b;
}

#endif

int sign (FloatType);

#endif /* MDUTILITY_H */
