/* 
 * File:   MDVec.cc
 * Author: tingnan
 * 
 * Created on February 8, 2011, 8:30 PM
 */

#ifndef  _MD_VEC_CUH_
#define  _MD_VEC_CUH_
#include "md_header.h"
#include "vector_types.h"
#include "helper_math.h"

#if USE_SINGLE
typedef float3 vec3;
typedef float4 vec4;
#endif

#if USE_DOUBLE
typedef double3 vec3;
typedef double4 vec4;
#endif

static __constant__ FloatType dc_math_pi = 3.1415926535897932384626433832795;
static const FloatType hc_math_pi = 3.1415926535897932384626433832795;


static __inline__ __host__ __device__ vec3 make_vec3(FloatType x, FloatType y, FloatType z)
{
	vec3 t; t.x = x; t.y = y; t.z = z; return t;
}

static __inline__ __host__ __device__ vec4 make_vec4(FloatType x, FloatType y, FloatType z, FloatType w)
{
	vec4 t; t.x = x; t.y = y; t.z = z; t.w = w; return t;
}

inline __host__ __device__ float3 operator*(float3 a, int3 b)
{
    return make_float3(a.x * b.x, a.y * b.y, a.z * b.z);
}

inline __host__ __device__ bool equal(int3 a, int3 b)
{
	return a.x == b.x && a.y == b.y && a.z == b.z;
}

inline __host__ __device__ double3 cross(double3 a, double3 b)
{
    return make_double3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
}


inline __host__ __device__ void operator+=(double3& a, double b)
{
    a.x += b;
    a.y += b;
    a.z += b;
}

inline __host__ __device__ void operator+=(double4& a, double4 b)
{
    a.x += b.x;
    a.y += b.y;
    a.z += b.z;
    a.w += b.w;
}

inline __host__ __device__ void operator-=(double4& a, double4 b)
{
    a.x -= b.x;
    a.y -= b.y;
    a.z -= b.z;
    a.w -= b.w;
}


inline __host__ __device__ void operator+=(double b, double3& a)
{
    a.x += b;
    a.y += b;
    a.z += b;
}

inline __host__ __device__ double4 operator/(double4 a, double4 b)
{
	return make_double4(a.x/b.x, a.y/b.y, a.z/b.z, a.w/b.w);
}

inline __host__ __device__ double4 operator*(double4 a, double4 b)
{
	return make_double4(a.x*b.x, a.y*b.y, a.z*b.z, a.w*b.w);
}

inline __host__ __device__ double4 operator/(double4 a, double b)
{
	return make_double4(a.x/b, a.y/b, a.z/b, a.w/b);
}

inline __host__ __device__ double4 operator*(double4 a, double b)
{
	return make_double4(a.x*b, a.y*b, a.z*b, a.w*b);
}

inline __host__ __device__ double4 operator+(double4 a, double4 b)
{
	return make_double4(a.x+b.x, a.y+b.y, a.z+b.z, a.w+b.w);
}

inline __host__ __device__ float3 operator-(float3 a)
{
    return make_float3(-a.x, -a.y, -a.z);
}


inline __host__ __device__ double3 operator-(double3 a)
{
    return make_double3(-a.x, -a.y, -a.z);
}

inline __host__ __device__ double3 operator-(double3 a, double3 b)
{
    return make_double3(a.x - b.x, a.y - b.y, a.z - b.z);
}

inline __host__ __device__ double3 operator-(double3 a, double b)
{
    return make_double3(a.x - b, a.y - b, a.z - b);
}

inline __host__ __device__ double3 operator+(double3 a, double3 b)
{
    return make_double3(a.x + b.x, a.y + b.y, a.z + b.z);
}

inline __host__ __device__ double3 operator+(double3 a, int3 b)
{
    return make_double3(a.x + b.x, a.y + b.y, a.z + b.z);
}


inline __host__ __device__ double3 operator/(double3 a, double3 b)
{
    return make_double3(a.x / b.x, a.y / b.y, a.z / b.z);
}

inline __host__ __device__ double3 operator/(double3 a, double b)
{
    return make_double3(a.x / b, a.y / b, a.z / b);
}


inline __host__ __device__ double3 operator*(double3 a, double3 b)
{
    return make_double3(a.x * b.x, a.y * b.y, a.z * b.z);
}

inline __host__ __device__ double3 operator*(double3 a, double b)
{
    return make_double3(a.x * b, a.y * b, a.z * b);
}

inline __host__ __device__ double3 operator*(double b, double3 a)
{
    return make_double3(a.x * b, a.y * b, a.z * b);
}

inline __host__ __device__ double3 operator*(double3 a, int3 b)
{
    return make_double3(a.x * b.x, a.y * b.y, a.z * b.z);
}

inline __host__ __device__ void operator+=(double3& a, double3 b)
{
    a.x += b.x;
    a.y += b.y;
    a.z += b.z;
}

inline __host__ __device__ double dot(double3 a, double3 b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline __host__ __device__ double length(double3 v)
{
    return sqrt(dot(v, v));
}


static __host__ __device__ vec3 dot(mat3 R, vec3 V)
{   
    FloatType xx = R[0][0] * V.x + R[0][1] * V.y + R[0][2] * V.z;
    FloatType yy = R[1][0] * V.x + R[1][1] * V.y + R[1][2] * V.z;
    FloatType zz = R[2][0] * V.x + R[2][1] * V.y + R[2][2] * V.z;
    return make_vec3(xx, yy, zz);
}

static __host__ __device__ vec3 rot_axis(vec3 axis, vec3 vec, FloatType theta)
{
	vec3 result = make_vec3(0.0, 0.0, 0.0);
    mat3 rotation;
    rotation[0][0] = cos(theta) + axis.x * axis.x * (1 - cos(theta));
    rotation[0][1] = axis.x * axis.y * (1 - cos(theta)) - axis.z * sin(theta);
    rotation[0][2] = axis.x * axis.z * (1 - cos(theta)) + axis.y * sin(theta);
    rotation[1][0] = axis.y * axis.x * (1 - cos(theta)) + axis.z * sin(theta);
    rotation[1][1] = cos(theta) + axis.y * axis.y * (1 - cos(theta));
    rotation[1][2] = axis.y * axis.z * (1 - cos(theta)) - axis.x * sin(theta);
    rotation[2][0] = axis.z * axis.x * (1 - cos(theta)) - axis.y * sin(theta);
    rotation[2][1] = axis.z * axis.y * (1 - cos(theta)) + axis.x * sin(theta);
    rotation[2][2] = cos(theta) + axis.z * axis.z * (1 - cos(theta));
    result = dot(rotation, vec);
	return result;
}


#include <iostream>
inline std::ostream& operator << (std::ostream& os, const int3& vec)
{
    os << "(" << vec.x << "," << vec.y << "," << vec.z << ")";
    return os;
}

inline std::ostream& operator << (std::ostream& os, const double3& vec)
{
    os << "(" << vec.x << "," << vec.y << "," << vec.z << ")";
    return os;
}

inline std::ostream& operator << (std::ostream& os, const float3& vec)
{
    os << "(" << vec.x << "," << vec.y << "," << vec.z << ")";
    return os;
}

inline double rsquare (double3 r)
{
    return r.x * r.x + r.z * r.z + r.y * r.y;
}

inline float rsquare (float3 r)
{
    return r.x * r.x + r.z * r.z + r.y * r.y;
}


#endif
