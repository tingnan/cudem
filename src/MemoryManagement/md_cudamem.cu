/* *
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 */
#include "md_cudamem.cuh"
#include "GeneralHelper/md_cuda_helper.cuh"

void CudaMallocD(void** p_ptr, size_t size)
{
    GPUCheckErrors(cudaMalloc(p_ptr, size));
}

void CudaMallocH(void** p_ptr, size_t size)
{
    GPUCheckErrors(cudaMallocHost(p_ptr, size));
}

void CudaMemHostToDevice(void* dst, void* src, size_t size)
{
    // std::cout << dst << "\t" << src << " : " << size << std::endl;
    GPUCheckErrors(cudaMemcpy(dst, src, size, cudaMemcpyHostToDevice));
    // std::cout << "done copying" << std::endl;
}

void CudaMemDeviceToHost(void* dst, void* src, size_t size)
{
    GPUCheckErrors(cudaMemcpy(dst, src, size, cudaMemcpyDeviceToHost));
}
