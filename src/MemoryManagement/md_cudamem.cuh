/*
 * md_cudamem.cuh
 *
 *  Created on: Jan 22, 2013
 *      Author: tingnan
 */

#ifndef _MD_CUDAMEM_CUH_
#define _MD_CUDAMEM_CUH_

void CudaMallocD(void**, size_t);
void CudaMallocH(void**, size_t);
void CudaMemHostToDevice(void*, void*, size_t);
void CudaMemDeviceToHost(void*, void*, size_t);

template <typename T>
void MD_CudaMalloc(T** d_params, T** h_params)
{
	CudaMallocD((void**)d_params, sizeof(T));
	CudaMallocH((void**)h_params, sizeof(T));;
}

template <typename T>
void MD_CudaMemCpyHostToDevice(T* d_params, T* h_params)
{
	CudaMemHostToDevice(d_params, h_params, sizeof(T));
}

template <typename T>
void MD_CudaMemCpyDeviceToHost(T* h_params, T* d_params)
{
	CudaMemDeviceToHost(h_params, d_params, sizeof(T));
}


#endif /* MD_CUDAMEM_CUH_ */
