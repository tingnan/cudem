#include "physics/ChSystem.h"
#include <sstream>
#include <fstream>
#include <vector>




void CreateRobot(chrono::ChSystem& msys)
{
    std::ifstream inputfile("input.txt");
    if (!inputfile.is_open())
    {
        std::cout << "No input file found. Press Enter to exit.\n";
        std::cin.ignore(std::numeric_limits <std::streamsize> ::max(), '\n');
        exit(0);
    }
    std::vector<std::vector<double> > data;
    data.reserve(1e4);
    ReadCSV(inputfile, data);
    //ChBroadPhaseCallbackNew * mcallback = new ChBroadPhaseCallbackNew;
    //msys.GetCollisionSystem()->SetBroadPhaseCallback(mcallback);
    const double lfact = 1e2;
    const double rhofact = 1e3;
    chrono::ChSharedPtr<ChBodyEasyBox> ground(new ChBodyEasyBox(2000 / lfact, 0.1 / lfact, 2000. / lfact, 1. * rhofact, false, false));
    const double bodyheight = 28. / lfact;
    ground->SetPos(ChVector<>(0, 0, 0.));
    ground->SetBodyFixed(true);
    ground->SetId(-1);
    ground->SetIdentifier(-1);
    msys.AddBody(ground);

    double zshift = 0.01;
    // create a perfactly controlled robot
    {
        ChSharedPtr<ChBodyEasyBox> upperframe(new ChBodyEasyBox(2. / lfact, 2. / lfact, 15. / lfact, 7. * rhofact, false, false));
        msys.AddBody(upperframe);
        upperframe->SetPos(ChVector<>(0, bodyheight, zshift));
        upperframe->SetId(0);
        upperframe->GetPos();        
        
        //upperframe->SetBodyFixed(true);
        ChSharedPtr<ChLinkLockOldham> inplanelink(new ChLinkLockOldham);
        inplanelink->Initialize(ground, upperframe, ChCoordsys<>(ChVector<>()));
        msys.AddLink(inplanelink);

        const double leglen[4] = { 7.57 / lfact, 7.77 / lfact, 7.48 / lfact, 3.40 / lfact };
        const ChQuaternion<> qter = Q_from_AngZ(-CH_C_PI_2);
        std::vector<ChSharedPtr<ChBodyEasyBox> > legcontainer(8);



        for (int j = 0; j < 2; ++j)
        {
            double z_step = -7.5 / lfact;
            if (j == 1)
                z_step = -z_step;
            z_step = z_step + zshift;
            double y_step = bodyheight + leglen[0] / 2.;
            // for each of the joints
            for (int k = 0; k < 4; ++k)
            {

                double x_step = 0. / lfact;
                if (k == 0)
                    y_step = y_step - leglen[k];
                else
                    y_step = y_step - 0.5 * (leglen[k] + leglen[k - 1]);
                double legw = 2.0 / lfact;
                double legh = 2.0 / lfact;
                if (k == 3)
                {
                    legw = 5.0 / lfact;
                }
                legcontainer[j * 4 + k] = ChSharedPtr<ChBodyEasyBox>(new ChBodyEasyBox(leglen[k], legw, legw, 5. * rhofact, false, false));
                legcontainer[j * 4 + k]->SetRot(qter);
                legcontainer[j * 4 + k]->SetPos(ChVector<>(x_step, y_step, z_step));
                legcontainer[j * 4 + k]->SetId(k + 1);
                msys.AddBody(legcontainer[j * 4 + k]);


                ChSharedPtr<ChLinkEngineControl> mylink(new ChLinkEngineControl);
                mylink->Set_mot_torque_limits(3., -3.);
                mylink->Set_mot_gain(50., 0, 0.3);
                if (k == 0)
                    mylink->Initialize(legcontainer[j * 4 + k], upperframe, ChCoordsys<>(ChVector<>(x_step, y_step + leglen[k] / 2.0, z_step)));
                else
                    mylink->Initialize(legcontainer[j * 4 + k], legcontainer[j * 4 + k - 1], ChCoordsys<>(ChVector<>(x_step, y_step + leglen[k] / 2.0, z_step)));
                mylink->Set_eng_mode(ChLinkEngineControl::ENG_MODE_ROTATION);
                ChFunction_Data* funptr = new ChFunction_Data(data);
                funptr->SetColumn(j * 4 + k + 1);
                mylink->Set_rot_funct(funptr);
                msys.AddLink(mylink);
            }
        }
    }
}
