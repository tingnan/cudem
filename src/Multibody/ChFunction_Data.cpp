#include "ChFunction_Data.h"

// first let us define a method to check the if an iterator points to the last object in the stl container.
template <typename Iter>
Iter nextofiter(Iter iter)
{
    return ++iter;
}
template <typename Iter, typename Cont>
bool is_last(Iter iter, const Cont& cont)
{
    return (iter != cont.end()) && (nextofiter(iter) == cont.end());
}

double Interp2pt(double x1, double y1, double x2, double y2, double x)
{
    return (y1 * (x2 - x) + y2 * (x - x1)) / (x2 - x1);
}


// compute the derivative at the point x, using 3 points (x1 < x2 < x3)
// first we find the 2nd order polynormial that passes the 3 points and then
// compute the derivative at point x ,the coefficient is find using the matrxi:
// inv = [-1 / (x1*x2 + x1*x3 - x2*x3 - x1 ^ 2),        -1 / (x1*x2 - x1*x3 + x2*x3 - x2 ^ 2),        1 / (x1*x2 - x1*x3 - x2*x3 + x3 ^ 2);
//        (x2 + x3) / (x1*x2 + x1*x3 - x2*x3 - x1 ^ 2), (x1 + x3) / (x1*x2 - x1*x3 + x2*x3 - x2 ^ 2), -(x1 + x2) / (x1*x2 - x1*x3 - x2*x3 + x3 ^ 2);
//        -(x2*x3) / (x1*x2 + x1*x3 - x2*x3 - x1 ^ 2),  -(x1*x3) / (x1*x2 - x1*x3 + x2*x3 - x2 ^ 2),  (x1*x2) / (x1*x2 - x1*x3 - x2*x3 + x3 ^ 2)]


double DerivPoly2from3pt(double x1, double y1, double x2, double y2, double x3, double y3, double x)
{
    double inv11 = -1 / (x1 * x2 + x1 * x3 - x2 * x3 - x1 * x1);
    double inv12 = -1 / (x1 * x2 - x1 * x3 + x2 * x3 - x2 * x2);
    double inv13 =  1 / (x1 * x2 - x1 * x3 - x2 * x3 + x3 * x3);
    double inv21 = -(x2 + x3) * inv11;
    double inv22 = -(x1 + x3) * inv12;
    double inv23 = -(x1 + x2) * inv13;
    double aa = inv11 * y1 + inv12 * y2 + inv13 * y3;
    double bb = inv21 * y1 + inv22 * y2 + inv23 * y3;
    //std::cout << "a: " << aa << " ";
    //std::cout << "b: " << bb << std::endl;
    return 2 * aa * x + bb;
}

double chrono::ChFunction_Data::Get_y(double x)
{
    if (x < data_.front()[0])
        return data_.front()[column_];
    if (x > data_.back()[0])
        return data_.back()[column_];
    datamat::iterator itr_ = std::lower_bound(data_.begin(), data_.end(), x, _Less());
    double x2 = (*itr_)[0];
    double y2 = (*itr_)[column_];
    double x1 = x2;
    double y1 = y2;
    if (itr_ != data_.begin())
    {
        datamat::iterator tmpit = itr_ - 1;
        x1 = (*tmpit)[0];
        y1 = (*tmpit)[column_];
    }
    // assert(x2 < x1);
    // assert((x1 == x2) && (y1 == y2));
    if (x1 == x2)
        return y2;
    return Interp2pt(x1, y1, x2, y2, x);
    // 
}


// dy/dx is computed from 3points, first we fit a parabola by choosing three points
// adjacent to x, and then compute the derivative of the parabola at x. 
double chrono::ChFunction_Data::Get_y_dx(double x)
{
    
    // three point formula
    double x1, y1, x2, y2, x3, y3;

    if (x <= data_.front()[0])
    {
        datamat::iterator tmpit = data_.begin();
        // initial data set
        x1 = std::min(x, (*tmpit)[0] - 1e-3);
        y1 = (*tmpit)[column_];
        x2 = (*tmpit)[0];
        y2 = (*tmpit)[column_];
        x3 = 2 * x2 - x1;
        y3 = (*tmpit)[column_];
        // if the data set has more than 1 row
        ++tmpit;
        if (tmpit != data_.end())
        {
            x3 = (*tmpit)[0];
            y3 = (*tmpit)[column_];
        }
        return DerivPoly2from3pt(x1, y1, x2, y2, x3, y3, x);
    }
    if (x >= data_.back()[0])
    {
        datamat::reverse_iterator tmpit = data_.rbegin();
        // initial data set
        x3 = std::max(x, (*tmpit)[0] + 1e-3);
        y3 = (*tmpit)[column_];
        x2 = (*tmpit)[0];
        y2 = (*tmpit)[column_];
        x1 = 2 * x2 - x3;
        y1 = (*tmpit)[column_];
        // if the data set has more than 1 row
        ++tmpit;
        if (tmpit != data_.rend())
        {
            x1 = (*tmpit)[0];
            y1 = (*tmpit)[column_];
        }
        //std::cout << "x\tx1\tx2\tx3\n";
        //std::cout << x << "\t" << x1 << "\t" << x2 << "\t" << x3 << "\n";
        return DerivPoly2from3pt(x1, y1, x2, y2, x3, y3, x);

    }

    datamat::iterator itr_ = std::lower_bound(data_.begin(), data_.end(), x, _Less());
    x2 = (*itr_)[0];
    y2 = (*itr_)[column_];

    x1 = x2 - 1e-3;
    y1 = y2;
    // check the boundary first
    if (itr_ != data_.begin())
    {
        datamat::iterator tmpit = itr_ - 1;
        x1 = (*tmpit)[0];
        y1 = (*tmpit)[column_];
    }

    x3 = x2 + 1e-3;
    y3 = x3 > data_.back()[0] ? Interp2pt(x1, y1, x2, y2, x3) : data_.back()[column_];
    datamat::iterator tmpit = itr_ + 1;
    if (tmpit != data_.end())
    {
        x3 = (*tmpit)[0];
        y3 = (*tmpit)[column_];
    }
    return DerivPoly2from3pt(x1, y1, x2, y2, x3, y3, x);
    // 
}