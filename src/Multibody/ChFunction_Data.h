#pragma once
#include "motion_functions/ChFunction_Base.h"
#include <algorithm>
namespace chrono
{


    // a Chfunction class that use multicolumn input data
    // x  y1 y2 y3 y4 ...
    // .. .. .. .. .. ...
    // .. .. .. .. .. ... 
    // the zeroth column always represents x data; and 
    // each of the rest column represent a set of values
    // at the given x. 

    // the rows are presumed to be sorted according to their x values. 
    // the stl binary search algorithms are used to facilitate O(log(n))
    // time complexisty.
    
    // because in chrono time will not 
    class ChFunction_Data : public ChFunction
    {
        typedef std::vector<std::vector<double> > datamat;
        // the order of definition shall not be changed;
        datamat data_;
        unsigned int column_;
        struct _Less
        {
            bool operator()(const std::vector<double>& a, const std::vector<double>& b)
            {
                // we will compare acoording to the first element 
                return a[0] < b[0];
            }
            bool operator()(const std::vector<double>& a, double b)
            {
                return a[0] < b;
            }
            bool operator()(double a, const std::vector<double>& b)
            {
                return a < b[0];
            }

        };
        struct _Equal
        {
            bool operator()(const std::vector<double>& a, const std::vector<double>& b)
            {
                return a[0] == b[0];
            }
        };
        void SortData()
        {
            std::stable_sort(data_.begin(), data_.end(), _Less());
        }
        // test if the adjacent row has the same x;
        bool HasEqual()
        {
            datamat::iterator itr = std::adjacent_find(data_.begin(), data_.end(), _Equal());
            return itr != data_.end();
        }
    public:
        // by default, the array contains only 1 value(0) to avoid any segfault;
        ChFunction_Data() : data_(1, std::vector<double>(1)), column_(0) {}
        ChFunction_Data(const ChFunction_Data& source)
        {
            data_ = source.data_;
            column_ = source.column_;
        }
        ChFunction_Data& operator = (const ChFunction_Data& source)
        {
            data_ = source.data_;
            column_ = source.column_;
            return *this;
        }
        ChFunction_Data(const datamat& buffer) : data_(buffer), column_(0)
        {
            // now perform sort;
            SortData();
            if (HasEqual())
            {
                std::cout << data_[1][0] << std::endl;
                abort();
            }
                
        }
        ~ChFunction_Data() {};
        void Copy(ChFunction_Data* source)
        {
            *this = *source;
        }
        ChFunction* new_Duplicate()
        {
            ChFunction_Data* mfun = new ChFunction_Data(*this);
            return mfun;
        }
        void SetData(const datamat& buffer)
        {
            data_ = buffer;
            SortData();
            if (HasEqual())
                abort();
        }
        void SetColumn(unsigned int column)
        {
            column_ = column;
        }
        double Get_y(double x);
        double Get_y_dx(double x);
    };
}