#ifndef _MD_MULTIBODY_H_
#define _MD_MULTIBODY_H_

/* this is an interface class that defines the behavior of multibody simulator

*/
#include "CollisionShapes/md_shape.h"
#include "md_header.h"

class MultibodySimulatorInterface
{
protected:
	std::vector<MDShape*> node_container_;
	FloatType time_step_;
public:
	MultibodySimulatorInterface() {}
	~MultibodySimulatorInterface() {}
	void SetTimeStep(FloatType t) {time_step_ = t;}
	// step one timestep forward
	virtual void Step() = 0;
	// exchange info with other part of the system
	const std::vector<MDShape*>& GetNodes() { return node_container_;}
	// I deally I do not like the idea of params, will change the behavior through polymorphism, 
	// I have to test polymorphism on CUDA, which will change the way we manage the memory 
	// (say we cannot shallow copy a complicated class)
	virtual void AddBody(MDShape*) = 0;
	virtual void SetTest() {};
	virtual void dump() {};

};

#endif