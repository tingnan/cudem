#include "md_multibody_chrono.h"
#include "md_shape_include.h"
#include "physics/ChSystem.h"
#include <fstream>
#include <sstream>
#include <limits>
#include "ChLinkEngineControl.h"
#include "physics/ChLinkLinActuator.h"
#include "motion_functions/ChFunction_Sine.h"
#include "ChFunction_Data.h"
#include <list>
#include <iomanip>

MultibodySimulatorInterfaceChrono::MultibodySimulatorInterfaceChrono() 
{ 
	system_ = new chrono::ChSystem();
	// do some parameter settings
	system_->Set_G_acc(chrono::ChVector<>(0, -981, 0));
	system_->SetIterLCPmaxItersSpeed(100);
    system_->SetIterLCPmaxItersStab(100);
    system_->SetLcpSolverType(chrono::ChSystem::LCP_ITERATIVE_MINRES);
    system_->SetTol(1e-8);
    system_->SetTolSpeeds(1e-8);
}

MultibodySimulatorInterfaceChrono::~MultibodySimulatorInterfaceChrono()
{
	delete system_;
}

void SetKinematicParamsFromMDToChrono(chrono::ChBody *newbody, MDShape* shape)
{
	// set position velocity orientation and angular

	vec3 bodypos;
	shape->GetNodePos(bodypos);
	newbody->SetPos(chrono::ChVector<>(bodypos.x, bodypos.y, bodypos.z));
	vec3 bodyvel;
	shape->GetNodeVel(bodyvel);
	newbody->SetPos_dt(chrono::ChVector<>(bodyvel.x, bodyvel.y, bodyvel.z));

	vec3 bodyome;
	shape->GetNodeAngVel(bodyome);
	newbody->SetWvel_par(chrono::ChVector<>(bodyome.x, bodyome.y, bodyome.z));

	mat3 bodyorimat;
	shape->GetNodeOriMat(bodyorimat);
	chrono::ChMatrix33<> chbodymat;
	for(int i = 0; i < 3; ++i)
		for(int j = 0; j < 3; ++j)
		{
			chbodymat[i][j] = bodyorimat[i][j];
		}
	newbody->SetRot(chbodymat);

}

void SetKinematicParamsFromChronoToMD(MDShape *shape, chrono::ChBody* newbody)
{
	
	chrono::ChVector<> pos = newbody->GetPos();
	chrono::ChVector<> vel = newbody->GetPos_dt();
	shape->SetNodePos(make_vec3(pos.x, pos.y, pos.z));
	shape->SetNodeVel(make_vec3(vel.x, vel.y, vel.z));

	// compute the rotation and omega
	chrono::ChVector<> X(1., 0., 0.);
	chrono::ChVector<> Y(0., 1., 0.);
	chrono::ChVector<> Z(0., 0., 1.);
	chrono::ChVector<> tmpdir = newbody->TransformPointLocalToParent(X) - pos;
	vec3 xdir = make_vec3(tmpdir.x, tmpdir.y, tmpdir.z);
	
	tmpdir = newbody->TransformPointLocalToParent(Y) - pos;
	vec3 ydir = make_vec3(tmpdir.x, tmpdir.y, tmpdir.z);
	
	tmpdir = newbody->TransformPointLocalToParent(Z) - pos;
	vec3 zdir = make_vec3(tmpdir.x, tmpdir.y, tmpdir.z);

	// set the rotation mat;
	shape->SetNodeOriVec(xdir, ydir, zdir);

	// update the angular velocity
	chrono::ChVector<> ome = newbody->GetWvel_par();
	vec3 omevec = make_vec3(ome.x, ome.y, ome.z);
	shape->SetNodeAngVel(omevec);
}

template<class T>
std::ostream& operator << (std::ostream& os, const chrono::ChVector<T>& vec)
{
    os << vec.x << " " << vec.y << " " << vec.z;
    return os;
}

template<class T>
std::ostream& operator << (std::ostream& os, const chrono::ChQuaternion<T>& vec)
{
    os << vec.e0 << " " << vec.e1 << " " << vec.e2 << " " << vec.e3;
    return os;
}



// dump pos vel acc, rot, ome, and rot_acc for the body
std::ofstream nodinfofile("biped.mov");
void DumpNodInfo(chrono::ChSystem& msys)
{
    nodinfofile << std::setprecision(8);
    nodinfofile << std::scientific;
    const int nnodes = msys.Get_bodylist()->size();
    for (unsigned int i = 0; i < nnodes; ++i)
    {
        chrono::ChBody* curbody = (*msys.Get_bodylist())[i];
        nodinfofile << curbody->GetPos() << " " << curbody->GetPos_dt() << " ";
        nodinfofile << curbody->GetRot() << " " << curbody->GetRot_dt() << "\n";        
    }
}


// dump pos vel acc, rot, ome, and rot_acc for the link
std::ofstream jntinfofile("biped.jnt");
void DumpJntInfo(chrono::ChSystem& msys)
{
    jntinfofile << std::setprecision(8);
    jntinfofile << std::scientific;
    std::list<chrono::ChLink*>::iterator itr;
    for (itr = msys.Get_linklist()->begin(); itr != msys.Get_linklist()->end(); ++itr)
    {
        chrono::ChVector<> localforce = (*itr)->Get_react_force();
        chrono::ChVector<> localtorque = (*itr)->Get_react_torque();
        jntinfofile << localforce << " " << localtorque << " ";
        jntinfofile << (*itr)->GetLinkRelativeCoords().rot.Rotate(localforce) << " " << (*itr)->GetLinkRelativeCoords().rot.Rotate(localtorque) << "\n";
    }
}

void MultibodySimulatorInterfaceChrono::dump()
{
    DumpNodInfo(*system_);
    DumpJntInfo(*system_);
}


void ReadCSV(std::ifstream& inputfile, std::vector<std::vector<double> >& data)
{
    std::string str;
    int i = 0;
    while (std::getline(inputfile, str, '\n'))
    {
        std::istringstream myline(str);
        std::string mynum;
        int j = 0;
        std::vector<double> tmpline(9);
        while (std::getline(myline, mynum, ','))
        {
            tmpline[j] = strtod(mynum.c_str(), NULL);
            ++j;
        }
        data.push_back(tmpline);
        // std::cout << data[i][0] << std::endl;
        ++i;
    }
}

void SetChronoBoxPhysicsProperty(chrono::ChBody *body, double Xsize, double Ysize, double Zsize, double mass)
{
		body->SetDensity(mass / (Xsize * Ysize * Zsize));
        body->SetMass(mass);
        body->SetInertiaXX(chrono::ChVector<> ((1.0/12.0) * mass * ( pow(Ysize, 2) + pow (Zsize, 2) ),
                                          (1.0/12.0) * mass * ( pow(Xsize, 2) + pow (Zsize, 2) ),
                                          (1.0/12.0) * mass * ( pow(Xsize, 2) + pow (Ysize, 2) )));
}


chrono::ChSharedBodyPtr AddToChSystem(chrono::ChSystem *system, MDShape* shape)
{
	chrono::ChSharedBodyPtr newbody(new chrono::ChBody);
	system->AddBody(newbody);
	SetKinematicParamsFromMDToChrono(newbody.get_ptr(), shape);
		// set mass and other property
	MD_Shape_Params *shape_params = shape->GetPhysicsParams();
	FloatType mass = shape_params->mass;
	newbody->SetMass(mass);	
	if(MDCuboid* pcube = dynamic_cast<MDCuboid*>(shape))
	{
		// create a new body
		// get the cube params

		MD_Cuboid_Params* cubparams = (MD_Cuboid_Params*)pcube->GetGeometryParams();
		SetChronoBoxPhysicsProperty(newbody.get_ptr(), cubparams->lx, cubparams->ly, cubparams->lz, mass);

		// set material property
		
		if(false)
		{
			// add collision model
		}
		// visual is handled by dem simulator
	}

    if(MDCone *pcone = dynamic_cast<MDCone*>(shape))
    {
        MD_Cone_Params* cubparams = (MD_Cone_Params*)pcone->GetGeometryParams();
        SetChronoBoxPhysicsProperty(newbody.get_ptr(), cubparams->height, cubparams->radius, cubparams->radius, mass);
    }
	return newbody;
}


void MultibodySimulatorInterfaceChrono::AddBody(MDShape* shape)
{
	
	node_container_.push_back(shape);
	chrono::ChSharedBodyPtr newbody = AddToChSystem(system_, shape);
	body_container_.push_back(newbody.get_ptr());
}


MDShape * createSampleBox(double lx, double ly, double lz, double mass)
{
    MDCuboid* pcuboid = (MDCuboid*)BaseShapeFactory::CreateShape("Cuboid");
    
    // set geometric property
    MD_Cuboid_Params geoparams;
    geoparams.lx = lx;
    geoparams.ly = ly;
    geoparams.lz = lz;
    geoparams.mparams.kn = 2e7;
    geoparams.mparams.gn = 500;
    geoparams.mparams.mu = 0.3;
    pcuboid->CuboidInit(geoparams);
    // set physics property

    MD_Shape_Params* phyparams = pcuboid->GetPhysicsParams();
    phyparams->mass = mass;

    return pcuboid;
}

MDShape * createSampleCone(double lx, double ly, double mass)
{
    MDCone* pcuboid = (MDCone*)BaseShapeFactory::CreateShape("Cone");
    
    // set geometric property
    MD_Cone_Params geoparams;
    geoparams.height = lx;
    geoparams.radius = ly;
    geoparams.mparams.kn = 2e7;
    geoparams.mparams.gn = 500;
    geoparams.mparams.mu = 0.3;
    pcuboid->ConeInit(geoparams);
    // set physics property

    MD_Shape_Params* phyparams = pcuboid->GetPhysicsParams();
    phyparams->mass = mass;

    return pcuboid;
}



void MultibodySimulatorInterfaceChrono::SetTest()
{
	std::ifstream inputfile("input.txt");
	if (!inputfile.is_open())
	{
    	std::cout << "No input file found. Press Enter to exit.\n";
    	std::cin.ignore(std::numeric_limits <std::streamsize> ::max(), '\n');
    	exit(0);
	}
	std::vector<std::vector<double> > data;
	data.reserve(1e4);
	ReadCSV(inputfile, data);
    const double bodyheight = 35;
    const double zshift = 0.01;

    
	chrono::ChSharedPtr<chrono::ChBody> ground(new chrono::ChBody);
    ground->SetPos(chrono::ChVector<>(10, 0, 10));
    ground->SetBodyFixed(true);
    system_->AddBody(ground);
    double torquelimit = 1.0;
    std::ifstream controlparamsfile("params.txt");
    if (!controlparamsfile.is_open())
    {
        std::cout << "no control file found\n";
        exit(0);
    }
    if (false)
    {
    MDShape *shape = createSampleCone(2, 5, 2.5 * 50);
    node_container_.push_back(shape);
    chrono::ChSharedBodyPtr tmp = AddToChSystem(system_, shape);
    tmp->SetPos(chrono::ChVector<>(10, 15, 30));
    body_container_.push_back(tmp.get_ptr());
    SetKinematicParamsFromChronoToMD(shape, tmp.get_ptr());
    }
    if (true)
    // create a perfactly controlled robot
    {
    	// create a upperframe
    	// cm and gram
        /*
    	MDShape *shape = createSampleBox(5, 2, 5, 400);
    	shape->SetNodePos(make_vec3(10, bodyheight, 10));
    	node_container_.push_back(shape);
    	chrono::ChSharedBodyPtr upperframe = AddToChSystem(system_, shape);
    	body_container_.push_back(upperframe.get_ptr());

        chrono::ChSharedPtr<chrono::ChLinkLockPrismatic> inplanelink(new chrono::ChLinkLockPrismatic);
        inplanelink->Initialize(ground, upperframe, chrono::ChCoordsys<>(chrono::ChVector<>()));
        
        system_->AddLink(inplanelink);

        chrono::ChFunction_Sine *posfunc = (new chrono::ChFunction_Sine(0, 1, 4));
        inplanelink->SetMotion_Y(posfunc);
        */

        MDShape *shape = createSampleBox(2, 2, 15, 200);
        shape->SetNodePos(make_vec3(15, bodyheight, 15));
        node_container_.push_back(shape);
        chrono::ChSharedBodyPtr upperframe = AddToChSystem(system_, shape);
        body_container_.push_back(upperframe.get_ptr());

        
    	chrono::ChSharedPtr<chrono::ChLinkLockOldham> inplanelink(new chrono::ChLinkLockOldham);
        inplanelink->Initialize(ground, upperframe, chrono::ChCoordsys<>(chrono::ChVector<>()));
        system_->AddLink(inplanelink);
        std::vector<chrono::ChSharedBodyPtr> legcontainer(8);

        const double leglen[4] = { 7.57, 7.77, 7.48, 3.40 };
        const chrono::ChQuaternion<> qter = chrono::Q_from_AngZ(chrono::CH_C_PI_2);
        for (int j = 0; j < 2; ++j)
        {
            double z_step = -7.5;
            if (j == 1)
                z_step = -z_step;
            z_step = z_step + 15;
            double y_step = bodyheight + leglen[0] / 2.;
            for (int k = 0; k < 4; ++k)
            {

                double x_step = 15.;
                if (k == 0)
                    y_step = y_step - leglen[k];
                else
                    y_step = y_step - 0.5 * (leglen[k] + leglen[k - 1]);
                double legw = 2.0;
                double legh = 2.0;
                MDShape *shape;
                if (k == 3)
                {
                    legw = 4.0;
                    //MDShape *shape = createSampleBox(leglen[k], legw, legw, 2.5 * (leglen[k] * legw * legw));
                    shape = createSampleCone(leglen[k], legw, 2.5 * (leglen[k] * legw * legw));
                }
                else
                {
                    shape = createSampleBox(leglen[k], legw, legw, 2.5 * (leglen[k] * legw * legw));

                }
                node_container_.push_back(shape);
                legcontainer[j * 4 + k] = AddToChSystem(system_, shape);
                legcontainer[j * 4 + k]->SetRot(qter);
                legcontainer[j * 4 + k]->SetPos(chrono::ChVector<>(x_step, y_step, z_step));
                legcontainer[j * 4 + k]->SetId(k + 1);
                body_container_.push_back(legcontainer[j * 4 + k].get_ptr());
                SetKinematicParamsFromChronoToMD(shape, legcontainer[j * 4 + k].get_ptr());

                chrono::ChSharedPtr<chrono::ChLinkEngineControl> mylink(new chrono::ChLinkEngineControl);
                std::string str;
                if (std::getline(controlparamsfile, str, '\n'))
                {
                    torquelimit = strtod(str.c_str(), NULL);
                }
                else
                {
                    torquelimit = 1;
                }
                std::cout << torquelimit << std::endl;
                mylink->Set_mot_torque_limits(torquelimit * 1e7, -torquelimit * 1e7);
                mylink->Set_mot_gain(30e7, 0, 0.3e7);
                if (k == 0)
                    mylink->Initialize(legcontainer[j * 4 + k], upperframe, chrono::ChCoordsys<>(chrono::ChVector<>(x_step, y_step + leglen[k] / 2.0, z_step)));
                else
                    mylink->Initialize(legcontainer[j * 4 + k], legcontainer[j * 4 + k - 1], chrono::ChCoordsys<>(chrono::ChVector<>(x_step, y_step + leglen[k] / 2.0, z_step)));
                mylink->Set_eng_mode(chrono::ChLinkEngineControl::ENG_MODE_TORQUE);
                chrono::ChFunction_Data* funptr = new chrono::ChFunction_Data(data);
                funptr->SetColumn(j * 4 + k + 1);
                mylink->Set_rot_funct(funptr);
                system_->AddLink(mylink);                

            }
        }
        

    }
}




void MultibodySimulatorInterfaceChrono::Step()
{
	const int nnode = node_container_.size();

	for(int i = 0; i < nnode; ++i)
	{
		
		vec3 shapeforce;
		vec3 shapemoment;
		node_container_[i]->GetNodeFoc(shapeforce);
		node_container_[i]->GetNodeMoF(shapemoment);
		body_container_[i]->Empty_forces_accumulators();
		chrono::ChVector<> force(shapeforce.x, shapeforce.y, shapeforce.z);
		chrono::ChVector<> moment(shapemoment.x, shapemoment.y, shapemoment.z);
		body_container_[i]->Accumulate_force(force, body_container_[i]->GetPos(), false);
		body_container_[i]->Accumulate_torque(moment, false);
	}
	// todo;
	system_->DoStepDynamics(time_step_);
	// now update our MDShape* using ChBody*
	
	for(int i = 0; i < nnode; ++i)
	{
		SetKinematicParamsFromChronoToMD(node_container_[i], body_container_[i]);
	}

}

