#ifndef _MD_MULTIBODY_CHRONO_H_
#define _MD_MULTIBODY_CHRONO_H_

/* this is an interface class that defines the behavior of multibody simulator

*/
#include "md_header.h"
#include "md_multibody.h"
#include <vector>

namespace chrono
{
	class ChSystem;
	class ChBody;
}

class MultibodySimulatorInterfaceChrono : public MultibodySimulatorInterface
{
	chrono::ChSystem* system_;
	std::vector<chrono::ChBody*> body_container_;
public:
	MultibodySimulatorInterfaceChrono();
	~MultibodySimulatorInterfaceChrono();
	// step one timestep forward
	void Step();
	// I deally I do not like the idea of params, will change the behavior through polymorphism, 
	// I have to test polymorphism on CUDA, which will change the way we manage the memory 
	// (say we cannot shallow copy a complicated class)
	void AddBody(MDShape*);
	void SetTest();
	void dump();
};

#endif