#include "md_rft.h"
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <cassert>
#include "MemoryManagement/md_cudamem.cuh"
#include "CollisionShapes/md_shape.h"
using namespace std;

#ifdef __ENABLE_CUDA__

MD_RFT::MD_RFT(int nvg, int nva): h_mem_(nvg, nva), d_mem_(nvg, nva)
{
	n_gamma_ = nvg;
	n_alpha_ = nva;
	karea_ = 2.54 * 2.54 * 1.5;
	planez_ = make_vec3(0.0, 0.0, 1.0);
    plane_dist_ = 5.0;
    ffac_ = 1e5;
}


void MD_RFT::GetkVector(string gfile, string afile)
{
    gammafname_ = gfile;
    alphafname_ = afile;
}

void MD_RFT::GetkMatrix(string kpfile, string kdfile)
{
    kdfname_ = kdfile;
    kpfname_ = kpfile;
}

void MD_RFT::InitializeMatrix()
{
	fstream inputfile;
    
	vector <double> buffer;
	    
    int mat_size = n_gamma_ * n_alpha_;
    buffer.resize(mat_size);
    
	inputfile.open(kpfname_.c_str(), ios::in | ios::binary);
	inputfile.read((char*) &buffer[0], sizeof(double) * mat_size);
	copy(buffer.begin(), buffer.end(), h_mem_.kp.begin());
	CudaMemHostToDevice(d_mem_.p_kp, h_mem_.p_kp, mat_size * sizeof(FloatType));
	inputfile.close();
    
	inputfile.open(kdfname_.c_str(), ios::in | ios::binary);
	inputfile.read((char*) &buffer[0], sizeof(double) * mat_size);
	copy(buffer.begin(), buffer.end(), h_mem_.kd.begin());
	CudaMemHostToDevice(d_mem_.p_kd, h_mem_.p_kd, mat_size * sizeof(FloatType));
	inputfile.close();
    
    
    buffer.resize(n_gamma_);
	inputfile.open(gammafname_.c_str(), ios::in | ios::binary);
	inputfile.read((char*) &buffer[0], sizeof(double) * n_gamma_);
	copy(buffer.begin(), buffer.end(), h_mem_.vgamma.begin());
	CudaMemHostToDevice(d_mem_.p_vg, h_mem_.p_vg, n_gamma_ * sizeof(FloatType));
	inputfile.close();
    
    buffer.resize(n_alpha_);
	inputfile.open(alphafname_.c_str(), ios::in | ios::binary);
	inputfile.read((char*) &buffer[0], sizeof(double) * n_alpha_);
	copy(buffer.begin(), buffer.end(), h_mem_.valpha.begin());
	CudaMemHostToDevice(d_mem_.p_va, h_mem_.p_va, n_alpha_ * sizeof(FloatType));
	inputfile.close();
}

MD_RFT::~MD_RFT()
{
    
}

void MD_RFT::IntersectSphere(vec4* pos, vec4* vel, uint npar, vec4* foc, vec4* mom)
{
	CudaCollideRFTSphere(d_mem_, pos, vel, npar, foc, mom);
}


void MD_RFT::Interpolate(FloatType gamma, FloatType alpha, FloatType* kzd)
{
    int i1, i2,  j1, j2;
    
    vector<FloatType>::iterator itg1;
    vector<FloatType>::iterator itg2;
    vector<FloatType>::iterator ita1;
    vector<FloatType>::iterator ita2;
    itg1 = lower_bound(h_mem_.vgamma.begin() + 1, h_mem_.vgamma.end() - 1, gamma);
    itg2 = upper_bound(h_mem_.vgamma.begin() + 1, h_mem_.vgamma.end() - 1, gamma);
    ita1 = lower_bound(h_mem_.valpha.begin() + 1, h_mem_.valpha.end() - 1, alpha);
    ita2 = upper_bound(h_mem_.valpha.begin() + 1, h_mem_.valpha.end() - 1, alpha);
    
    std::vector<FloatType>* pz;
    std::vector<FloatType>* pd;
    pz = &h_mem_.kp;
    pd = &h_mem_.kd;
    
    FloatType kd, kz;
    FloatType xs[2];
    FloatType ys[2];

    FloatType data[2][2];
    xs[0] = *itg1;
    xs[1] = *itg2;
    ys[0] = *ita1;
    ys[1] = *ita2;
    // --------------------------------------------
    i1 = int(itg1 - h_mem_.vgamma.begin());
    i2 = int(itg2 - h_mem_.vgamma.begin());
    j1 = int(ita1 - h_mem_.valpha.begin());
    j2 = int(ita2 - h_mem_.valpha.begin());
    
    data[0][0] = (*pz)[i1 * n_alpha_ + j1];
    data[1][0] = (*pz)[i2 * n_alpha_ + j1];
    data[0][1] = (*pz)[i1 * n_alpha_ + j2];
    data[1][1] = (*pz)[i2 * n_alpha_ + j2];
    // kz = itp_bilinear(gamma, alpha, xs, ys, data);
    // interpolation is done for the input matrices
    kz = (data[0][0] + data[1][0] + data[0][1] + data[1][1]) / 4.0;
    //------------------------------------------------
    data[0][0] = (*pd)[i1 * n_alpha_ + j1];
    data[1][0] = (*pd)[i2 * n_alpha_ + j1];
    data[0][1] = (*pd)[i1 * n_alpha_ + j2];
    data[1][1] = (*pd)[i2 * n_alpha_ + j2];
    // kd = itp_bilinear(gamma, alpha, xs, ys, data);
    // interpolation is done for the input matrices
    kd = (data[0][0] + data[1][0] + data[0][1] + data[1][1]) / 4.0;
    
    kzd[0] = kz;
    kzd[1] = kd;
}

void MD_RFT::InteractExt(MDShape* pshape)
{
    int npiece = 100;
    vector<vec3> pos_list(npiece);
    vector<vec3> vel_list(npiece);
    vector<vec3> ori_list(npiece);
    vector<FloatType> are_list(npiece);
    vector<vec3> nor_list(npiece);
    vector<vec3> foc_list(npiece);
    
    vec3 shape_pos = make_vec3(0, 0, 0);
    vec3 foc_pre = make_vec3(0, 0, 0);
    vec3 mom_pre = make_vec3(0, 0, 0);
    pshape->GetNodePos(shape_pos);
    bool ifcollision = pshape->RFTiDivSegments(planez_, plane_dist_, pos_list, ori_list, nor_list, vel_list, are_list);
    if (ifcollision) 
    {
        vec3 tforce = make_vec3(0, 0, 0);
        vec3 shapef = make_vec3(0, 0, 0);
        vec3 shapem = make_vec3(0, 0, 0);
        pshape->GetNodeFocList(foc_list);
		vec3 ydir;
		pshape->GetNodeAxisY(ydir);
        // cout << ydir << " ";
        // cout << setprecision(6) << scientific << pos_list[0] << vel_list[0] << endl;
        for (int i = 0; i < npiece; i++) 
        {
            // cout << i << " " << ori_list[i] << " " << nor_list[i] << " ";
            InteractPiece2(ydir, pos_list[i], vel_list[i], ori_list[i], nor_list[i], are_list[i], foc_list[i], tforce);
            foc_list[i] = tforce;
            shapef = shapef + tforce;
            shapem = shapem + cross(pos_list[i] - shape_pos, tforce);
            // cout << endl;
            // cout << i << " " << are_list[i] << endl;
        }
        // cout << foc_list[0] << endl;
        pshape->GetNodeFoc(foc_pre);
        pshape->GetNodeMoF(mom_pre);
        pshape->SetNodeFocList(foc_list);
        pshape->SetNodeFoc(shapef + foc_pre);
        pshape->SetNodeMoF(shapem + mom_pre);
    }
}

void MD_RFT::InteractPiece2(const vec3& ydir, const vec3& pos, const vec3& vel, const vec3& ori, const vec3& nor, FloatType area, const vec3& force_pre, vec3& force)
{
    const FloatType height = dot(planez_, pos);
    if (height < plane_dist_)
    {
		FloatType vy = dot(ydir, vel);
		vec3 velp = vel - vy * ydir;
        FloatType abs_ori = sqrt(dot(ori, ori));
        assert (abs_ori > 1e-8);
        FloatType abs_vel = sqrt(dot(velp, velp));
        if (dot(nor, velp) < 0)
        {
            force = make_vec3(0.0, 0.0, 0.0);
            return;
        }
        if (abs_vel < MD_RFT_VTHRESH)
        {
            force = force_pre;
            return;
        }
		vec3 planex = GetPlanePara(ydir);
        // cout << "orit " << ori << endl;
        // cout << "para " << velp << endl;
        // cout << "dirc " << planex << endl;
        FloatType gamma = GetVelAngle(velp, planex, planez_);
        FloatType alpha = GetOriAngle(ori,  planex, planez_);
        FloatType kzd[2];
        Interpolate(gamma, alpha, kzd);
        // cout << gamma << " " << alpha  << " " << kzd[0] << "\n";
        FloatType forceperp = kzd[0] * area / karea_ * ffac_;
        FloatType forcepara = kzd[1] * area / karea_ * ffac_;
        // two regimes
        forceperp = forceperp * (plane_dist_ - height);
        forcepara = forcepara * (plane_dist_ - height);
        force = forceperp * planez_ + forcepara * planex;
		// introduce a ydirection force to prevent drifting
		int signvy = vy >= 0 ? 1 : -1;
		// cout << "v: " << planex  <<" " <<  velp << endl;
		FloatType fy = -signvy * min(0.3 * sqrt(dot(force, force)), 1e4 * abs(vy));
		force = force + fy * ydir;
    }
    else
    {
        force = make_vec3(0.0, 0.0, 0.0);
    }       
}


void MD_RFT::InteractPiece3(const vec3& ydir, const vec3& pos, const vec3& vel, const vec3& ori, const vec3& nor, FloatType area, const vec3& force_pre, vec3& force)
{
    FloatType height = dot(planez_, pos);
    if (height < plane_dist_)
    {
        const vec3 gvec = make_vec3(0.0, 0.0, -1);
        FloatType abs_vel = sqrt(dot(vel, vel));
        if (dot(nor, vel) < 0)
        {
            force = make_vec3(0.0, 0.0, 0.0);
        }
        if(abs_vel < MD_RFT_VTHRESH)
        {
            force = force_pre;
            return;
        }
        // may not needed if ori is normalized;
        FloatType abs_ori = sqrt(dot(ori, ori));
        assert(abs_ori > 1e-8);
        vec3 ohat = ori / abs_ori;
        //
        vec3 vhat = vel / abs_vel;
        vec3 nvec = cross(gvec, vhat);  // vector perp to the plane of vel and gravity.
        vec3 lvec = cross(ohat, nvec);  // direction of the intersection line between small plate and the gravity-velocity plane.
        vec3 pori = cross(nvec, lvec);  // the projected normal direction of the plate in the gravity-velocity plane.
        FloatType sinpangle = dot(nvec, ohat);
        FloatType cospangle = sqrt(1 - sinpangle * sinpangle);
        vec3 planex = cross(nvec, gvec);
        //
        assert(dot(planex, vel) >= 0);
        //
        FloatType gamma = GetVelAngle(vel,  planex, planez_);
        FloatType alpha = GetOriAngle(ohat, planex, planez_);
        FloatType kxz[2];
        Interpolate(gamma, alpha, kxz);

        FloatType fz = kxz[0] * cospangle * area / karea_ * (plane_dist_ - height) * ffac_;
        FloatType fx = kxz[1] * cospangle * area / karea_ * (plane_dist_ - height) * ffac_;
        vec3 forcepara = fx * planex + fz * planez_;
        FloatType fpp = dot(forcepara, pori);
        if(dot(pori, vel) > 0)
        {
            fpp = -fpp;
        }

    }
    else
    {
        force = make_vec3(0.0, 0.0, 0.0);
    }       
}

#else
#endif
