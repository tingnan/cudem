#include "md_rft.cuh"
#include "GeneralHelper/md_cuda_helper.cuh"
#include "MathHelper/md_vec_cuda.cuh"

__host__ __device__ vec3 GetPlaneNorm()
{
    return make_vec3(0.0, 0.0, 1.0);
}
__host__ __device__ vec3 GetPlanePara(vec3 vecy)
{
	// vec3 vecx = make_vec3(1.0, 0.0, 0.0);
	// vec3 vecz = GetPlaneNorm(vel);
	// vec3 velp = vel - dot(vel, vecz) * vecz;
	// FloatType abs_velp = sqrt(dot(velp, velp));
	// return abs_velp < MD_RFT_VTHRESH? vecx: velp/abs_velp;
	vec3 vecz = GetPlaneNorm();
	return cross(vecy, vecz);
}


__host__ __device__ FloatType GetVelAngle(vec3 vel, vec3 plane_para, vec3 plane_dirc)
{
    FloatType gamma;
    FloatType math_pi = 3.1415926535897932384626433832795;
    bool ioflag; // if the piece is inserting or retracting
    if (dot(vel, plane_dirc) < 0)
    {
        ioflag = 0;
    }
    else
    {
        ioflag = 1;
    }
    FloatType abs_vel = length(vel);
    FloatType cosgamma = ( dot(vel, plane_para) / abs_vel );
    if (ioflag == 1)
        gamma = acos(cosgamma);
    if (ioflag == 0)
        gamma = 2 * math_pi - acos(cosgamma);
    return gamma;
}

__host__ __device__ FloatType GetOriAngle(vec3 ori, vec3 plane_para, vec3 plane_dirc)
{
    FloatType alpha;
    vec3 tmp_ori = ori;
    if (dot(ori, plane_dirc) < 0)
        tmp_ori = make_vec3(0, 0, 0) - ori;
    FloatType ori_para = dot(plane_para, tmp_ori);
    FloatType abs_ori = length(ori);
    FloatType cosalpha = dot(tmp_ori, plane_dirc) / abs_ori;
    if (ori_para > 0)
        alpha = -acos(cosalpha);
    else
        alpha =  acos(cosalpha);
    return alpha;

}

// need some work
__device__ void IntersectPlaneSphere(vec3  perp,
                                     vec3  para, 
                                FloatType  dist, 
                                     vec3  sphere_pos, 
                                     vec3  sphere_vel,
                                FloatType  sphere_rad,
                                     vec3* ori_list,
                                     vec3* pos_list,
                                     vec3* vel_list,
                                FloatType* are_list)
{
	FloatType s_angle = -dc_math_pi;
	FloatType e_angle =  dc_math_pi;
	int npiece = 30;
	vec3 orth = cross(perp, para);
	for (int i = 0; i < npiece; i++)
	{
		FloatType tmpangle = s_angle + (e_angle - s_angle) * FloatType(i) / FloatType(npiece);
		vec3 surf_drv = sphere_rad * rot_axis(orth, perp, tmpangle);
		ori_list[i] = surf_drv / sphere_rad;
		pos_list[i] = sphere_pos + surf_drv;
		vel_list[i] = sphere_vel;
		are_list[i] = abs((dc_math_pi *sin(tmpangle) * sphere_rad) * (sphere_rad * 2 * dc_math_pi / FloatType(npiece)));
	}
}

__device__ uint MatIndex(uint i, uint j, uint ny)
{
    return i * ny + j;
}

__device__ void Interpolation(FloatType* p_kp,
                              FloatType* p_kd,
                              FloatType* p_vg,
                              FloatType* p_va,
                              uint ngamma,
                              uint nalpha,
                              FloatType  gamma,
                              FloatType  alpha,
                              FloatType* p_kpd)
{
    int i;
    for(i = 0; i < ngamma; ++i)
    {
        if(gamma > p_vg[i]) 
			continue;
		break;
    }
    int j;
    for(j = 0; j < nalpha; ++j)
    {
        if(alpha > p_va[j]) 
			continue;
		break;
    }
	uint index = iIndex();
    uint lbi = max(i - 1, 0);
    uint ubi = min(ngamma - 1, i);
    uint lbj = max(j - 1, 0);
    uint ubj = min(nalpha - 1, j);
    uint index1 = MatIndex(lbi, lbj, nalpha);
    uint index2 = MatIndex(lbi, ubj, nalpha);
    uint index3 = MatIndex(ubi, lbj, nalpha);
    uint index4 = MatIndex(ubi, ubj, nalpha);
    p_kpd[0] = (p_kp[index1] + p_kp[index2] + p_kp[index3] + p_kp[index4])/4.0;
    p_kpd[1] = (p_kd[index1] + p_kd[index2] + p_kd[index3] + p_kd[index4])/4.0;
}


__device__ FloatType ScalingFact(FloatType abs_vel)
{
    if(abs_vel < 5e-1)
        return abs_vel / 5e-1;
    else
        return 1;
}

__device__ vec3 InteractPiece(vec3 perp, vec3 para, FloatType dist, vec3 ori, vec3 pos, vec3 vel, FloatType area, FloatType kpd[2])
{
	FloatType karea = 2.54 * 2.54 * 1.5;
    FloatType height = dot(perp, pos);
    vec3 force;
    if (height < dist)
    {
        if (dot(ori, vel) < 0)
        {
            force = make_vec3(0.0, 0.0, 0.0);
            return force;
        }
        FloatType factor = ScalingFact(dot(ori, vel));
        FloatType forceperp = factor * kpd[0] * area / karea * (dist - height) * 1e5;
        FloatType forcepara = factor * kpd[1] * area / karea * (dist - height) * 1e5;
        force = forceperp * perp + forcepara * para;
        return force;
    }
    else
    {
        force = make_vec3(0.0, 0.0, 0.0);
        return force;
    }       
}

__global__ void CollideRFTSphere(FloatType* p_kp,
                                 FloatType* p_kd,
                                 FloatType* p_vg,
                                 FloatType* p_va,
                                 uint  ngamma,
                                 uint  nalpha,
                                 vec4* p_pos, 
                                 vec4* p_vel, 
                                 uint  num_par, 
                                 vec4* c_foc, 
                                 vec4* c_mom)
{
    uint index = iIndex();
    if(index >= num_par) return;
    
    vec4 pos = p_pos[index];
    vec4 vel = p_vel[index];
    vec3 sphere_pos = make_vec3(pos.x, pos.y, pos.z);
    vec3 sphere_vel = make_vec3(vel.x, vel.y, vel.z);

    vec3 ori_list[30];
    vec3 pos_list[30];
    vec3 vel_list[30];
    FloatType are_list[30];
    vec3 perp = make_vec3(0.0, 0.0, 1.0);
    FloatType dist = 5.0;
    
    FloatType sphere_spd = sqrt(dot(sphere_vel, sphere_vel));
    if (sphere_spd < MD_RFT_VTHRESH)
    {
        // the current approach only works for objects without angular velocity
        return;
    }

    // intersect wee need to find a normal direction to vel!
	vec3 para = GetPlanePara(cross(sphere_vel/sphere_spd, GetPlaneNorm()));
    IntersectPlaneSphere(perp, para, dist, sphere_pos, sphere_vel, pos.w, ori_list, pos_list, vel_list, are_list);
    
    vec3 force = make_vec3(0.0, 0.0, 0.0);
    for(uint i = 0; i < 30; ++i)
    {
        FloatType gamma = GetVelAngle(vel_list[i], para, perp);
        FloatType alpha = GetOriAngle(ori_list[i], para, perp);
        // interpolate;
        FloatType kpd[2];
        Interpolation(p_kp, p_kd, p_vg, p_va, ngamma, nalpha, gamma, alpha, kpd);
        // calculate force;
        force += InteractPiece(perp, para, dist, ori_list[i], pos_list[i], vel_list[i], are_list[i], kpd);
    }
    c_foc[index] = make_vec4(-force.x, -force.y, -force.z, 0.0);
}


void CudaCollideRFTSphere(MD_RFT_Cuda_Memory& d_mem, vec4* pos, vec4* vel, uint num_par, vec4* foc, vec4* mom)
{
    uint nblocks, nthreads;
    ComputeGridSize(num_par, WORK_SIZE, nblocks, nthreads);
    uint ng = d_mem.vgamma.size();
    uint na = d_mem.valpha.size();
    //cudaPrintfInit ();
    CollideRFTSphere<<<nblocks, nthreads>>>(d_mem.p_kp, d_mem.p_kd, d_mem.p_vg, d_mem.p_va, ng, na, pos, vel, num_par, foc, mom);
    //cudaPrintfDisplay (stdout, true);
    //cudaPrintfEnd ();
    KernelError("CudaCollideRFTSphere");
}

void MD_RFT_Cuda_Malloc(MD_RFT_Cuda_Memory& obj, uint n1, uint n2)
{
    obj.kp.resize(n1 * n2);
    obj.kd.resize(n1 * n2);
    obj.vgamma.resize(n1);
    obj.valpha.resize(n2);
    obj.p_kp = thrust::raw_pointer_cast(&(obj.kp[0]));
    obj.p_kd = thrust::raw_pointer_cast(&(obj.kd[0]));
    obj.p_vg = thrust::raw_pointer_cast(&(obj.vgamma[0]));
    obj.p_va = thrust::raw_pointer_cast(&(obj.valpha[0]));
}
