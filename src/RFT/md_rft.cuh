#ifndef _MD_RFT_CUDA_CUH_
#define _MD_RFT_CUDA_CUH_
#include "MathHelper/md_vec_cuda.cuh"
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#define MD_RFT_VTHRESH 1e-2

struct MD_RFT_Cuda_Memory
{
	friend void MD_RFT_Cuda_Malloc(MD_RFT_Cuda_Memory&, uint, uint);
public:	
    thrust::device_vector<FloatType> kp;
	thrust::device_vector<FloatType> kd;
	thrust::device_vector<FloatType> vgamma;
	thrust::device_vector<FloatType> valpha;
	FloatType* p_kp;
	FloatType* p_kd;
	FloatType* p_vg;
	FloatType* p_va;

	MD_RFT_Cuda_Memory(uint n1, uint n2)
	{
		MD_RFT_Cuda_Malloc(*this, n1, n2);
	}
};


struct MD_RFT_Host_Memory
{
typedef std::vector<FloatType> f_array;
public:	
    f_array kp;
	f_array kd;
	f_array vgamma;
	f_array valpha;
	FloatType* p_kp;
	FloatType* p_kd;
	FloatType* p_vg;
	FloatType* p_va;	

	MD_RFT_Host_Memory(uint n1, uint n2)
	{
		vgamma.resize(n1);
		valpha.resize(n2);
		kp.resize(n1 * n2);
		kd.resize(n1 * n2);
		p_kp = &kp[0];
		p_kd = &kd[0];
		p_vg = &vgamma[0];
		p_va = &valpha[0];
	}
};


__host__ __device__ FloatType GetVelAngle(vec3, vec3, vec3);
__host__ __device__ FloatType GetOriAngle(vec3, vec3, vec3);
__host__ __device__ vec3 GetPlanePara(vec3);
__host__ __device__ vec3 GetPlaneNorm();

void CudaCollideRFTSphere(MD_RFT_Cuda_Memory&, vec4* pos, vec4* vel, uint npar, vec4* foc, vec4* mom);

#endif
