/* 
 * File:   md_rft.h
 * Author: tingnan
 *
 * Created on December 15, 2011, 4:44 PM
 */

#ifndef _MD_RFT_H_
#define	_MD_RFT_H_
#include "md_header.h"
#include <vector>
#include <fstream>
#include <string>
#include <sstream>

#ifdef __ENABLE_CUDA__

#include "md_rft.cuh"
class MD_RFT {
private:
	MD_RFT_Cuda_Memory d_mem_;
	MD_RFT_Host_Memory h_mem_;
	int n_gamma_;
	int n_alpha_;
	vec3 planez_;
    FloatType plane_dist_;
    FloatType karea_;
    FloatType ffac_;
    std::string gammafname_;
    std::string alphafname_;
    std::string kdfname_;
    std::string kpfname_;
    //cpu only code
    void InteractPiece2(const vec3& xdir, const vec3& pos, const vec3& vel, const vec3& ori, const vec3& nor, FloatType area, const vec3& force_pre, vec3& force);
    void InteractPiece3(const vec3& xdir, const vec3& pos, const vec3& vel, const vec3& ori, const vec3& nor, FloatType area, const vec3& force_pre, vec3& force);
    void Interpolate(FloatType, FloatType, FloatType*);
    FloatType itp_bilinear(FloatType, FloatType, FloatType*, FloatType*, FloatType**);
    FloatType GetAngleVel(const vec3&, const vec3&);
    FloatType GetAngleOri(const vec3&, const vec3&);
public:
    MD_RFT(int, int);
    ~MD_RFT();
    void GetkVector(std::string, std::string);
    void GetkMatrix(std::string, std::string);
    void InitializeMatrix();
    //cpu only function
    void InteractExt(class MDShape* pshape);
    //gpu only function
	void IntersectSphere(vec4*, vec4*, uint, vec4*, vec4*);
};

#else
#endif
#endif	/* MD_RFT_H */

