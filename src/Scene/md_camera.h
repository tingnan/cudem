#include <glm/glm.hpp>
// glm::translate, glm::rotate, glm::scale
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

class GLMCamera
{
protected:
    float mAspectRatio;
    float mFov;
    float mNear;
    float mFar;
    // its position in world coordinate
    glm::vec3 mPosition;
    // where it looks at
    glm::vec3 mLookAt;
    // the "up" direction
    glm::vec3 mUp;
    // computed from glm::LookAt(mPosition, mLookAt, mUp);
    glm::mat4 mModelViewMatrix;
    // project from camera coordinates to clip space
    // using glm::perspective(mFov, mAspectRatio, mNear, mFar);
    glm::mat4 mProjectionMatrix;
    glm::mat4 mModelViewProjectionMatrix;

    // angleX and angleY in world coordinate space
    glm::vec3 mCameraDirection;
    float mDistance;
    const glm::vec3 mGlobalUp;
    glm::vec3 mPitchAxis;

    void UpdateMatrix()
    {
        // if we have set new values of any internal variable
        mModelViewMatrix = glm::lookAt(mPosition, mLookAt, mUp);
        mProjectionMatrix = glm::perspective(mFov, mAspectRatio, mNear, mFar); 
        mModelViewProjectionMatrix = mProjectionMatrix * mModelViewMatrix;
    }


public:
    GLMCamera(const glm::vec3& pos,
        const glm::vec3& lookat,
        const glm::vec3& up,
        float fov = 45.0f,
        float aspect = 4.0f / 3.0f,
        float near = 0.1f,
        float far = 100.0f) : 
        mPosition(pos), 
        mLookAt(lookat), 
        mUp(up), 
        mFov(fov), 
        mAspectRatio(aspect), 
        mNear(near), 
        mFar(far),
        mGlobalUp(0., 1., 0.)
    {
        

        mCameraDirection = mPosition - mLookAt;
        mDistance = glm::length(mCameraDirection);
        mCameraDirection = glm::normalize(mCameraDirection);
        mPitchAxis = glm::normalize(glm::cross(mGlobalUp, mCameraDirection));
        mUp = glm::normalize(glm::cross(mCameraDirection, mPitchAxis));
        UpdateMatrix();

    }

    void SetFov(float fov)
    {
        mFov = fov;
    }

    void SetAspectRatio(float aspect)
    {
        mAspectRatio = aspect;
    }
    void SetPosition(const glm::vec3& newpos)
    {
        mPosition = newpos;
    }
    void SetLookAt(const glm::vec3& newlookat)
    {
        mLookAt = newlookat;
    }
    void SetUpDirection(const glm::vec3& newup)
    {
        mUp = newup;
    }
    // will move both mPosition and mLookAt by delta

    int sign(double x)
    {
        return x >= 0 ? 1 : -1;
    }

    void SetDistance(float delta)
    {

        mDistance = mDistance - delta;
        mPosition = mCameraDirection * mDistance + mLookAt;
        UpdateMatrix();
    }

    void Translate(float deltaX, float deltaY)
    {
        glm::vec3 delta = mPitchAxis * deltaX + mUp * deltaY;
        mPosition = mPosition + delta;
        mLookAt = mLookAt + delta;
        // distance is preserved, up is not changed
        UpdateMatrix();
    }

    void Rotate(float deltaX, float deltaY)
    {
        // rotate about the mLookAt a
        // determine the current angleX and angleY in the world frame
        // first do y then do x

        // rotate about pitch deltaY
        // update the mCameraDirection and up direction, but keep pitchaxis
        mCameraDirection = glm::rotate(mCameraDirection, deltaY, mPitchAxis);

        // now rotate about (0, 1, 0)

        mCameraDirection = glm::rotate(mCameraDirection, deltaX, mGlobalUp);
        mPitchAxis = glm::rotate(mPitchAxis, deltaX, mGlobalUp);
        mUp = glm::normalize(glm::cross(mCameraDirection, mPitchAxis));

        // now both mCameraDirection, mPitchAxis, and mUp are at the final
        // update the mCameraPosition

        mPosition = mDistance * mCameraDirection + mLookAt;

        UpdateMatrix();
    }

    glm::mat4& GetModelViewMatrix()
    {
        return mModelViewMatrix;
    }
    glm::mat4& GetProjectionMatrix()
    {
        return mProjectionMatrix;
    }
    glm::mat4& GetModelViewProjectionMatrix()
    {
        return mModelViewProjectionMatrix;
    }


};