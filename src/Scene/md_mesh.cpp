
#include "md_mesh.h"
#include "md_shape_include.h"
#include <algorithm>
using std::vector;
using std::string;

void triangulateCube(FloatType x, 
    FloatType y, 
    FloatType z, 
    std::vector<FloatType>& vertices, 
    std::vector<FloatType>& colors,
    std::vector<FloatType>& normals,
    std::vector<uint>& indices)
{
    // to be used by glDrawElements()
    // number of vertices
    vertices.resize(24 * 4);
    colors.resize(24 * 4);
    normals.resize(24 * 4);
    // number of indicies
    indices.resize(12 * 3);
    // pm x/2, pm y/2 pm z/2
    const FloatType cubevertices[] = {
        // +x plane
        x / 2.0, -y / 2.0, -z / 2.0, 1.0, 
        x / 2.0, -y / 2.0,  z / 2.0, 1.0, 
        x / 2.0,  y / 2.0, -z / 2.0, 1.0, 
        x / 2.0,  y / 2.0,  z / 2.0, 1.0,

        // -x plane
       -x / 2.0, -y / 2.0, -z / 2.0, 1.0, 
       -x / 2.0, -y / 2.0,  z / 2.0, 1.0, 
       -x / 2.0,  y / 2.0, -z / 2.0, 1.0, 
       -x / 2.0,  y / 2.0,  z / 2.0, 1.0,

       // +y plane
       -x / 2.0,  y / 2.0, -z / 2.0, 1.0, 
       -x / 2.0,  y / 2.0,  z / 2.0, 1.0, 
        x / 2.0,  y / 2.0, -z / 2.0, 1.0, 
        x / 2.0,  y / 2.0,  z / 2.0, 1.0,

       // -y plane
       -x / 2.0, -y / 2.0, -z / 2.0, 1.0, 
       -x / 2.0, -y / 2.0,  z / 2.0, 1.0, 
        x / 2.0, -y / 2.0, -z / 2.0, 1.0, 
        x / 2.0, -y / 2.0,  z / 2.0, 1.0,


       // +z plane
       -x / 2.0, -y / 2.0,  z / 2.0, 1.0, 
        x / 2.0, -y / 2.0,  z / 2.0, 1.0, 
       -x / 2.0,  y / 2.0,  z / 2.0, 1.0, 
        x / 2.0,  y / 2.0,  z / 2.0, 1.0,

       // -z plane

       -x / 2.0, -y / 2.0, -z / 2.0, 1.0, 
        x / 2.0, -y / 2.0, -z / 2.0, 1.0, 
       -x / 2.0,  y / 2.0, -z / 2.0, 1.0, 
        x / 2.0,  y / 2.0, -z / 2.0, 1.0

    };

    const FloatType cubenormals[] = {
        // +x plane
        1.0, 0.0, 0.0, 1.0,
        1.0, 0.0, 0.0, 1.0,
        1.0, 0.0, 0.0, 1.0,
        1.0, 0.0, 0.0, 1.0,
        // -x plane
       -1.0, 0.0, 0.0, 1.0,
       -1.0, 0.0, 0.0, 1.0,
       -1.0, 0.0, 0.0, 1.0,
       -1.0, 0.0, 0.0, 1.0,
       // +y plane
        0.0, 1.0, 0.0, 1.0,
        0.0, 1.0, 0.0, 1.0,
        0.0, 1.0, 0.0, 1.0,
        0.0, 1.0, 0.0, 1.0,
       // -y plane
        0.0,-1.0, 0.0, 1.0,
        0.0,-1.0, 0.0, 1.0,
        0.0,-1.0, 0.0, 1.0,
        0.0,-1.0, 0.0, 1.0,
       // +z plane
        0.0, 0.0, 1.0, 1.0,
        0.0, 0.0, 1.0, 1.0,
        0.0, 0.0, 1.0, 1.0,
        0.0, 0.0, 1.0, 1.0,
        // -z plane
        0.0, 0.0,-1.0, 1.0,
        0.0, 0.0,-1.0, 1.0,
        0.0, 0.0,-1.0, 1.0,
        0.0, 0.0,-1.0, 1.0
    };

    const uint cubetirangleidices [] = {
        0, 1, 2,  1, 2, 3,
        4, 5, 6,  5, 6, 7,
        8, 9,10,  9,10,11,
       12,13,14, 13,14,15,
       16,17,18, 17,18,19,
       20,21,22, 21,22,23,
    };
    std::cout << "done generating!\n";
    std::copy(cubevertices, cubevertices + 24 * 4, vertices.begin());
    std::copy(cubenormals, cubenormals + 24 * 4, normals.begin());
    std::copy(cubenormals, cubenormals + 24 * 4, colors.begin());
    std::copy(cubetirangleidices, cubetirangleidices + 3 * 12, indices.begin());
}


void triangulateConeSurface(FloatType height, 
    FloatType radius,
    std::vector<FloatType>& vertices, 
    std::vector<FloatType>& colors,
    std::vector<FloatType>& normals,
    std::vector<uint>& indices)
{
    const int n = 30;

    // 2 * n on the circle, n on the cone's head, 1 on the center of bottom
    vertices.resize(12 * n + 4);
    colors.resize(12 * n + 4);
    normals.resize(12 * n + 4);
    // number of indicies
    indices.resize(6 * n);
    FloatType sidelen = sqrt(radius * radius + height * height);
    FloatType normalx = radius / sidelen;
    FloatType normalyz = height / sidelen;

    // process the bottom center point
    vertices[12 * n + 0] = 0;
    vertices[12 * n + 1] = 0;
    vertices[12 * n + 2] = 0;
    vertices[12 * n + 3] = 1.0;
    normals[12 * n + 0] = -1.0;
    normals[12 * n + 1] = 0;
    normals[12 * n + 2] = 0;
    normals[12 * n + 3] = 1.0;

    for (int i = 0; i < n; ++i)
    {
        FloatType theta = FloatType(i) / n * 2 * M_PI;
        // process each of the vertices on the circle, for the side surface
        vertices[4 * i + 0] = 0;
        vertices[4 * i + 1] = radius * cos(theta);
        vertices[4 * i + 2] = radius * sin(theta);
        vertices[4 * i + 3] = 1.0;
        normals[4 * i + 0] = normalx;
        normals[4 * i + 1] = normalyz * cos(theta);
        normals[4 * i + 2] = normalyz * sin(theta);
        normals[4 * i + 3] = 1.0;
        // process each of the vertices on the head, for the side surface
        vertices[4 * i + 0 + 4 * n] = height;
        vertices[4 * i + 1 + 4 * n] = 0;
        vertices[4 * i + 2 + 4 * n] = 0;
        vertices[4 * i + 3 + 4 * n] = 1.0;
        normals[4 * i + 0 + 4 * n] = normalx;
        normals[4 * i + 1 + 4 * n] = normalyz * cos(theta);
        normals[4 * i + 2 + 4 * n] = normalyz * sin(theta);
        normals[4 * i + 3 + 4 * n] = 1.0;
        // process each of the vertices on the circle, for the bottom surface
        vertices[4 * i + 0 + 8 * n] = 0;
        vertices[4 * i + 1 + 8 * n] = radius * cos(theta);
        vertices[4 * i + 2 + 8 * n] = radius * sin(theta);
        vertices[4 * i + 3 + 8 * n] = 1.0;
        normals[4 * i + 0 + 8 * n] = -1.0;
        normals[4 * i + 1 + 8 * n] = 0.0;
        normals[4 * i + 2 + 8 * n] = 0.0;
        normals[4 * i + 3 + 8 * n] = 1.0;    
    }

    for (int i = 0; i < n; ++i)
    {
        int j = i + 1 == n ? 0 : i + 1;
        // side plate
        indices[6 * i + 0] = i;
        indices[6 * i + 1] = j;
        indices[6 * i + 2] = i + n;
        // bottom plate
        indices[6 * i + 3] = i + 2 * n;
        indices[6 * i + 4] = j + 2 * n;
        indices[6 * i + 5] = 3 * n;
    }
    std::cout << "done generating\n";
}
void MeshGenerator::generateMesh(const char* name, 
        const void* params,
        std::vector<FloatType>& vertices, 
        std::vector<FloatType>& colors,
        std::vector<FloatType>& normals,
        std::vector<uint>& indices)
{
    string shapename(name);
    if(shapename == "Cuboid")
    {
        const MD_Cuboid_Params *cubeparam = (const MD_Cuboid_Params*)params;
        triangulateCube(cubeparam->lx, cubeparam->ly, cubeparam->lz, vertices, colors, normals, indices);
    }
    if(shapename == "Cone")
    {
        const MD_Cone_Params *conparam = (MD_Cone_Params*)params;
        triangulateConeSurface(conparam->height, conparam->radius, vertices, colors, normals, indices);
    }
}
