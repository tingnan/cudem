#ifndef _MD_MESH_H_
#define _MD_MESH_H_

#include <vector>
#include "md_header.h"
#include <string>
#include "CollisionShapes/md_shape.h"
#include <map>


class MeshGenerator
{
public:
    static void generateMesh(const char* name, 
    	const void* params, 
    	std::vector<FloatType>& vertices, 
    	std::vector<FloatType>& colors,
    	std::vector<FloatType>& normals,
    	std::vector<uint>& indices);
};


class MDGLMesh
{
protected:
	// vbos for each of the quantities defined below
	std::map<std::string, uint> mVBOs;
	// the vertices and their colors and normals;
    std::vector<FloatType> mVertices;
    std::vector<FloatType> mColors;
    std::vector<FloatType> mNormals;
    // the rendering triangle order
    std::vector<uint> mIndices;
public:
	const std::map<std::string, uint>& getVBOs() const {return mVBOs;}
	const std::vector<uint>& getVertexIndices() const {return mIndices;}
	const std::vector<FloatType>& getVertices() const {return mVertices;};
	const std::vector<FloatType>& getVertexNormals() const { return mNormals;}
	const std::vector<FloatType>& getVertexColors() const { return mColors;}

	// the non const version
	// since they are inlined functions, we do not need to use the cast trick
	std::map<std::string, uint>& getVBOs() {return mVBOs;}
	std::vector<uint>& getVertexIndices() {return mIndices;}
	std::vector<FloatType>& getVertices() {return mVertices;};
	std::vector<FloatType>& getVertexNormals() { return mNormals;}
	std::vector<FloatType>& getVertexColors() { return mColors;}


};

#endif