#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "md_scene.h"
#include "md_shape_include.h"
#include "Multibody/md_multibody_chrono.h"
#include "glm/gtc/matrix_inverse.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "md_camera.h"

GLuint CompileGLSLShader(GLenum target, const GLchar* shaderstr)
{
    GLuint shaderobj = glCreateShader(target);
    if(!shaderobj)
        return shaderobj;
    glShaderSource(shaderobj, 1, &shaderstr, NULL);
    glCompileShader(shaderobj);
    GLint iscompiled = 0;
    glGetShaderiv(shaderobj, GL_COMPILE_STATUS, &iscompiled);

    if(iscompiled != GL_TRUE)
    {
        GLint logSize = 0;
        glGetShaderiv(shaderobj, GL_INFO_LOG_LENGTH, &logSize);
        char errorLog[256] = "";
        glGetShaderInfoLog(shaderobj, 256, NULL, &errorLog[0]);
        std::cout << errorLog << std::endl;
        glDeleteShader(shaderobj);
    }
    return shaderobj;
}

GLuint CompileGLSLShaderFromFile(GLenum target, const char* filename)
{
    // we have to read the shader as a whole char;
    std::ifstream ifile(filename, std::ios::binary);
    ifile.seekg(0, ifile.end);
    int flength = ifile.tellg();
    ifile.seekg(0, ifile.beg);
    std::vector<char> buffer(flength);
    ifile.read(&buffer[0], flength);
    ifile.close();
    // make the char null terminated;
    buffer.push_back('\0');
    GLuint shader = CompileGLSLShader(target, &buffer[0]);
    return shader;
}

std::string Num2Str(int num)
{
  char buffer[256];
  sprintf(buffer,"%04d",num);
  std::string tmp(buffer);
  //x1-std::cout << tmp << std::endl;
  return tmp;
}

//////////////////////////////////////////////////
// Grab the OpenGL screen and save it as a .tga //
// Copyright (C) Marius Andra 2001              //
// http://cone3d.gz.ee  EMAIL: cone3d@hot.ee    //
//////////////////////////////////////////////////
// some modification done by me
int screenShotTGA(int const num)
{
    typedef unsigned char uchar;
    // we will store the image data here
    uchar *pixels;
    // the thingy we use to write files
    FILE * shot;
    // we get the width/height of the screen into this array
    int screenStats[4];

    // get the width/height of the window
    glGetIntegerv(GL_VIEWPORT, screenStats);
    std::cout << screenStats[0] << " ";
    std::cout << screenStats[1] << " ";
    std::cout << screenStats[2] << " ";
    std::cout << screenStats[3] << "\n";

    // generate an array large enough to hold the pixel data 
    // (width*height*bytesPerPixel)
    pixels = new unsigned char[screenStats[2]*screenStats[3]*3];
    // read in the pixel data, TGA's pixels are BGR aligned
    glReadPixels(0, 0, screenStats[2], screenStats[3], 0x80E0, 
    GL_UNSIGNED_BYTE, pixels);

    // open the file for writing. If unsucessful, return 1
    std::string filename = "./" + Num2Str(num) + ".tga";

    shot=fopen(filename.c_str(), "wb");

    if (shot == NULL)
        return 1;
    // this is the tga header it must be in the beginning of 
    // every (uncompressed) .tga
    uchar TGAheader[12]={0,0,2,0,0,0,0,0,0,0,0,0};
    // the header that is used to get the dimensions of the .tga
    // header[1]*256+header[0] - width
    // header[3]*256+header[2] - height
    // header[4] - bits per pixel
    // header[5] - ?
    uchar header[6]={((int)(screenStats[2]%256)),
    ((int)(screenStats[2]/256)),
    ((int)(screenStats[3]%256)),
    ((int)(screenStats[3]/256)),24,0};

    // write out the TGA header
    fwrite(TGAheader, sizeof(uchar), 12, shot);
    // write out the header
    fwrite(header, sizeof(uchar), 6, shot);
    // write the pixels
    fwrite(pixels, sizeof(uchar), 
    screenStats[2]*screenStats[3]*3, shot);

    // close the file
    fclose(shot);
    // free the memory
    delete [] pixels;

    // return success
    return 0;
}


#define BUFFER_OFFSET(i) ((void*)(i))


GLMCamera *gCamera;

GLApp::GLApp() : vertexSize_(4), partProgram_(0)
{  
    // have to create an OpenGL context first, or the sand pos will segfault  
    InitGL();
    // initialize a sample sandbox
    psandbox_ = new MDSimpleSand();//(4e4, 30, 30, 30);
    psandbox_->LoadFile("san.h5");
    //psandbox_->GenerateSampleSand();
    // set up an camera lookat it
    float aspect_ratio = ((float)windowW) / windowH;
    glm::vec3 campos(75.f, 45.f, 65.f);
    glm::vec3 lookat(35.f, 0.,  15.f);
    glm::vec3 up(0.f, 1.f, 0.f);
    camera_ = new GLMCamera(campos, lookat, up, 60.f, aspect_ratio);
    simulator_ = new MultibodySimulatorInterfaceChrono();
    simulator_->SetTimeStep(1e-5);
    gCamera = camera_;
    if(false)
    {
        collisionShapes_.push_back(BaseShapeFactory::CreateShape("Cuboid"));
        MDCuboid* pcuboid = (MDCuboid*)collisionShapes_.back();
    
        // set geometric property
        MD_Cuboid_Params geoparams;
        geoparams.lx = 5;
        geoparams.ly = 5;
        geoparams.lz = 5;
        geoparams.mparams.kn = 2e7;
        geoparams.mparams.gn = 500;
        geoparams.mparams.mu = 0.1;
        pcuboid->CuboidInit(geoparams);
        // set physics property

        MD_Shape_Params* phyparams = pcuboid->GetPhysicsParams();
        phyparams->mass = 200;

        vec3 pos = make_vec3(5.0, 15.0, 11.0);
        pcuboid->SetNodePos(pos);
        vec3 vel = make_vec3(50.0, 0.0, 0.0);
        pcuboid->SetNodeVel(vel);
        vec3 ome = make_vec3(0.0, 30.0, 0.0);
        pcuboid->SetNodeAngVel(ome);
        simulator_->AddBody(pcuboid);
    }

    simulator_->SetTest();
    // copy node_container;
    collisionShapes_ = simulator_->GetNodes();
    std::cout << collisionShapes_.size() << std::endl;
    
}

GLApp::~GLApp()
{
    glfwDestroyWindow(window_);
    glfwTerminate();
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
    switch(key)
    {
        case GLFW_KEY_SPACE :
            break;
        default:
            break;
    }
}


double ox;
double oy;

static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
    double dx = xpos - ox;
    double dy = ypos - oy;

    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
    {
        gCamera->Rotate(-dx / 50., -dy / 50.);
    }

    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS)
    {
        gCamera->Translate(-dx / 50., -dy / 50.);
    }

    ox = xpos;
    oy = ypos;
}


void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    gCamera->SetDistance(yoffset);
}



void CheckOpenGLVersion(GLFWwindow* window)
{
    int major, minor, rev;
    major = glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MAJOR);
    minor = glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MINOR);
    rev = glfwGetWindowAttrib(window, GLFW_CONTEXT_REVISION);
    printf("OpenGL version recieved: %d.%d.%d\n", major, minor, rev);
    printf("Supported OpenGL is %s\n", (const char*)glGetString(GL_VERSION));
    printf("Supported GLSL is %s\n", (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION));
}

void GLApp::InitGL()
{
    windowW = 800; 
    windowH = 600;

    if(!glfwInit())
    {
        std::cerr << "Failed to Init GLFW\n";
    }
    glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.2
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL 
    window_ = glfwCreateWindow(windowW, windowH, "My Title", NULL, NULL);
    if(!window_)
    {
        std::cerr << "Failed to Open GLFW window" << std::endl;
        glfwTerminate();
    }
    glfwMakeContextCurrent(window_);
    glfwSetKeyCallback(window_, key_callback);
    glfwSetCursorPosCallback(window_, cursor_position_callback);
    glfwSetScrollCallback(window_, scroll_callback);
    CheckOpenGLVersion(window_);
    // without this there will be segmentation fault
    glewExperimental=true; 
    if(glewInit() != GLEW_OK)
    {

        std::cerr << "Using GLEW Version: " << glewGetString(GLEW_VERSION) << std::endl;
        exit(EXIT_FAILURE);
    }

    glViewport(0, 0, windowW, windowH);
    cudaSetDevice(1);
}


void CreateProgramWithShaders(const char* vertshadername, const char* fragshadername, GLuint& programID)
{
    GLuint vertShaderID = CompileGLSLShaderFromFile(GL_VERTEX_SHADER, vertshadername);
    GLuint fragShaderID = CompileGLSLShaderFromFile(GL_FRAGMENT_SHADER, fragshadername);

    programID = glCreateProgram();
    glAttachShader(programID, vertShaderID);
    glAttachShader(programID, fragShaderID);
    glLinkProgram(programID);

    GLint success = 0;
    glGetProgramiv(programID, GL_LINK_STATUS, &success);

    if (!success)
    {
        char temp[256];
        glGetProgramInfoLog(programID, 256, 0, temp);
        std::cerr << "Failed to link program: " << temp << std::endl;
        glDeleteProgram(programID);
        programID = 0;
    }

}

void GLApp::SetShaders()
{
    CreateProgramWithShaders("partvertshader.glsl", "partfragshader.glsl", partProgram_);
    CreateProgramWithShaders("shapevertshader.glsl", "shapefragshader.glsl", shapeProgram_);
}


void GLApp::BindVisualAssets()
{
    // for now we only generate one vbo and one vao
    // for each shape we need to have a buffer and a vao

    // sand
    partVBO_ = psandbox_->getVBO();
    glGenVertexArrays(1, &partVAO_);
    glBindBuffer(GL_ARRAY_BUFFER, partVBO_);
        glBindVertexArray(partVAO_);
            glEnableVertexAttribArray(0);
            glVertexAttribPointer(0, vertexSize_, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
        glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // shapes
    const int nshapes = collisionShapes_.size();
    shapeVAOs_.resize(nshapes);
    for(int i = 0; i < nshapes; ++i)
    {
        collisionShapes_[i]->_bindGLAsset();
        std::map<std::string, uint> vbomap = collisionShapes_[i]->_getMesh()->getVBOs();
        glGenVertexArrays(1, &shapeVAOs_[i]);
        // bind the vertex
        glBindBuffer(GL_ARRAY_BUFFER, vbomap["vertex"]);
        glBindVertexArray(shapeVAOs_[i]);
            glEnableVertexAttribArray(0);
            glVertexAttribPointer(0, vertexSize_, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // bind the normal

        glBindBuffer(GL_ARRAY_BUFFER, vbomap["normal"]);
        glBindVertexArray(shapeVAOs_[i]);
            glEnableVertexAttribArray(1);
            glVertexAttribPointer(1, vertexSize_, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

    }
}

void GLApp::_drawPoints()
{
    //glEnable(GL_POINT_SPRITE_ARB);
    //glTexEnvi(GL_POINT_SPRITE_ARB, GL_COORD_REPLACE_ARB, GL_TRUE);
    glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
    

    glUseProgram(partProgram_);
        glUniform1f(glGetUniformLocation(partProgram_, "pointScale"), windowH / tanf(60.0 * 0.5f * (float)M_PI / 180.0f));
        const glm::mat4& ModelViewMatrix = camera_->GetModelViewMatrix();
        glUniformMatrix4fv(glGetUniformLocation(partProgram_, "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
        const glm::mat4& ModelViewProjectionMatrix = camera_->GetModelViewProjectionMatrix();
        glUniformMatrix4fv(glGetUniformLocation(partProgram_, "ModelViewProjectionMatrix"), 1, GL_FALSE, &ModelViewProjectionMatrix[0][0]);

        GLfloat shapecolor[] = {1.0f, 1.0f, 0.0f};
        glUniform3fv(glGetUniformLocation(partProgram_, "inputColor"), 1, shapecolor);

        glBindVertexArray(partVAO_);
            glDrawArrays(GL_POINTS, 0, psandbox_->NumParts());
        glBindVertexArray(0);

    glUseProgram(0);

}

std::ostream& operator << (std::ostream& out, const glm::mat4& matrix44)
{
    for(int i = 0; i < 4; ++i)
    {
        for(int j = 0; j < 4; ++j)
            out << matrix44[i][j] << " ";
        out << "\n";
    }
}

glm::mat4 getTransformationMatrix(MDShape* pshape)
{
    vec3 compos;
    mat3 rotmat;
    pshape->GetNodePos(compos);
    pshape->GetNodeOriMat(rotmat);

    // now convert the mat to glm mat 4
    glm::mat4 modelmat;
    for(int i = 0; i < 3; ++i)
        for(int j = 0; j < 3; ++j)
        {
            modelmat[i][j] = rotmat[j][i];
        }
    modelmat[3][0] = compos.x;
    modelmat[3][1] = compos.y;
    modelmat[3][2] = compos.z;
    modelmat[3][3] = 1.0;
    /*
    // some simple translation and rotation test
    glm::mat4 ViewTranslate = glm::translate(
        modelmat,
        glm::vec3(0.0f, 0.0f, 0.f)); 
   
    modelmat = glm::rotate(
        ViewTranslate,
        45.f, glm::vec3(0.0f, 0.0f, 1.0f));
    */
    return modelmat;
}

void GLApp::_drawShapes()
{
    const int nshapes = collisionShapes_.size();
    glUseProgram(shapeProgram_);
    for(int i = 0; i < nshapes; ++i)
    {

        const glm::mat4 modelmat = getTransformationMatrix(collisionShapes_[i]);
        const glm::mat4& ModelViewMatrix = camera_->GetModelViewMatrix() * modelmat;
        glUniformMatrix4fv(glGetUniformLocation(shapeProgram_, "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);
        const glm::mat4& ModelViewProjectionMatrix = camera_->GetModelViewProjectionMatrix() * modelmat;
        glUniformMatrix4fv(glGetUniformLocation(shapeProgram_, "ModelViewProjectionMatrix"), 1, GL_FALSE, &ModelViewProjectionMatrix[0][0]);
        const glm::mat3 NormalViewMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
        glUniformMatrix3fv(glGetUniformLocation(shapeProgram_, "NormalViewMatirx"), 1, GL_FALSE, &NormalViewMatrix[0][0]);

        GLfloat shapecolor[] = {0.0f, 1.0f, 1.0f};
        glUniform3fv(glGetUniformLocation(shapeProgram_, "inputColor"), 1, shapecolor);

        std::map<std::string, uint> vbomap = collisionShapes_[i]->_getMesh()->getVBOs();
        const std::vector<uint> indices = collisionShapes_[i]->_getMesh()->getVertexIndices();


        glBindVertexArray(shapeVAOs_[i]);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbomap["index"]);

            glDrawElements(
                GL_TRIANGLES,      // mode
                indices.size(),    // count
                GL_UNSIGNED_INT,   // type
                (void*)0           // element array buffer offset
            );
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

    }
    glUseProgram(0);

}


void GLApp::DisplayShapes()
{
    int iframe = 0;
    while(!glfwWindowShouldClose(window_) && iframe < 1e5)
    {
        glEnable(GL_DEPTH_TEST);
        glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);

        // let us simulate 10 steps before we render the scene
        _simulate();

        std::cout << "Simulation Time: " << psandbox_->CurrStep() << "\n";
        std::cout << "CPU time: " << glfwGetTime() << std::endl;
        _drawPoints();
        _drawShapes();
        glfwSwapBuffers(window_);
        glfwPollEvents();
        
        if(iframe % 100 == 0)
        {
            screenShotTGA(iframe / 100);
            psandbox_->OutputFile();
            simulator_->dump();

        }
        iframe++;
        
    }
    
}
