#include <GL/glew.h>
#include "md_scene.h"
#include "md_shape_include.h"
#include "md_scene.cuh"
#include "Multibody/md_multibody_chrono.h"

using std::cerr;
using std::cout;
using std::vector;
using std::endl;





void GLApp::_simulate()
{
    thrust::device_vector<FloatType> shape_foc(psandbox_->NumParts() * 4, 0.); 
    thrust::device_vector<FloatType> shape_mom(psandbox_->NumParts() * 4, 0.);

    for(int i = 0; i < 10; ++i)
    {
        if(psandbox_)
        {
            // define necessary components
            vec4 *p_pos, *p_vel, *p_ome, *p_foc, *p_mom;

            // use device vector
            thrust::fill(shape_foc.begin(), shape_foc.end(), 0.);
            thrust::fill(shape_mom.begin(), shape_mom.end(), 0.);
            vec4* c_foc = (vec4*)thrust::raw_pointer_cast(&shape_foc[0]);
            vec4* c_mom = (vec4*)thrust::raw_pointer_cast(&shape_mom[0]);

            psandbox_->PPCollidePhase();

            
            psandbox_->CopyDevicePtr((FloatType**)&(p_pos), 
                (FloatType**)&p_vel, 
                (FloatType**)&p_ome, 
                (FloatType**)&p_foc, 
                (FloatType**)&p_mom);


            const int nshapes = collisionShapes_.size();

            for(int j = 0; j < nshapes; ++j)
            {
                collisionShapes_[j]->IntersectSphere(p_pos, p_vel, p_ome, psandbox_->NumParts(), c_foc, c_mom, p_mom);
                // now compute p_foc -= c_foc
                // first wrap p_foc
                thrust::device_ptr<FloatType> part_foc((FloatType*)p_foc);
                thrust::transform(part_foc, part_foc + 4 * psandbox_->NumParts(), shape_foc.begin(), part_foc, thrust::minus<FloatType>());

                // reduce the force and torque container
                thrust::device_ptr<vec4> foc_ptr(c_foc);
                thrust::device_ptr<vec4> mom_ptr(c_mom);
                vec4 san_foc = thrust::reduce(foc_ptr, foc_ptr + psandbox_->NumParts(), make_vec4(0, 0, 0, 0));
                vec4 san_mom = thrust::reduce(mom_ptr, mom_ptr + psandbox_->NumParts(), make_vec4(0, 0, 0, 0));

                // std::cout << san_foc.x << "\t";
                // std::cout << san_foc.y << "\t";
                // std::cout << san_foc.z << "\t";
                // std::cout << san_foc.w << "\n";

                collisionShapes_[j]->SetNodeFoc(make_vec3(san_foc.x, san_foc.y, san_foc.z));
                collisionShapes_[j]->SetNodeMoF(make_vec3(san_mom.x, san_mom.y, san_mom.z));
            }
            psandbox_->IntegratePhase();
        }
        if(simulator_)
            simulator_->Step();
    }    
}
