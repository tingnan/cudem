#ifndef _MD_SCENE_H_
#define _MD_SCENE_H_

#include <cstdlib>
#include <iostream>
#include <vector>
#include <cassert>
#include <cmath>
#include <fstream>
#include <DEM/md_sand.h>



class GLApp
{
    uint partVBO_;
    uint partVAO_;
    std::vector<uint> shapeVAOs_;
    uint partProgram_;
    uint shapeProgram_;
    uint vertexSize_;
    uint numPart_;
    std::vector<float> partPos_;
    int windowH;
    int windowW;
    class GLFWwindow* window_;
    class GLMCamera* camera_;
    MDSimpleSand *psandbox_;
    std::vector<class MDShape*> collisionShapes_;
    void InitGL();
    void _drawPoints();
    void _drawShapes();
    void _simulate();

    // set a multibody simulator
    class MultibodySimulatorInterface* simulator_;
public:
    GLApp();
    ~GLApp();
    void SetShaders();
    void BindVisualAssets();
    void DisplayShapes();
    // since I am lazy~~, we will just manipulate the shapecontainer outside the class for now
    std::vector<class MDShape*>& getShapeContainer() {return collisionShapes_;}
};

#endif