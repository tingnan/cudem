// adpated from the CGAL example code 
#include <CGAL/trace.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/Surface_mesh_default_triangulation_3.h>
#include <CGAL/make_surface_mesh.h>
#include <CGAL/Implicit_surface_3.h>
#include <CGAL/IO/output_surface_facets_to_polyhedron.h>
#include <CGAL/Poisson_reconstruction_function.h>
#include <CGAL/Point_with_normal_3.h>
#include <CGAL/Vector_3.h>
#include <CGAL/property_map.h>
#include <CGAL/IO/read_xyz_points.h>
#include <CGAL/compute_average_spacing.h>
#include <CGAL/Eigen_solver_traits.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;

std::vector<FloatType> MeshGenerator::getMesh(
    const std::vector<FloatType>& vertices, 
    const std::vector<FloatType>& normals)
{
    std::vector<FloatType> output(vertices.size());
    _reconstructWithNormal(vertices, normals, output);
}


void reconstructWithNormal(
    const std::vector<FloatType>& vertices, 
    const std::vector<FloatType>& normals, 
    std::vector<FloatType>& output)
{
    // for opengl each vertex has 4 components. thus check if inputlist has size % 4 == 0
    if(vertices.size() % 4 != 0)
    {
        //error!
        return;
    }
    int npts = vertices.size() / 4;
    // now reconstruct the surface mesh from the points
    std::vector<CGAL::Point_with_normal_3<Kernel> > points;
    points.reserve(npts);
    for(int i = 0; i < npts; ++i)
    {
        FloatType hw = vertices[4 * i + 3] == 0 ? 1 : vertices[4 * i + 3];
        CGAL::Vector_3<Kernel> curnorm(normals[3 * i], normals[3 *i + 1], normals[3 * i + 2]);
        CGAL::Point_with_normal_3<Kernel> tmppt(vertices[4 * i], vertices[4 * i + 1], vertices[4 * i + 2], hw, curnorm);
        points[i] = tmppt;
    }

    // use a poission reconstruction function

    CGAL::Poisson_reconstruction_function<Kernel> myfunction(
        points.begin(), 
        points.end(), 
        CGAL::make_normal_of_point_with_normal_pmap(std::vector<CGAL::Point_with_normal_3<Kernel> >::value_type()));

    // compute the implicit function
    CGAL::Eigen_solver_traits<> solver;
    if( ! myfunction.compute_implicit_function(solver))
    {

        return;
    }

    // compute average spacing
    Kernel::FT average_spacing = CGAL::compute_average_spacing(points.begin(), points.end(), 6);

    // define implicit surface

    Kernel::Point_3 inner_point = myfunction.get_inner_point();
    Kernel::Sphere_3 bsphere = myfunction.bounding_sphere();
    Kernel::FT radius = std::sqrt(bsphere.squared_radius());

    Kernel::FT sm_sphere_radius = 5. * radius;
    Kernel::FT sm_distance = 0.3;
    Kernel::FT sm_dichotomy_error = sm_distance * average_spacing / 1000.0;

    CGAL::Implicit_surface_3<Kernel, CGAL::Poisson_reconstruction_function<Kernel> > surface(myfunction, 
        Kernel::Sphere_3(inner_point, sm_sphere_radius * sm_sphere_radius),
        sm_dichotomy_error / sm_sphere_radius);
    // definte surface mesh generation criteria

    Kernel::FT sm_angle = 20.0;
    Kernel::FT sm_radius = 30;
    CGAL::Surface_mesh_default_criteria_3<CGAL::Surface_mesh_default_triangulation_3> criteria(sm_angle, // Min triangle angle (degrees)
        sm_radius*average_spacing, // Max triangle size
        sm_distance*average_spacing); // Approximation error
    CGAL::Surface_mesh_default_triangulation_3 tr;
    CGAL::Surface_mesh_complex_2_in_triangulation_3< CGAL::Surface_mesh_default_triangulation_3> c2t3(tr);


    CGAL::make_surface_mesh(c2t3, // reconstructed mesh
        surface, // implicit surface
        criteria, // meshing criteria
        CGAL::Manifold_with_boundary_tag()); 
    
    if(tr.number_of_vertices() == 0)
    {
        return;
    }
}