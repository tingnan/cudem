/**
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 */
#include "Scene/md_scene.h"
#include "cudem.h"
#include "md_shape_include.h"
#include "GeneralHelper/md_cuda_helper.cuh"

int main()
{
    GLApp newApp;




    newApp.SetShaders();
    newApp.BindVisualAssets();
    newApp.DisplayShapes();

    return 0;
}