/* $Header: /var/cvs/mbdyn/mbdyn/mbdyn-1.0/modules/module-cudatest/cudatest.h,v 1.1 2012/12/20 20:38:28 masarati Exp $ */
/*
 * MBDyn (C) is a multibody analysis code.
 * http://www.mbdyn.org
 *
 * Copyright (C) 1996-2012
 *
 * Pierangelo Masarati  <masarati@aero.polimi.it>
 *
 * Dipartimento di Ingegneria Aerospaziale - Politecnico di Milano
 * via La Masa, 34 - 20156 Milano, Italy
 * http://www.aero.polimi.it
 *
 * Changing this copyright notice is forbidden.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation (version 2 of the License).
 * 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * Authors:	Pierangelo Masarati <masarati@aero.polimi.it>
 * 		Tingnan Zhang <tingnan1986@gatech.edu>
 */

#ifndef _GPULOADER_H_
#define _GPULOADER_H_
#include "md_header.h"
#include <string>

struct MDShapeData 
{
    RealVec F;
    RealVec M;
    RealVec X;
    RealVec V;
    RealVec A;
    mat3 Rot;
};
typedef std::vector<MDShapeData> MDShapeParser;

class GPULoader 
{
    // MD Specific Data
public:
    virtual	~GPULoader(void) {};
    virtual void SetSaveFreq(uint) = 0;
    virtual void RunOneStep(MDShapeParser&) = 0;
    virtual void InitGEO(const std::string&) = 0;
    virtual void InitDEM(const std::string&) = 0;
    virtual void InitRFT(const std::string&, const std::string&, const std::string&, const std::string&) = 0;
};

GPULoader* GPULoaderInit(uint);
void InitGPU(uint);
#endif // _GPULOADER_H_


